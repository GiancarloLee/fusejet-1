# Setting up dependencies

We recommend using IntelliJ IDEA as the IDE.

FuseJet uses maven to manage dependencies. Make sure that your ~/.m2/settings.xml file look like this:

    <?xml version="1.0" encoding="UTF-8"?>
    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
           <id>yifans-repo</id>
           <username>USERNAME</username>
           <password>PASSWORD</password>
        </server>
        <server>
           <id>yifans-repo-release</id>
           <username>USERNAME</username>
           <password>PASSWORD</password>
        </server>
    </servers>
    </settings>

# Running the program

Download BAE\_articles.zip from [our SkyDrive directory](http://sdrv.ms/13nuSNf). This is the rule-based relation extraction pipeline.

You can find FuseJet.jar FuseJet-BAE2Relations-Combined.properties and
Models/ in the zip file. FuseJet.jar is the fat jar built from the codebase
in this repository.

First, modify the first line in FuseJet-BAE2Relations-Combined.properties
to point to the Models/ directory. This properties file and can be found at src/main/resources/configs

Then run:

    java -cp FuseJet.jar -Xmx4g FuseJet.Utils.Console
    FuseJet-BAE2Relations-Combined.properties input.BAE_new.filelist
    output.new_facts.filelist

to extract relations.

# Code structure

FuseJet is called by:

    FuseJet.Utils.Console PROPS_FILE INPUT_FILE_LIST OUTPUT_FILE_LIST

`FuseJet.Utils.Console` looks at the properties file to execute
a sequence of actions. In the properties file, the user can define one Reader,
one Writer , and a sequence of Annotators. Readers and Writers read/write documents
in a file list and reside in `FuseJet.IO`. Annotators add annotations (terms,
name entities, relations) to a document and reside in `FuseJet.Annotators`.

# Jet resources

FuseJet is based on [Jet](http://cs.nyu.edu/grishman/jet/jet.html). Jet enjoys much nicer 
[documentation](http://cs.nyu.edu/grishman/jet/guide/Jet.html) than FuseJet, especially 
on [pattern matching](http://cs.nyu.edu/grishman/jet/guide/patterns.html), 
prepared by [Ralph](http://cs.nyu.edu/grishman/).