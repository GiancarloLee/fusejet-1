package FuseJet.Sentiment.JargonSentiment;

public class OptimizeHighlightsBagofWordsClassifier 
{
	static double bestprecision = 0.0;
	
	static int bestptopn = 0;
	static int beststopn = 0;
	static int bestctopn = 0;
	
	static double bestpthreshold = 0.0;
	static double beststhreshold = 0.0;
	static double bestcthreshold = 0.0;
	
	static int bestpcount = 0;
	static int bestscount = 0;
	static int bestccount = 0;

	public static void main(String[] args) throws Exception
	{
		optimizeHighlightsBagofWordsClassifier();
	}
	
	public static void optimizeHighlightsBagofWordsClassifier() throws Exception
	{
		int ptopn = 10;
		int stopn = 10;
		int ctopn = 10;
		
		int pcount = 2;
		int scount = 2;
		int ccount = 2;
		
		double pthreshold = 0.0;
		double sthreshold = 0.0;
		double cthreshold = 0.0;
		
		for(int cp = pcount; cp <= 2; ++cp)
			for(int cs = scount; cs <= 2; ++cs)
				for(int cc = ccount; cc <= 2; ++cc)
					for(double tp = pthreshold; tp <= 20; tp+=10)
						for(double ts = sthreshold; ts <= 20; ts+=10)
							for(double tc = cthreshold; tc <= 20; tc+=10)
								for(int topp = ptopn; topp <= 70; topp+=30)
									for(int tops = stopn; tops <= 70; tops+=30)
										for(int topc = ctopn; topc <= 70; topc+=30)
										{
											System.out.println("Processing : " + topp + "\t" + tops + "\t" + topc + "\t" + tp + "\t" + ts + "\t" + tc + "\t" + cp + "\t" + cs + "\t" + cc);
											optimizeH(topp, tops, topc, tp, ts, tc, cp, cs, cc);
										}
		
		System.out.println("Best Precision : " + bestprecision);
		System.out.println("Best PTOPN = " + bestptopn + "\tBest PTHRESHOLD = " + bestpthreshold + "\tBest PCOUNT = " + bestpcount);
		System.out.println("Best STOPN = " + beststopn + "\tBest STHRESHOLD = " + beststhreshold + "\tBest SCOUNT = " + bestscount);
		System.out.println("Best CTOPN = " + bestctopn + "\tBest CTHRESHOLD = " + bestcthreshold + "\tBest CCOUNT = " + bestccount);
	}
	
	public static void optimizeH(int ptopn, int stopn, int ctopn, double pthreshold, double sthreshold, double cthreshold, int pcount, int scount, int ccount) throws Exception
	{		
		HighlightsBagofWordsClassifier hc = new HighlightsBagofWordsClassifier(ptopn, stopn, ctopn, pthreshold, sthreshold, cthreshold, pcount, scount, ccount);
		
		double precision = ((double)(hc.countcorrectpositive + hc.countcorrectsignificant + hc.countcorrectcomparison + hc.countcorrectrelatedwork)/(double)hc.total);
		
		if(precision > bestprecision)
		{
			bestprecision = precision;
			
			bestptopn = ptopn;
			beststopn = stopn;
			bestctopn = ctopn;
			bestpthreshold = pthreshold;
			beststhreshold = sthreshold;
			bestcthreshold = cthreshold;
			bestpcount = pcount;
			bestscount = scount;
			bestccount = ccount;
		}
	}

}
