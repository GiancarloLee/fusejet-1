package FuseJet.Sentiment.JargonSentiment;

///////////////////////////////////////////////////////////////////////////////
//Copyright (C) 2001 Chieu Hai Leong and Jason Baldridge
//
//This library is free software; you can redistribute it and/or
//modify it under the terms of the GNU Lesser General Public
//License as published by the Free Software Foundation; either
//version 2.1 of the License, or (at your option) any later version.
//
//This library is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public
//License along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import opennlp.maxent.BasicContextGenerator;
import opennlp.maxent.ContextGenerator;
import opennlp.maxent.DataStream;
import opennlp.maxent.PlainTextByLineDataStream;
import opennlp.model.GenericModelReader;
import opennlp.model.MaxentModel;
import opennlp.model.RealValueFileEventStream;

/**
 * Test the model on some input.
 *
 * @author Jason Baldridge
 * @version $Revision: 1.4 $, $Date: 2008/11/06 20:00:34 $
 */
public class SentimentAnalysisPredictor {
    private MaxentModel _model;
    ContextGenerator _cg = new BasicContextGenerator();

//    public SentimentAnalysisPredictor() throws Exception {
//        String[] args = {};
//        main(args);
//    }

    private SentimentAnalysisPredictor(MaxentModel m) {
        _model = m;
    }

    /*** Added by Yifan He ***/

    private static SentimentAnalysisPredictor instance = null;

    private Set<String> significantwordset = new HashSet<String>();
    private Set<String> figureformulaset = new HashSet<String>();
    private Set<String> positivewordset = new HashSet<String>();
    private Set<String> comparisonwordset = new HashSet<String>();
    private NGramsNumbersFeatureExtractor ngramFeatureExtractor;

    public static SentimentAnalysisPredictor getInstance(String modelFile, String dataDir) {
        if (instance == null) {
            instance = new SentimentAnalysisPredictor(modelFile, dataDir);
        }
        return instance;
    }

    private SentimentAnalysisPredictor(String modelFile, String dataDir) {
        try {
            _model = new GenericModelReader(new File(modelFile)).getModel();
            significantwordset = readWordList(dataDir + "/SignificantWordList.txt");
            figureformulaset = readWordList(dataDir + "/FigureFormulaWordList.txt");
            positivewordset = readWordList(dataDir + "/PositiveWordList.txt");
            comparisonwordset = readWordList(dataDir + "/ComparisonWordList.txt");
            ngramFeatureExtractor = new NGramsNumbersFeatureExtractor(dataDir);
        } catch (Exception e) {
            System.err.println("Error reading sentiment classification model file...");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String predict(String[] words) {
        String[] features = generateFeatures(words);
        return _model.getBestOutcome(_model.eval(features));
    }

    private Set<String> readWordList(String wordListFile) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(wordListFile)));
        Set<String> wordList = new HashSet<String>();
        String line;
        while((line = reader.readLine())!=null)
        {
            wordList.add(line);
        }

        reader.close();
        return wordList;
    }

    private String[] generateFeatures(String[] words) {
        List<String> features = new ArrayList<String>();
        boolean contains = false;
        for (int i = 0; i < words.length; i++) {
            if (significantwordset.contains(words[i]) ||
                    (i<(words.length-1) && significantwordset.contains(words[i]+" "+words[i+1])))  {
                contains = true;
            }
        }
        features.add(contains ? "contains_significant_key_word=true":"contains_significant_key_word=false");

        contains = false;
        boolean figureContains = false;
        boolean comparisonContains = false;
        for(int i = 0 ; i < words.length ; ++i)
        {
            if(positivewordset.contains(words[i]))
            {
                contains = true;
            }
            if (comparisonwordset.contains(words[i]))   {
                comparisonContains = true;
            }
            if (figureformulaset.contains(words[i])) {
                figureContains = true;
            }

        }
        features.add(contains ? "contains_positive_key_word=true" : "contains_positive_key_word=false");
        features.add(comparisonContains ? "contains_comparison_key_word=true" : "contains_comparison_key_word=false");
        features.add(figureContains ? "contains_Figure_Formula=true" : "contains_Figure_Formula=false");
        features.add(ngramFeatureExtractor.generateFeature(wordsToSentence(words)));
        return features.toArray(new String[features.size()]);
    }

    private String wordsToSentence(String[] words) {
        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            builder.append(word).append(" ");
        }
        return builder.toString().trim();
    }

    public static void main(String[] args) {
        SentimentAnalysisPredictor p = SentimentAnalysisPredictor.getInstance("/Users/yhe/Dropbox/Work/FUSE/Release/Models/SentimentWordList/1Model.txt",
                "/Users/yhe/Dropbox/Work/FUSE/Release/Models/SentimentWordList");
        System.out.println(p.predict(new String[]{"this", "improves", "confirms"}));

    }

    /*** added by Yifan He ***/

    private void eval(String predicates) {
        eval(predicates, false);
    }

    private void eval(String predicates, boolean real) {
        String[] contexts = predicates.split(" ");
        double[] ocs;
        if (!real) {
            ocs = _model.eval(contexts);
        } else {
            float[] values = RealValueFileEventStream.parseContexts(contexts);
            ocs = _model.eval(contexts, values);
        }
//System.out.println("For context: " + predicates+ "\n" + _model.getAllOutcomes(ocs) + "\n");
//writer.println("For context: " + predicates+ "\n" + _model.getAllOutcomes(ocs) + "\n");

        System.out.println(_model.getAllOutcomes(ocs));
        writer.println(_model.getAllOutcomes(ocs));
    }

    private static void usage() {

    }


    public static PrintWriter writer;

    /**
     * Main method. Call as follows:
     * <p/>
     * java Predict dataFile (modelFile)
     */
//    public static void main(String[] args) throws Exception {
//        String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_21_2012/MaxEnt/TrainAndTest/";
//
//        int number = 10;
//        for (int i = 1; i <= number; ++i) {
//            String testingfile = home + "Test" + i + "/" + i + ".test";
//
//            String[] arguments = {testingfile};
//
//            String analysisfile = home + "Test" + i + "/" + i + ".result";
//            writer = new PrintWriter(analysisfile, "UTF-8");
//            predicts(arguments);
//            writer.close();
//        }
//    }



    public static void predicts(String[] args) {
        String dataFileName, modelFileName;
        boolean real = false;
        String type = "maxent";
        int ai = 0;
        if (args.length > 0) {
            while (args[ai].startsWith("-")) {
                if (args[ai].equals("-real")) {
                    real = true;
                } else if (args[ai].equals("-perceptron")) {
                    type = "perceptron";
                } else {
                    usage();
                }
                ai++;
            }
            dataFileName = args[ai++];
            if (args.length > ai) {
                modelFileName = args[ai++];
            } else {
                modelFileName = dataFileName.substring(0, dataFileName.lastIndexOf('.')) + "Model.txt";
            }
        } else {
            dataFileName = "";
            modelFileName = "weatherModel.txt";
        }
        SentimentAnalysisPredictor predictor = null;
        try {
            MaxentModel m = new GenericModelReader(new File(modelFileName)).getModel();
            predictor = new SentimentAnalysisPredictor(m);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        if (dataFileName.equals("")) {
            predictor.eval("Rainy Happy Humid");
            predictor.eval("Rainy");
            predictor.eval("Blarmey");
        } else {
            try {
                DataStream ds =
                        new PlainTextByLineDataStream(
                                new FileReader(new File(dataFileName)));

                while (ds.hasNext()) {
                    String s = (String) ds.nextToken();
                    predictor.eval(s.substring(0, s.lastIndexOf(' ')), real);
                }
            } catch (Exception e) {
                System.out.println("Unable to read from specified file: " + modelFileName);
                System.out.println();
                e.printStackTrace();
            }
        }
    }

}
