//FOR MY ANNOTATION SPECIFICATIONS.
//This class is used to get the numbers of high-ranked N-Grams in "RELATED_WORK", "POSITIVE", "SIGNIFICANT" and "COMPARISON" respectively,
//which are used as features in the maxent model.

package FuseJet.Sentiment.JargonSentiment;

import java.io.*;
import java.util.*;

public class NGramsNumbersFeatureExtractor {
    //the top n grams in the other-only files.
    static int RELATEDWORKTOPN = 100;
    static int POSITIVETOPN = 100;
    static int SIGNIFICANTTOPN = 100;
    static int COMPARISONTOPN = 100;

    //the threshold to select the top-ranked grams in rank files.
    static double RELATEDWORKTHRESHOLD = 20.0;
    static double POSITIVETHRESHOLD = 20.0;
    static double SIGNIFICANTTHRESHOLD = 20.0;
    static double COMPARISONTHRESHOLD = 20.0;

    //the threshold to consider the sentence as type X if the number of X-Facilitated-Grams in the sentence contains is more than X-COUNTTHREHOLD.
    static int RELATEDWORKCOUNTTHRESHOLD = 2;
    static int POSITIVECOUNTTHRESHOLD = 2;
    static int SIGNIFICANTCOUNTTHRESHOLD = 2;
    static int COMPARISONCOUNTTHRESHOLD = 2;

    //The number of divided files; n in n-folder cross validation
    static final int number = 10;

    //The max number of N-Grams
    static final int MAXN = 3;

    //directories
    static String encoding = "UTF-8";
    static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_21_2012/TrainAndTest/";

    //this hash set stores all the unigrams of highlights in rank file
    static HashSet<String> relatedworkunigramsset;
    static HashSet<String> positiveunigramsset;
    static HashSet<String> significantunigramsset;
    static HashSet<String> comparisonunigramsset;

    //this hash set stores all the bigrams of highlights in rank file
    static HashSet<String> relatedworkbigramsset;
    static HashSet<String> positivebigramsset;
    static HashSet<String> significantbigramsset;
    static HashSet<String> comparisonbigramsset;

    //this hash set stores all the trigrams of highlights in rank file
    static HashSet<String> relatedworktrigramset;
    static HashSet<String> positivetrigramset;
    static HashSet<String> significanttrigramset;
    static HashSet<String> comparisontrigramset;

    //this hash map stores the answer keys for the input data
    static HashMap<String, String> answermap;

    //this hash map stores the predicted results for the input data
    static HashMap<String, String> resultmap;

    //this arraylist stores all the testing data;
    static ArrayList<String> testdata;

    static String wronginstancesfile = home + "wronginstancesfile.txt";
    static String correctinstancesfile = home + "correctinstancesfile.txt";

    static String numberfile = home + "numbers.txt";
    static PrintWriter numberwriter;

    static PrintWriter wrongwriter;
    static PrintWriter correctwriter;

    static int countcorrectpositive = 0;
    static int countcorrectsignificant = 0;
    static int countcorrectcomparison = 0;
    static int countcorrectrelatedwork = 0;

    static int total = 0;

    //count only predicted as POSTIVE, SIGNIFICANT, COMPARISON;
    static int subtotal = 0;

    public NGramsNumbersFeatureExtractor() {
        relatedworkunigramsset = new HashSet<String>();
        relatedworkbigramsset = new HashSet<String>();
        relatedworktrigramset = new HashSet<String>();

        positiveunigramsset = new HashSet<String>();
        positivebigramsset = new HashSet<String>();
        positivetrigramset = new HashSet<String>();

        significantunigramsset = new HashSet<String>();
        significantbigramsset = new HashSet<String>();
        significanttrigramset = new HashSet<String>();

        comparisonunigramsset = new HashSet<String>();
        comparisonbigramsset = new HashSet<String>();
        comparisontrigramset = new HashSet<String>();
    }

    public NGramsNumbersFeatureExtractor(String subDirectory) {
        relatedworkunigramsset = new HashSet<String>();
        relatedworkbigramsset = new HashSet<String>();
        relatedworktrigramset = new HashSet<String>();

        positiveunigramsset = new HashSet<String>();
        positivebigramsset = new HashSet<String>();
        positivetrigramset = new HashSet<String>();

        significantunigramsset = new HashSet<String>();
        significantbigramsset = new HashSet<String>();
        significanttrigramset = new HashSet<String>();

        comparisonunigramsset = new HashSet<String>();
        comparisonbigramsset = new HashSet<String>();
        comparisontrigramset = new HashSet<String>();
//        answermap = new HashMap<String, String>();
//        resultmap = new HashMap<String, String>();
        try {
            readRelatedWorkFacilitatedGrams(subDirectory + "/");
            readPositiveFacilitatedGrams(subDirectory + "/");
            readSignificantFacilitatedGrams(subDirectory + "/");
            readComparisonFacilitatedGrams(subDirectory + "/");
        } catch (Exception e) {
            System.err.println("Error loading sentiment models...");
            e.printStackTrace();
        }
    }

    public String generateFeature(String sentence) {
        int relatedworkcount = countRelatedWorkFacilitatedGrams(sentence);
        int positivecount = countPositiveFacilitatedGrams(sentence);
        int significantcount = countSignificantFacilitatedGrams(sentence);
        int comparisoncount = countComparisonFacilitatedGrams(sentence);

        if (positivecount >= POSITIVECOUNTTHRESHOLD) {
            return "NGrams_feature=POSITIVE";
        } else if (significantcount >= SIGNIFICANTCOUNTTHRESHOLD) {
            return "NGrams_feature=SIGNIFICANT";
        } else if (comparisoncount >= COMPARISONCOUNTTHRESHOLD) {
            return "NGrams_feature=COMPARISON";
        } else {
            return "NGrams_feature=RELATED_WORK";
        }
    }

    public static void main(String args[]) throws Exception {
        wrongwriter = new PrintWriter(wronginstancesfile, encoding);
        correctwriter = new PrintWriter(correctinstancesfile, encoding);

        numberwriter = new PrintWriter(numberfile, encoding);

        for (int i = 1; i <= number; ++i) {
            String subdirectory = home + "Test" + i + "/";

//			System.out.println("Processing : " + subdirectory + " ... ");

            String testingfile = subdirectory + i + ".testing";

            relatedworkunigramsset = new HashSet<String>();
            relatedworkbigramsset = new HashSet<String>();
            relatedworktrigramset = new HashSet<String>();

            positiveunigramsset = new HashSet<String>();
            positivebigramsset = new HashSet<String>();
            positivetrigramset = new HashSet<String>();

            significantunigramsset = new HashSet<String>();
            significantbigramsset = new HashSet<String>();
            significanttrigramset = new HashSet<String>();

            comparisonunigramsset = new HashSet<String>();
            comparisonbigramsset = new HashSet<String>();
            comparisontrigramset = new HashSet<String>();

            answermap = new HashMap<String, String>();
            resultmap = new HashMap<String, String>();

            testdata = new ArrayList<String>();

            readRelatedWorkFacilitatedGrams(subdirectory);
            readPositiveFacilitatedGrams(subdirectory);
            readSignificantFacilitatedGrams(subdirectory);
            readComparisonFacilitatedGrams(subdirectory);

            readData(testingfile);
            classify();
        }

        wrongwriter.close();
        correctwriter.close();

        numberwriter.close();
    }

    public static void readRelatedWorkFacilitatedGrams(String subdirectory) throws Exception {
        String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_related_work_Highlight.txt";
        String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_related_work_Highlight.txt";
        String trigramotheronlyinput = subdirectory + "3Grams/3gram_data_only_in_related_work_Highlight.txt";

        String unigramrankinput = subdirectory + "1Grams/1gram_data_related_work_Rank.txt";
        String bigramrankinput = subdirectory + "2Grams/2gram_data_related_work_Rank.txt";
        String trigramrankinput = subdirectory + "3Grams/3gram_data_related_work_Rank.txt";

        BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
        BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
        BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));

        BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
        BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
        BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

        String line = "";
        int linenumber = 1;
        while ((line = unigramotheronlyreader.readLine()) != null) {
            if (linenumber <= RELATEDWORKTOPN) {
                String fields[] = line.split("\\t");
                relatedworkunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = bigramotheronlyreader.readLine()) != null) {
            if (linenumber <= RELATEDWORKTOPN) {
                String fields[] = line.split("\\t");
                relatedworkbigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = trigramotheronlyreader.readLine()) != null) {
            if (linenumber <= RELATEDWORKTOPN) {
                String fields[] = line.split("\\t");
                relatedworktrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";

        while ((line = unigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= RELATEDWORKTHRESHOLD) {
                relatedworkunigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = bigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= RELATEDWORKTHRESHOLD) {
                relatedworkbigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = trigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= RELATEDWORKTHRESHOLD) {
                relatedworktrigramset.add(fields[0]);
//				System.out.println(line);
            }
        }

        unigramotheronlyreader.close();
        bigramotheronlyreader.close();
        trigramotheronlyreader.close();

        unigramrankreader.close();
        bigramrankreader.close();
        trigramrankreader.close();
    }

    public static void readPositiveFacilitatedGrams(String subdirectory) throws Exception {
        String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_positive_Highlight.txt";
        String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_positive_Highlight.txt";
        String trigramotheronlyinput = subdirectory + "3Grams/3gram_data_only_in_positive_Highlight.txt";

        String unigramrankinput = subdirectory + "1Grams/1gram_data_positive_Rank.txt";
        String bigramrankinput = subdirectory + "2Grams/2gram_data_positive_Rank.txt";
        String trigramrankinput = subdirectory + "3Grams/3gram_data_positive_Rank.txt";

        BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
        BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
        BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));

        BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
        BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
        BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

        String line = "";
        int linenumber = 1;
        while ((line = unigramotheronlyreader.readLine()) != null) {
            if (linenumber <= POSITIVETOPN) {
                String fields[] = line.split("\\t");
                positiveunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = bigramotheronlyreader.readLine()) != null) {
            if (linenumber <= POSITIVETOPN) {
                String fields[] = line.split("\\t");
                positivebigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = trigramotheronlyreader.readLine()) != null) {
            if (linenumber <= POSITIVETOPN) {
                String fields[] = line.split("\\t");
                positivetrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";

        while ((line = unigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD) {
                positiveunigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = bigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD) {
                positivebigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = trigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD) {
                positivetrigramset.add(fields[0]);
//				System.out.println(line);
            }
        }

        unigramotheronlyreader.close();
        bigramotheronlyreader.close();
        trigramotheronlyreader.close();

        unigramrankreader.close();
        bigramrankreader.close();
        trigramrankreader.close();
    }

    public static void readSignificantFacilitatedGrams(String subdirectory) throws Exception {
        String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_significant_Highlight.txt";
        String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_significant_Highlight.txt";
        String trigramotheronlyinput = subdirectory + "3Grams/3gram_data_only_in_significant_Highlight.txt";

        String unigramrankinput = subdirectory + "1Grams/1gram_data_significant_Rank.txt";
        String bigramrankinput = subdirectory + "2Grams/2gram_data_significant_Rank.txt";
        String trigramrankinput = subdirectory + "3Grams/3gram_data_significant_Rank.txt";

        BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
        BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
        BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));

        BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
        BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
        BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

        String line = "";
        int linenumber = 1;
        while ((line = unigramotheronlyreader.readLine()) != null) {
            if (linenumber <= SIGNIFICANTTOPN) {
                String fields[] = line.split("\\t");
                significantunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = bigramotheronlyreader.readLine()) != null) {
            if (linenumber <= SIGNIFICANTTOPN) {
                String fields[] = line.split("\\t");
                significantbigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = trigramotheronlyreader.readLine()) != null) {
            if (linenumber <= SIGNIFICANTTOPN) {
                String fields[] = line.split("\\t");
                significanttrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";

        while ((line = unigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD) {
                significantunigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = bigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD) {
                significantbigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = trigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD) {
                significanttrigramset.add(fields[0]);
//				System.out.println(line);
            }
        }

        unigramotheronlyreader.close();
        bigramotheronlyreader.close();
        trigramotheronlyreader.close();

        unigramrankreader.close();
        bigramrankreader.close();
        trigramrankreader.close();
    }

    public static void readComparisonFacilitatedGrams(String subdirectory) throws Exception {
        String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_comparison_Highlight.txt";
        String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_comparison_Highlight.txt";
        String trigramotheronlyinput = subdirectory + "3Grams/3gram_data_only_in_comparison_Highlight.txt";

        String unigramrankinput = subdirectory + "1Grams/1gram_data_comparison_Rank.txt";
        String bigramrankinput = subdirectory + "2Grams/2gram_data_comparison_Rank.txt";
        String trigramrankinput = subdirectory + "3Grams/3gram_data_comparison_Rank.txt";

        BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
        BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
        BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));

        BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
        BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
        BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

        String line = "";
        int linenumber = 1;
        while ((line = unigramotheronlyreader.readLine()) != null) {
            if (linenumber <= COMPARISONTOPN) {
                String fields[] = line.split("\\t");
                comparisonunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = bigramotheronlyreader.readLine()) != null) {
            if (linenumber <= COMPARISONTOPN) {
                String fields[] = line.split("\\t");
                comparisonbigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";
        linenumber = 1;

        while ((line = trigramotheronlyreader.readLine()) != null) {
            if (linenumber <= COMPARISONTOPN) {
                String fields[] = line.split("\\t");
                comparisontrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
                ++linenumber;
            } else
                break;
        }

        line = "";

        while ((line = unigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD) {
                comparisonunigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = bigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD) {
                comparisonbigramsset.add(fields[0]);
//				System.out.println(line);
            }
        }

        line = "";

        while ((line = trigramrankreader.readLine()) != null) {
            String fields[] = line.split("\\t");
            if (Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD) {
                comparisontrigramset.add(fields[0]);
//				System.out.println(line);
            }
        }

        unigramotheronlyreader.close();
        bigramotheronlyreader.close();
        trigramotheronlyreader.close();

        unigramrankreader.close();
        bigramrankreader.close();
        trigramrankreader.close();
    }

    public static void readData(String inputfile) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));

        String line = "";

        while ((line = reader.readLine()) != null) {
            ++total;
            String fields[] = line.split("\\t");

            if (resultmap.containsKey(line))
                System.out.println(line);

            answermap.put(line, fields[6]);
            resultmap.put(line, "none");

            testdata.add(line);
        }

        reader.close();
    }

    public static void classify() throws Exception {
        int positivepredictionnum = 0;
        int significantpredictionnum = 0;
        int comparisonpredictionnum = 0;
        int relatedworkpredictionnum = 0;

        int positivecorrectpredictionnum = 0;
        int significantcorrectpredictionnum = 0;
        int comparisoncorrectpredictionnum = 0;
        int relatedworkcorrectpredictionnum = 0;

        for (int i = 0; i < testdata.size(); ++i) {
//		for(String key : resultmap.keySet())
//		{
            String key = testdata.get(i);

            int relatedworkcount = 0;
            int positivecount = 0;
            int significantcount = 0;
            int comparisoncount = 0;

            String fields[] = key.split("\\t");
            String sentencefield = fields[8];
            String sentence = sentencefield.substring(9, sentencefield.length());

//			System.out.println(sentence);

            relatedworkcount = countRelatedWorkFacilitatedGrams(sentence);
            positivecount = countPositiveFacilitatedGrams(sentence);
            significantcount = countSignificantFacilitatedGrams(sentence);
            comparisoncount = countComparisonFacilitatedGrams(sentence);

//			numberwriter.println("related_work_grams_num=" + relatedworkcount + "\tpositive_grams_num=" + positivecount + "\tsignificant_grams_num=" + significantcount + "\tcomparison_grams_num=" + comparisoncount);

            if (positivecount >= POSITIVECOUNTTHRESHOLD) {
                resultmap.put(key, "SENTIMENT=POSITIVE");
                ++positivepredictionnum;
                ++subtotal;

                numberwriter.println("NGrams_feature=POSITIVE");

                if (answermap.get(key).equals("SENTIMENT=POSITIVE")) {
                    ++countcorrectpositive;
                    ++positivecorrectpredictionnum;
                    correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
                } else {
                    wrongwriter.println(key);
                }
            } else if (significantcount >= SIGNIFICANTCOUNTTHRESHOLD) {
                resultmap.put(key, "SENTIMENT=SIGNIFICANT");
                ++significantpredictionnum;
                ++subtotal;

                numberwriter.println("NGrams_feature=SIGNIFICANT");

                if (answermap.get(key).equals("SENTIMENT=SIGNIFICANT")) {
                    ++countcorrectsignificant;
                    ++significantcorrectpredictionnum;
                    correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
                } else {
                    wrongwriter.println(key);
                }
            } else if (comparisoncount >= COMPARISONCOUNTTHRESHOLD) {
                resultmap.put(key, "SENTIMENT=COMPARISON");
                ++comparisonpredictionnum;
                ++subtotal;

                numberwriter.println("NGrams_feature=COMPARISON");

                if (answermap.get(key).equals("SENTIMENT=COMPARISON")) {
                    ++countcorrectcomparison;
                    ++comparisoncorrectpredictionnum;
                    correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
                } else {
                    wrongwriter.println(key);
                }
            } else {
                resultmap.put(key, "SENTIMENT=RELATED_WORK");
                ++relatedworkpredictionnum;

                numberwriter.println("NGrams_feature=RELATED_WORK");

                if (answermap.get(key).equals("SENTIMENT=RELATED_WORK")) {
                    ++countcorrectrelatedwork;
                    ++relatedworkcorrectpredictionnum;
                    correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Assert : " + count);
                } else {
                    wrongwriter.println(key);
                }
            }
        }

        System.out.println(positivecorrectpredictionnum + "\t" + positivepredictionnum + "\t\t" + significantcorrectpredictionnum + "\t" + significantpredictionnum +
                "\t\t" + comparisoncorrectpredictionnum + "\t" + comparisonpredictionnum + "\t\t" + relatedworkcorrectpredictionnum + "\t" + relatedworkpredictionnum);

    }

    public static int countRelatedWorkFacilitatedGrams(String sentence) {
        int unigramcount = 0;
        int bigramcount = 0;
        int trigramcount = 0;

        String tokens[] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");

        ArrayList<String> unigramarray = new ArrayList<String>();
        ArrayList<String> bigramarray = new ArrayList<String>();
        ArrayList<String> trigramarray = new ArrayList<String>();

        for (int i = 0; i < tokens.length; ++i) {
            String unigram = tokens[i];
            unigramarray.add(unigram);
        }

        for (int i = 0; i < tokens.length - 1; ++i) {
            String bigram = tokens[i] + " " + tokens[i + 1];
            bigramarray.add(bigram);
        }

        for (int i = 0; i < tokens.length - 2; ++i) {
            String tribgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
            trigramarray.add(tribgram);
        }

        for (int i = 0; i < unigramarray.size(); ++i) {
            if (relatedworkunigramsset.contains(unigramarray.get(i))) {
                ++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < bigramarray.size(); ++i) {
            if (relatedworkbigramsset.contains(bigramarray.get(i))) {
                ++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < trigramarray.size(); ++i) {
            if (relatedworktrigramset.contains(trigramarray.get(i))) {
                ++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

//		return (unigramcount + bigramcount + trigramcount);
        return (bigramcount + trigramcount);
//		return (unigramcount);
    }

    public static int countPositiveFacilitatedGrams(String sentence) {
        int unigramcount = 0;
        int bigramcount = 0;
        int trigramcount = 0;

        String tokens[] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");

        ArrayList<String> unigramarray = new ArrayList<String>();
        ArrayList<String> bigramarray = new ArrayList<String>();
        ArrayList<String> trigramarray = new ArrayList<String>();

        for (int i = 0; i < tokens.length; ++i) {
            String unigram = tokens[i];
            unigramarray.add(unigram);
        }

        for (int i = 0; i < tokens.length - 1; ++i) {
            String bigram = tokens[i] + " " + tokens[i + 1];
            bigramarray.add(bigram);
        }

        for (int i = 0; i < tokens.length - 2; ++i) {
            String tribgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
            trigramarray.add(tribgram);
        }

        for (int i = 0; i < unigramarray.size(); ++i) {
            if (positiveunigramsset.contains(unigramarray.get(i))) {
                ++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < bigramarray.size(); ++i) {
            if (positivebigramsset.contains(bigramarray.get(i))) {
                ++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < trigramarray.size(); ++i) {
            if (positivetrigramset.contains(trigramarray.get(i))) {
                ++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

//		return (unigramcount + bigramcount + trigramcount);
        return (bigramcount + trigramcount);
//		return (unigramcount);
    }

    public static int countSignificantFacilitatedGrams(String sentence) {
        int unigramcount = 0;
        int bigramcount = 0;
        int trigramcount = 0;

        String tokens[] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");

        ArrayList<String> unigramarray = new ArrayList<String>();
        ArrayList<String> bigramarray = new ArrayList<String>();
        ArrayList<String> trigramarray = new ArrayList<String>();

        for (int i = 0; i < tokens.length; ++i) {
            String unigram = tokens[i];
            unigramarray.add(unigram);
        }

        for (int i = 0; i < tokens.length - 1; ++i) {
            String bigram = tokens[i] + " " + tokens[i + 1];
            bigramarray.add(bigram);
        }

        for (int i = 0; i < tokens.length - 2; ++i) {
            String tribgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
            trigramarray.add(tribgram);
        }

        for (int i = 0; i < unigramarray.size(); ++i) {
            if (significantunigramsset.contains(unigramarray.get(i))) {
                ++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < bigramarray.size(); ++i) {
            if (significantbigramsset.contains(bigramarray.get(i))) {
                ++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < trigramarray.size(); ++i) {
            if (significanttrigramset.contains(trigramarray.get(i))) {
                ++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

//		return (unigramcount + bigramcount + trigramcount);
        return (bigramcount + trigramcount);
//		return (unigramcount);
    }

    public static int countComparisonFacilitatedGrams(String sentence) {
        int unigramcount = 0;
        int bigramcount = 0;
        int trigramcount = 0;

        String tokens[] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");

        ArrayList<String> unigramarray = new ArrayList<String>();
        ArrayList<String> bigramarray = new ArrayList<String>();
        ArrayList<String> trigramarray = new ArrayList<String>();

        for (int i = 0; i < tokens.length; ++i) {
            String unigram = tokens[i];
            unigramarray.add(unigram);
        }

        for (int i = 0; i < tokens.length - 1; ++i) {
            String bigram = tokens[i] + " " + tokens[i + 1];
            bigramarray.add(bigram);
        }

        for (int i = 0; i < tokens.length - 2; ++i) {
            String tribgram = tokens[i] + " " + tokens[i + 1] + " " + tokens[i + 2];
            trigramarray.add(tribgram);
        }

        for (int i = 0; i < unigramarray.size(); ++i) {
            if (comparisonunigramsset.contains(unigramarray.get(i))) {
                ++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < bigramarray.size(); ++i) {
            if (comparisonbigramsset.contains(bigramarray.get(i))) {
                ++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

        for (int i = 0; i < trigramarray.size(); ++i) {
            if (comparisontrigramset.contains(trigramarray.get(i))) {
                ++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
            }
        }

//		System.out.print("\n");

//		return (unigramcount + bigramcount + trigramcount);
        return (bigramcount + trigramcount);
//		return (unigramcount);
    }

//	public static void printngrams(String sentence) throws Exception
//	{
//		int unigramcount = 0;
//		int bigramcount = 0;
//		int trigramcount = 0;
//		
//		String tokens [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
//		
//		ArrayList<String> unigramarray = new ArrayList<String>();
//		ArrayList<String> bigramarray = new ArrayList<String>();
//		ArrayList<String> trigramarray = new ArrayList<String>();
//		
//		for(int i = 0; i < tokens.length; ++i)
//		{
//			String unigram = tokens[i];
//			unigramarray.add(unigram);
//		}
//		
//		for(int i = 0 ; i < tokens.length-1; ++i)
//		{
//			String bigram = tokens[i]+" "+tokens[i+1];
//			bigramarray.add(bigram);
//		}
//		
//		for(int i = 0 ; i < tokens.length-2; ++i)
//		{
//			String tribgram = tokens[i] + " " + tokens[i+1] + " " + tokens[i+2];
//			trigramarray.add(tribgram);
//		}
//		
//		for(int i = 0; i < unigramarray.size(); ++i)
//		{
//			if(unigramsset.contains(unigramarray.get(i)))
//			{
//				++unigramcount;
//				correctwriter.print(unigramarray.get(i) + " | ");
////				System.out.print(unigramarray.get(i)+"\t");
//			}
//		}
//		
////		System.out.print("\n");
//		
//		for(int i = 0; i < bigramarray.size(); ++i)
//		{
//			if(bigramsset.contains(bigramarray.get(i)))
//			{
//				++bigramcount;
//				correctwriter.print(bigramarray.get(i) + " | ");
////				System.out.print(bigramarray.get(i)+"\t");
//			}
//		}
//		
////		System.out.print("\n");
//		
//		for(int i = 0; i < trigramarray.size(); ++i)
//		{
//			if(trigramset.contains(trigramarray.get(i)))
//			{
//				++trigramcount;
//				correctwriter.print(trigramarray.get(i) + " | ");
////				System.out.print(trigramarray.get(i)+"\t");
//			}
//		}
//		
//		correctwriter.print("\n");
////		System.out.print("\n");
//	}
}
