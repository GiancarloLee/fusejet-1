//FOR MY ANNOTATION SPECIFICATIONS.
//This class is used to extract and rank N-Grams from the highlights of the training data in the Highlight Classifier.

package FuseJet.Sentiment.JargonSentiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class ExtractNGramsForTrainingData 
{
	//The max number of N-Grams
	static final int MAXN = 3;
			
	//The number of divided files; n in n-folder cross validation
	static final int number = 10;
			
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Mar_24_2012/TrainAndTest/";

	static ArrayList<String> relatedworkhighlightsarray;
	static ArrayList<String> notrelatedworkarray;
	
	static ArrayList<String> positivehighlightsarray;
	static ArrayList<String> notpositivearray;
	
	static ArrayList<String> significanthighlightsarray;
	static ArrayList<String> notsignificantarray;
	
	static ArrayList<String> comparisonhighlightsarray;
	static ArrayList<String> notcomparisonarray;
	
	static HashMap<String, Double> relatedworkhighlightmap;
	static HashMap<String, Double> notrelatedworkmap;
	static HashMap<String, Double> relatedworkrankmap;
	static HashMap<String, Double> relatedworkhighlightonlymap;
	
	static HashMap<String, Double> positivehighlightmap;
	static HashMap<String, Double> notpositivemap;
	static HashMap<String, Double> positiverankmap;
	static HashMap<String, Double> positivehighlightonlymap;
	
	static HashMap<String, Double> significanthighlightmap;
	static HashMap<String, Double> notsignificantmap;
	static HashMap<String, Double> significantrankmap;
	static HashMap<String, Double> significanthighlightonlymap;
	
	static HashMap<String, Double> comparisonhighlightmap;
	static HashMap<String, Double> notcomparisonmap;
	static HashMap<String, Double> comparisonrankmap;
	static HashMap<String, Double> comparisonhighlightonlymap;
	
	public static void main(String[] args) throws Exception
	{
		for(int i = 1; i <= number; ++i)
		{	
			String subdirectory = home + "Test" + i + "/";
			
			System.out.println("Processing : " + subdirectory + " ... ");
			
			String trainingfile = subdirectory + i +".training";
			
			relatedworkhighlightsarray = new ArrayList<String>();
			notrelatedworkarray = new ArrayList<String>();
			
			readRelatedWorkHighlights(trainingfile);
			readNotRelatedWorkSentences(trainingfile);
			
			positivehighlightsarray = new ArrayList<String>();
			notpositivearray = new ArrayList<String>();
			
			readPositiveHighlights(trainingfile);
			readNotPositiveSentences(trainingfile);
			
			significanthighlightsarray = new ArrayList<String>();
			notsignificantarray = new ArrayList<String>();
			
			readSignificantHighlights(trainingfile);
			readNotSignificantSentences(trainingfile);
			
			comparisonhighlightsarray = new ArrayList<String>();
			notcomparisonarray = new ArrayList<String>();
			
			readComparisonHighlights(trainingfile);
			readNotComparisonSentences(trainingfile);

			for(int j = 1; j<=MAXN ; ++j)
			{
				relatedworkhighlightmap = new HashMap<String, Double>();
				notrelatedworkmap = new HashMap<String, Double>();
				relatedworkrankmap = new HashMap<String, Double>();
				relatedworkhighlightonlymap = new HashMap<String, Double>();
				
				extractRelatedWorkHighlightNGrams(j, subdirectory);
				extractNotRelatedWorkSentNGrams(j, subdirectory);
				rankRelatedWorkNGrams(j, subdirectory);
				
				positivehighlightmap = new HashMap<String, Double>();
				notpositivemap = new HashMap<String, Double>();
				positiverankmap = new HashMap<String, Double>();
				positivehighlightonlymap = new HashMap<String, Double>();
				
				extractPositiveHighlightNGrams(j, subdirectory);
				extractNotPositiveSentNGrams(j, subdirectory);
				rankPositiveNGrams(j, subdirectory);
				
				significanthighlightmap = new HashMap<String, Double>();
				notsignificantmap = new HashMap<String, Double>();
				significantrankmap = new HashMap<String, Double>();
				significanthighlightonlymap = new HashMap<String, Double>();
				
				extractSignificantHighlightNGrams(j, subdirectory);
				extractNotSignificantSentNGrams(j, subdirectory);
				rankSignificantNGrams(j, subdirectory);
				
				comparisonhighlightmap = new HashMap<String, Double>();
				notcomparisonmap = new HashMap<String, Double>();
				comparisonrankmap = new HashMap<String, Double>();
				comparisonhighlightonlymap = new HashMap<String, Double>();
				
				extractComparisonHighlightNGrams(j, subdirectory);
				extractNotComparisonSentNGrams(j, subdirectory);
				rankComparisonNGrams(j, subdirectory);
			}
		}
	}
	
	public static void readRelatedWorkHighlights(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(!fields[6].equals("SENTIMENT=RELATED_WORK"))
				continue;
			else
			{
				String highlighted = fields[7];
				
				if(highlighted.equals("highlight=null"))
					continue;
				
				String highlight = highlighted.substring(10, highlighted.length());
				
//				System.out.println(highlight);
				
				relatedworkhighlightsarray.add(highlight);
			}
		}
		
		reader.close();
	}

	public static void readNotRelatedWorkSentences(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(fields[6].equals("SENTIMENT=RELATED_WORK"))
				continue;
			
			String sentencefield = fields[8];
			String sentence = sentencefield.substring(9, sentencefield.length());
				
//			System.out.println(sentence);
			
			notrelatedworkarray.add(sentence);
		}
		
		reader.close();
	}
	
	public static void extractRelatedWorkHighlightNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_related_work_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
		for(int i = 0; i < relatedworkhighlightsarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = relatedworkhighlightsarray.get(i).split(" ");
			
//			System.out.println(highlightsarray.get(i));
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				String ngram = "";
				
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				for(int m = j; m < j+n ; ++m)
				{
					
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		HashMap<String, Integer> sortedmapngram = new HashMap<String, Integer>();
		sortedmapngram = sortHashMapByValuesI(ngramsmap);
		
		for(String key : sortedmapngram.keySet())
		{
			relatedworkhighlightmap.put(key, (double)sortedmapngram.get(key)/(double)total);
			writer.println(key+"\t"+sortedmapngram.get(key)+"\t"+(double)sortedmapngram.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void extractNotRelatedWorkSentNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_not_related_work_Sent.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
//		System.out.println(notrelatedworkarray.size());
		
		for(int i = 0; i < notrelatedworkarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = notrelatedworkarray.get(i).split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
			
//			System.out.println();
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				String ngram = "";
				
				for(int m = j; m < j+n ; ++m)
				{
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		for(String key : ngramsmap.keySet())
		{
			notrelatedworkmap.put(key, (double)ngramsmap.get(key)/(double)total);
			writer.println(key+"\t"+ngramsmap.get(key)+"\t"+(double)ngramsmap.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void rankRelatedWorkNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n + "gram_data_related_work_Rank.txt";
		String highlightonlyoutput = ngramsubdirectory + n + "gram_data_only_in_related_work_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		PrintWriter highlightonlywriter = new PrintWriter(highlightonlyoutput, encoding);
		
		for(String key : relatedworkhighlightmap.keySet())
		{
			if(!notrelatedworkmap.keySet().contains(key))
			{
				relatedworkhighlightonlymap.put(key, relatedworkhighlightmap.get(key));
			}
			else
			{
				relatedworkrankmap.put(key, relatedworkhighlightmap.get(key)/notrelatedworkmap.get(key));
			}
		}
		
		HashMap<String, Double> sortedmapngram = new HashMap<String, Double>();
		sortedmapngram = sortHashMapByValuesD(relatedworkrankmap);
		
		for(String key : sortedmapngram.keySet())
		{
			writer.println(key+"\t"+sortedmapngram.get(key));
		}
		
		HashMap<String, Double> sortedhighlightmap = new HashMap<String, Double>();
		sortedhighlightmap = sortHashMapByValuesD(relatedworkhighlightonlymap);
		
		for(String key : sortedhighlightmap.keySet())
		{
			highlightonlywriter.println(key+"\t"+sortedhighlightmap.get(key));
		}
		
		writer.close();
		highlightonlywriter.close();
	}
	
	public static void readPositiveHighlights(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(!fields[6].equals("SENTIMENT=POSITIVE"))
				continue;
			else
			{
				String highlighted = fields[7];
				
				if(highlighted.equals("highlight=null"))
					continue;
				
				String highlight = highlighted.substring(10, highlighted.length());
				
//				System.out.println(highlight);
				
				positivehighlightsarray.add(highlight);
			}
		}
		
		reader.close();
	}

	public static void readNotPositiveSentences(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(fields[6].equals("SENTIMENT=POSITIVE"))
				continue;
			
			String sentencefield = fields[8];
			String sentence = sentencefield.substring(9, sentencefield.length());
				
//			System.out.println(sentence);
			
			notpositivearray.add(sentence);
		}
		
		reader.close();
	}
	
	public static void extractPositiveHighlightNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_positive_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
		for(int i = 0; i < positivehighlightsarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = positivehighlightsarray.get(i).split(" ");
			
//			System.out.println(highlightsarray.get(i));
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				String ngram = "";
				
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				for(int m = j; m < j+n ; ++m)
				{
					
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		HashMap<String, Integer> sortedmapngram = new HashMap<String, Integer>();
		sortedmapngram = sortHashMapByValuesI(ngramsmap);
		
		for(String key : sortedmapngram.keySet())
		{
			positivehighlightmap.put(key, (double)sortedmapngram.get(key)/(double)total);
			writer.println(key+"\t"+sortedmapngram.get(key)+"\t"+(double)sortedmapngram.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void extractNotPositiveSentNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_not_positive_Sent.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
//		System.out.println(notrelatedworkarray.size());
		
		for(int i = 0; i < notpositivearray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = notpositivearray.get(i).split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
			
//			System.out.println();
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				String ngram = "";
				
				for(int m = j; m < j+n ; ++m)
				{
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		for(String key : ngramsmap.keySet())
		{
			notpositivemap.put(key, (double)ngramsmap.get(key)/(double)total);
			writer.println(key+"\t"+ngramsmap.get(key)+"\t"+(double)ngramsmap.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void rankPositiveNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n + "gram_data_positive_Rank.txt";
		String highlightonlyoutput = ngramsubdirectory + n + "gram_data_only_in_positive_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		PrintWriter highlightonlywriter = new PrintWriter(highlightonlyoutput, encoding);
		
		for(String key : positivehighlightmap.keySet())
		{
			if(!notpositivemap.keySet().contains(key))
			{
				positivehighlightonlymap.put(key, positivehighlightmap.get(key));
			}
			else
			{
				positiverankmap.put(key, positivehighlightmap.get(key)/notpositivemap.get(key));
			}
		}
		
		HashMap<String, Double> sortedmapngram = new HashMap<String, Double>();
		sortedmapngram = sortHashMapByValuesD(positiverankmap);
		
		for(String key : sortedmapngram.keySet())
		{
			writer.println(key+"\t"+sortedmapngram.get(key));
		}
		
		HashMap<String, Double> sortedhighlightmap = new HashMap<String, Double>();
		sortedhighlightmap = sortHashMapByValuesD(positivehighlightonlymap);
		
		for(String key : sortedhighlightmap.keySet())
		{
			highlightonlywriter.println(key+"\t"+sortedhighlightmap.get(key));
		}
		
		writer.close();
		highlightonlywriter.close();
	}
	
	public static void readSignificantHighlights(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(!fields[6].equals("SENTIMENT=SIGNIFICANT"))
				continue;
			else
			{
				String highlighted = fields[7];
				
				if(highlighted.equals("highlight=null"))
					continue;
				
				String highlight = highlighted.substring(10, highlighted.length());
				
//				System.out.println(highlight);
				
				significanthighlightsarray.add(highlight);
			}
		}
		
		reader.close();
	}

	public static void readNotSignificantSentences(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(fields[6].equals("SENTIMENT=SIGNIFICANT"))
				continue;
			
			String sentencefield = fields[8];
			String sentence = sentencefield.substring(9, sentencefield.length());
				
//			System.out.println(sentence);
			
			notsignificantarray.add(sentence);
		}
		
		reader.close();
	}
	
	public static void extractSignificantHighlightNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_significant_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
		for(int i = 0; i < significanthighlightsarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = significanthighlightsarray.get(i).split(" ");
			
//			System.out.println(highlightsarray.get(i));
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				String ngram = "";
				
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				for(int m = j; m < j+n ; ++m)
				{
					
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		HashMap<String, Integer> sortedmapngram = new HashMap<String, Integer>();
		sortedmapngram = sortHashMapByValuesI(ngramsmap);
		
		for(String key : sortedmapngram.keySet())
		{
			significanthighlightmap.put(key, (double)sortedmapngram.get(key)/(double)total);
			writer.println(key+"\t"+sortedmapngram.get(key)+"\t"+(double)sortedmapngram.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void extractNotSignificantSentNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_not_significant_Sent.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
//		System.out.println(notrelatedworkarray.size());
		
		for(int i = 0; i < notsignificantarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = notsignificantarray.get(i).split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
			
//			System.out.println();
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				String ngram = "";
				
				for(int m = j; m < j+n ; ++m)
				{
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		for(String key : ngramsmap.keySet())
		{
			notsignificantmap.put(key, (double)ngramsmap.get(key)/(double)total);
			writer.println(key+"\t"+ngramsmap.get(key)+"\t"+(double)ngramsmap.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void rankSignificantNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n + "gram_data_significant_Rank.txt";
		String highlightonlyoutput = ngramsubdirectory + n + "gram_data_only_in_significant_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		PrintWriter highlightonlywriter = new PrintWriter(highlightonlyoutput, encoding);
		
		for(String key : significanthighlightmap.keySet())
		{
			if(!notsignificantmap.keySet().contains(key))
			{
				significanthighlightonlymap.put(key, significanthighlightmap.get(key));
			}
			else
			{
				significantrankmap.put(key, significanthighlightmap.get(key)/notsignificantmap.get(key));
			}
		}
		
		HashMap<String, Double> sortedmapngram = new HashMap<String, Double>();
		sortedmapngram = sortHashMapByValuesD(significantrankmap);
		
		for(String key : sortedmapngram.keySet())
		{
			writer.println(key+"\t"+sortedmapngram.get(key));
		}
		
		HashMap<String, Double> sortedhighlightmap = new HashMap<String, Double>();
		sortedhighlightmap = sortHashMapByValuesD(significanthighlightonlymap);
		
		for(String key : sortedhighlightmap.keySet())
		{
			highlightonlywriter.println(key+"\t"+sortedhighlightmap.get(key));
		}
		
		writer.close();
		highlightonlywriter.close();
	}
	
	public static void readComparisonHighlights(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(!fields[6].equals("SENTIMENT=COMPARISON"))
				continue;
			else
			{
				String highlighted = fields[7];
				
				if(highlighted.equals("highlight=null"))
					continue;
				
				String highlight = highlighted.substring(10, highlighted.length());
				
//				System.out.println(highlight);
				
				comparisonhighlightsarray.add(highlight);
			}
		}
		
		reader.close();
	}

	public static void readNotComparisonSentences(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			if(fields[6].equals("SENTIMENT=COMPARISON"))
				continue;
			
			String sentencefield = fields[8];
			String sentence = sentencefield.substring(9, sentencefield.length());
				
//			System.out.println(sentence);
			
			notcomparisonarray.add(sentence);
		}
		
		reader.close();
	}
	
	public static void extractComparisonHighlightNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_comparison_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
		for(int i = 0; i < comparisonhighlightsarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = comparisonhighlightsarray.get(i).split(" ");
			
//			System.out.println(highlightsarray.get(i));
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				String ngram = "";
				
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				for(int m = j; m < j+n ; ++m)
				{
					
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		HashMap<String, Integer> sortedmapngram = new HashMap<String, Integer>();
		sortedmapngram = sortHashMapByValuesI(ngramsmap);
		
		for(String key : sortedmapngram.keySet())
		{
			comparisonhighlightmap.put(key, (double)sortedmapngram.get(key)/(double)total);
			writer.println(key+"\t"+sortedmapngram.get(key)+"\t"+(double)sortedmapngram.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void extractNotComparisonSentNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n +"gram_data_not_comparison_Sent.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		
		int total = 0;
		HashMap<String, Integer> ngramsmap = new HashMap<String, Integer>();
		
//		System.out.println(notrelatedworkarray.size());
		
		for(int i = 0; i < notcomparisonarray.size(); ++i)
		{
			//split the sentence by the space or other common punctuation
			String tokens [] = notcomparisonarray.get(i).split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
			
//			System.out.println();
			
			if(tokens.length < n)
				continue;
			
			for(int j = 0; j< tokens.length - (n - 1); ++j)
			{
				if(j == 0 && tokens[j].equals(""))
					continue;
				
				String ngram = "";
				
				for(int m = j; m < j+n ; ++m)
				{
					if(m == j)
						ngram = tokens[m];
					else
					{
						ngram += " " + tokens[m];
					}
				}
				
//				System.out.println(ngram);
			
				if(!ngramsmap.containsKey(ngram.toLowerCase()))
					ngramsmap.put(ngram.toLowerCase(), 1);
				else
				{
					int count = ngramsmap.get(ngram.toLowerCase());
					count += 1;
					ngramsmap.put(ngram.toLowerCase(),count);
				}
				
				++total;
			}
			
//			System.out.println("\n");
		}
		
		for(String key : ngramsmap.keySet())
		{
			notcomparisonmap.put(key, (double)ngramsmap.get(key)/(double)total);
			writer.println(key+"\t"+ngramsmap.get(key)+"\t"+(double)ngramsmap.get(key)/(double)total);
		}
		
		writer.close();
	}
	
	public static void rankComparisonNGrams(int n, String subdirectory) throws Exception
	{
		String ngramsubdirectory = subdirectory + n + "Grams/";
		
		boolean exists = (new File("ngramsubdirectory")).exists();
		
		if(!exists)
			new File(ngramsubdirectory).mkdir();
		
		String ngramsoutput = ngramsubdirectory + n + "gram_data_comparison_Rank.txt";
		String highlightonlyoutput = ngramsubdirectory + n + "gram_data_only_in_comparison_Highlight.txt";
		
		PrintWriter writer = new PrintWriter(ngramsoutput, encoding);
		PrintWriter highlightonlywriter = new PrintWriter(highlightonlyoutput, encoding);
		
		for(String key : comparisonhighlightmap.keySet())
		{
			if(!notcomparisonmap.keySet().contains(key))
			{
				comparisonhighlightonlymap.put(key, comparisonhighlightmap.get(key));
			}
			else
			{
				comparisonrankmap.put(key, comparisonhighlightmap.get(key)/notcomparisonmap.get(key));
			}
		}
		
		HashMap<String, Double> sortedmapngram = new HashMap<String, Double>();
		sortedmapngram = sortHashMapByValuesD(comparisonrankmap);
		
		for(String key : sortedmapngram.keySet())
		{
			writer.println(key+"\t"+sortedmapngram.get(key));
		}
		
		HashMap<String, Double> sortedhighlightmap = new HashMap<String, Double>();
		sortedhighlightmap = sortHashMapByValuesD(comparisonhighlightonlymap);
		
		for(String key : sortedhighlightmap.keySet())
		{
			highlightonlywriter.println(key+"\t"+sortedhighlightmap.get(key));
		}
		
		writer.close();
		highlightonlywriter.close();
	}
	
	static LinkedHashMap sortHashMapByValuesI(HashMap passedMap) 
	{
	    List mapKeys = new ArrayList(passedMap.keySet());
	    List mapValues = new ArrayList(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.sort(mapKeys);
	    
	    Collections.reverse(mapValues);
	    Collections.reverse(mapKeys);
	        
	    LinkedHashMap sortedMap = new LinkedHashMap();
	    
	    Iterator valueIt = mapValues.iterator();
	    
	    while (valueIt.hasNext()) 
	    {
	        Object val = valueIt.next();
	        Iterator keyIt = mapKeys.iterator();
	        
	        while (keyIt.hasNext()) 
	        {
	            Object key = keyIt.next();
	            String comp1 = passedMap.get(key).toString();
	            String comp2 = val.toString();
	            
	            if (comp1.equals(comp2))
	            {
	                passedMap.remove(key);
	                mapKeys.remove(key);
	                sortedMap.put((String)key, (Integer)val);
	                break;
	            }

	        }

	    }
	    return sortedMap;
	}
	
	static LinkedHashMap sortHashMapByValuesD(HashMap passedMap) 
	{
	    List mapKeys = new ArrayList(passedMap.keySet());
	    List mapValues = new ArrayList(passedMap.values());
	    Collections.sort(mapValues);
	    Collections.sort(mapKeys);
	    
	    Collections.reverse(mapValues);
	    Collections.reverse(mapKeys);
	        
	    LinkedHashMap sortedMap = new LinkedHashMap();
	    
	    Iterator valueIt = mapValues.iterator();
	    
	    while (valueIt.hasNext()) 
	    {
	        Object val = valueIt.next();
	        Iterator keyIt = mapKeys.iterator();
	        
	        while (keyIt.hasNext()) 
	        {
	            Object key = keyIt.next();
	            String comp1 = passedMap.get(key).toString();
	            String comp2 = val.toString();
	            
	            if (comp1.equals(comp2))
	            {
	                passedMap.remove(key);
	                mapKeys.remove(key);
	                sortedMap.put((String)key, (Double)val);
	                break;
	            }

	        }

	    }
	    return sortedMap;
	}
}
