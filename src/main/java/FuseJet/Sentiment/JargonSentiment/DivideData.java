//FOR MY ANNOTATION SPECIFICATIONS.
//This class is used to divide data for the n-fold cross validation.

package FuseJet.Sentiment.JargonSentiment;

import java.io.*;
import java.util.*;

public class DivideData 
{
	//The number of divided files; n in n-folder cross validation
	static final int number = 10;
	
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Mar_24_2012/";
	static String rawdata = home + "Annotations_mar_4_2012_citation_sentences_mar_23_2012.txt";
	
	static String trainingdirectory = home + "DividedFiles/TrainingFiles/";
	static String testingdirectory = home + "DividedFiles/TestingFiles/";
	static String answerkeydirectory = home + "DividedFiles/AnswerKeyFiles/";
	static String originaldirectory = home + "DividedFiles/OriginalDataFiles/";
	
	static String trainandtestdirectory = home + "/TrainAndTest/";
	
	//The number of files have been created
	static int filenumber = 1;

	//This arraylist contains all the lines of the raw data
	static ArrayList<String> linesarray = new ArrayList<String>();
	
	public static void main(String[] args) throws Exception
	{
		divideData();
	}
	
	public static void divideData() throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(rawdata), encoding));
		
		String line = "";
		int linesnumber = 0;
		
		while((line=reader.readLine())!=null)
		{
			++linesnumber;
			linesarray.add(line);
		}
		
		reader.close();
		
//		System.out.println(linesnumber);
		
		int range = linesnumber/number;
		int i = 0;
		
		for(; i<(number-1) ; ++i)
		{
			generateTrainingFile(i*range, (i+1)*range-1, filenumber);
			generateTestingFile(i*range, (i+1)*range-1, filenumber);
			generateAnswerkey(i*range, (i+1)*range-1, filenumber);
			generateOriginalFile(i*range, (i+1)*range-1, filenumber);
			System.out.println((i*range)+" -- "+ ((i+1)*range-1) + " -- "+filenumber);
			
			++filenumber;
		}
		
		generateTrainingFile((linesnumber-(linesnumber-i*range)), linesnumber-1, filenumber);
		generateTestingFile((linesnumber-(linesnumber-i*range)), linesnumber-1, filenumber);
		generateAnswerkey((linesnumber-(linesnumber-i*range)), linesnumber-1, filenumber);
		generateOriginalFile((linesnumber-(linesnumber-i*range)), linesnumber-1, filenumber);
		System.out.println((linesnumber-(linesnumber-i*range)) + " -- " + (linesnumber-1) + " -- " + filenumber);
	}
	
	/*
	 * generate the training file with the file number except the lines between variable start and variable end
	 */
	public static void generateTrainingFile(int start, int end, int filenamenumber) throws Exception
	{
		boolean exists = (new File("trainingdirectory")).exists();
		
		if(!exists)
			new File(trainingdirectory).mkdir();
		
		String filename = trainingdirectory + filenamenumber + ".training";
		
		boolean exists2 = (new File(trainandtestdirectory + "Test" + filenamenumber + "/")).exists();
		
		if(!exists2)
			new File(trainandtestdirectory + "Test" + filenamenumber + "/").mkdir();
		
		String trainandtestfilename = trainandtestdirectory + "Test" + filenamenumber + "/" + filenamenumber + ".training";
		
		PrintWriter writer = new PrintWriter(filename);
		PrintWriter trainandtestwriter = new PrintWriter(trainandtestfilename);
		
		for(int i=0; i<linesarray.size(); ++i)
		{
			if(i>=start && i<=end)
				continue;
			else
			{
				writer.println(linesarray.get(i));
				trainandtestwriter.println(linesarray.get(i));
			}
		}
		
		writer.close();
		trainandtestwriter.close();
	}
	
	/*
	 * generate the testing file with the file number with the lines just between variable start and variable end
	 */
	public static void generateTestingFile(int start, int end, int filenamenumber) throws Exception
	{
		boolean exists = (new File("testingdirectory")).exists();
		
		if(!exists)
			new File(testingdirectory).mkdir();
		
		String filename = testingdirectory + filenamenumber +".testing";
//		System.out.println(filename);
		String trainandtestfilename = trainandtestdirectory + "Test" + filenamenumber + "/" + filenamenumber + ".testing";
		
		PrintWriter writer = new PrintWriter(filename);
		PrintWriter trainandtestwriter = new PrintWriter(trainandtestfilename);

		for(int i = start; i <= end; ++i)
		{
			writer.println(linesarray.get(i));
			trainandtestwriter.println(linesarray.get(i));
		}
		
		writer.close();
		trainandtestwriter.close();
	}
	
	/*
	 * generate the answer key file with the file number with the lines just between variable start and variable end
	 */
	public static void generateAnswerkey(int start, int end, int filenamenumber) throws Exception
	{
		boolean exists = (new File("answerkeydirectory")).exists();
		
		if(!exists)
			new File(answerkeydirectory).mkdir();
		
		String filename = answerkeydirectory + filenamenumber +".answerkey";
//		System.out.println(filename);
		String trainandtestfilename = trainandtestdirectory + "Test" + filenamenumber + "/" + filenamenumber + ".answerkey";

		PrintWriter writer = new PrintWriter(filename);
		PrintWriter trainandtestwriter = new PrintWriter(trainandtestfilename);

		for(int i = start; i <= end; ++i)
		{
			String newline = "";
			String fields [] = linesarray.get(i).split("\\t");
		    
			newline = fields[6]+"\n"; 
			writer.print(newline);
			trainandtestwriter.print(newline);
		}
		
		writer.close();
		trainandtestwriter.close();
	}
	
	/*
	 * generate the original data file with the file number with the lines just between variable start and variable end
	 */
	public static void generateOriginalFile(int start, int end, int filenamenumber) throws Exception
	{
		boolean exists = (new File("originaldirectory")).exists();
		
		if(!exists)
			new File(originaldirectory).mkdir();
		
		String filename = originaldirectory + filenamenumber +".original";
//		System.out.println(filename);
		String trainandtestfilename = trainandtestdirectory + "Test" + filenamenumber + "/" + filenamenumber + ".original";

		PrintWriter writer = new PrintWriter(filename);
		PrintWriter trainandtestwriter = new PrintWriter(trainandtestfilename);
		
		for(int i = start; i <= end; ++i)
		{
			writer.println(linesarray.get(i));
			trainandtestwriter.println(linesarray.get(i));
		}
		
		writer.close();
		trainandtestwriter.close();
	}
}