//This class is used to generate the feature values of the following:
//Feature 10 : check whether the sentence contains some word/expression to indicate "significant"	
//Feature 11 : the distance between the citation and the "significant" word/expression

package FuseJet.Sentiment.JargonSentiment;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashSet;

public class SignificantKeyWordFeature 
{
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_21_2012/FeatureValues/";
	
	static String significantwordlistfile = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/WordList/SignificantWordList.txt";
	
	static String inputfile = home + "Annotations_mar_4_2012_citation_sentences_mar_23_2012.txt";
	static String outputfile = home + "significantfeaturevalue.txt";

	static HashSet<String> significantwordset = new HashSet<String>();

	public static void main(String[] args) throws Exception
	{
		readSignificantWordList();
		generateFeatureValues();
	}
	
	public static void readSignificantWordList() throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(significantwordlistfile), encoding));

		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			significantwordset.add(line);
		}
		
		reader.close();
	}

	public static void generateFeatureValues() throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		PrintWriter writer = new PrintWriter(outputfile, encoding);
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			int distance = Integer.MAX_VALUE;
			
			String fields [] = line.split("\\t");
			
			String citation = fields[3].replace("text=", "");
			
			String citationfields [] = citation.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\-|\\_|\\.\\.|\\.\\.\\.|\\t)+( )?");
			
			int citationindex = -1;
			int significantwordindex = -1;
			
			String sentencefield = fields[8];
			String sentence = sentencefield.replace("sentence=", "").toLowerCase();
			
			boolean contains = false;
			
			String words [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\-|\\_|\\.\\.|\\.\\.\\.|\\t)+( )?");
			
			for (int i = 0 ; i < words.length ; ++i)
			{
				if(significantwordset.contains(words[i]) || (i<(words.length-1) && significantwordset.contains(words[i]+" "+words[i+1])))
				{
					contains = true;
					significantwordindex = i;
//					System.out.println(words[i] + "\t" + line);
				}
				
				if(words[i].equalsIgnoreCase(citationfields[0]) && words[i+(citationfields.length-1)].equalsIgnoreCase(citationfields[citationfields.length-1]))
				{
					citationindex = i;
				}
			}
			
			if(contains)
			{
				distance = citationindex - significantwordindex;
//				writer.println("contains_significant_key_word=true");
				writer.println("contains_significant_key_word=true\t" + "distance_significant_key_word_citation=" + distance);
			}
			else
			{
//				writer.println("contains_significant_key_word=false");
				writer.println("contains_significant_key_word=false\tdistance_significant_key_word_citation=none");
			}
		}
		
		reader.close();
		writer.close();
	}	
}
