package FuseJet.Sentiment.JargonSentiment;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * User: yhe
 * Date: 11/5/12
 * Time: 11:33 AM
 */
public class PRFCalculator {
    private Map<String, int[]> record = new HashMap<String, int[]>();
    //int[0] = correctCount int[1] = predictCount int[2] = goldCount

    public Set<String> getLabels() {
        return record.keySet();
    }

    public void addData(String predictedLabel, String goldLabel) {
        if (predictedLabel.equals(goldLabel)) {
            if (!record.containsKey(predictedLabel)) {
                record.put(predictedLabel, new int[]{1, 1, 1});
            }
            else {
                int[] stat = record.get(predictedLabel);
                stat[0]++;stat[1]++;stat[2]++;
            }
        }
        else {
            if (!record.containsKey(predictedLabel)) {
                record.put(predictedLabel, new int[]{0, 1, 0});
            }
            else {
                int[] stat = record.get(predictedLabel);
                stat[1]++;
            }
            if (!record.containsKey(goldLabel)) {
                record.put(goldLabel, new int[]{0, 0, 1});
            }
            else {
                int[] stat = record.get(goldLabel);
                stat[2]++;
            }
        }
    }

    public double precisionFor(String label) {
        int[] stat = record.get(label);
        if (stat == null) return -1;
        if (stat[1] == 0) return -1;
        return (double)stat[0]/stat[1];
    }

    public double recallFor(String label) {
        int[] stat = record.get(label);
        if (stat == null) return -1;
        if (stat[2] == 0) return -1;
        return (double)stat[0]/stat[2];
    }

    public double fValueFor(String label) {
        double precision = precisionFor(label);
        double recall = recallFor(label);
        if ((precision == -1) || (recall == -1)) return -1;
        if ((precision + recall) == 0.0) {
            return -1;
        }
        return 2*precision*recall/(precision+recall);
    }

}
