//FOR MY ANNOTATION SPECIFICATIONS.
//This class is used to extract the sentences which contains the certain citations from the article.

package FuseJet.Sentiment.JargonSentiment;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class ExtractCitationSentence 
{
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Mar_23_2012/";

	static String annotationdirectory = home + "Annotations_mar_4_2012/";
	
	static String sentencesegmentationdirectory = home + "Annotations_mar_4_2012_sentence_segmentation/data/";
	
	static String outputfile = home + "Annotations_mar_4_2012_citation_sentences_mar_23_2012.txt";
	
	static PrintWriter writer; 
	
	static ArrayList<String> sentencearraylist;
	
	public static void main(String[] args) throws Exception
	{
		writer = new PrintWriter(outputfile);
		extractCitationSentences();
		writer.close();
	}
	
	public static void extractCitationSentences() throws Exception
	{
		File annotationfolder = new File(annotationdirectory);
		File [] annotationfiles = annotationfolder.listFiles();
	
		for(int i = 0; i < annotationfiles.length; ++i)
		{
			sentencearraylist = new ArrayList<String>();
			readSegmentedSentences(sentencesegmentationdirectory + annotationfiles[i].getName().replace(".xml", ".seg"));
			extractCitationSentencesFromFile(annotationfiles[i]);
//			System.out.println(annotationfiles[i].getName());
		}
	}
	
	public static void readSegmentedSentences(String segmentedfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(segmentedfile), encoding));
		
		String line = "";
		
		while((line=reader.readLine())!=null)
		{
			String [] words = line.split(" ");
			
			if(line.equals("") || words.length<=2)
				continue;
			sentencearraylist.add(line);
//			System.out.println(line);
		}
		
		reader.close();
	}

	public static void extractCitationSentencesFromFile(File annotationfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(annotationfile), encoding));
		
		String line = "";
		String filetext = "";
		String sentencetext = "";
		
		line=reader.readLine();
		line=reader.readLine();
		line=reader.readLine();
		
		while((line=reader.readLine())!=null)
		{				
			if(line.equalsIgnoreCase("]]></TEXT>"))
				break;
			
			filetext += line + "\n";
			
//			if(!line.equalsIgnoreCase("") && line.endsWith("."))
//				sentencetext += line + "\n";
		}

//		System.out.println(sentencetext);
//		
//		String [] sentences = sentencetext.split("[A-Z](.+)?\\. ");
//		
//		for(int i = 0 ; i <sentences.length; ++i )
//		{
//			System.out.println(sentences[i]);
//		}
		
//		System.out.println(filetext.substring(1580, 3758));
		
		line = reader.readLine();
		
		while((line=reader.readLine())!=null)
		{
			if(line.equalsIgnoreCase("</TAGS>"))
				break;
			
			Pattern pattern = Pattern.compile("<ENAMEX id=\"(.+)?\" start=\"(.+)?\" end=\"(.+)?\" text=\"(.+)?\" type=\"(.+)?\" ENTITY_ID=\"(.+)?\" SENTIMENT=\"(.+)?\" Highlights=\"(.+)?\" />");
			Matcher matcher = pattern.matcher(line);
			matcher.find();
			
			String id = matcher.group(1);
			String start = matcher.group(2);
			String end = matcher.group(3);
			String text = matcher.group(4);
			String type = matcher.group(5);
			String entityid = matcher.group(6);
			String sentiment = matcher.group(7);
			String highlight = matcher.group(8);
			
			String newline = "id="+id+"\tstart="+start+"\tend="+end+"\ttext="+text+"\ttype="+type+"\tENTITY_ID="+entityid+"\tSENTIMENT="+sentiment+"\thighlight="+highlight+"\t";
			
//			System.out.println(newline);
//			System.out.println(filetext.indexOf("ROS affect signal transduction pathways by directly or indirectly modifying signal molecules such as protein kinases, transcription factors, phospholipases, phosphatases, ion channels, and G proteins. 1"));
			
			for(int j = 0 ; j<sentencearraylist.size()-1; ++j)
			{	
				int startindex = filetext.indexOf(sentencearraylist.get(j));
				int endindex = filetext.indexOf(sentencearraylist.get(j+1))-1;
				
//				System.out.println(startindex + " -- " + endindex);
				
				if(startindex<0)
					continue;
				
				if((startindex-1 <= Integer.parseInt(start)) && (endindex >= Integer.parseInt(end)))
				{
					newline += "sentence=" + sentencearraylist.get(j);
					writer.println(newline);
					
//					System.out.println(startindex + " -- " + endindex + "\t" + start + " -- " + end);
					
					break;
				}
			}
		}
	
		
		reader.close();
	}
}
