//FOR MY ANNOTATION SPECIFICATIONS.
//This class is used to classify all the instances into "RELATED_WORK", "POSITIVE", "SIGNIFICANT" and "COMPARISON" through Bag-of-Words in Highlights.

package FuseJet.Sentiment.JargonSentiment;

import java.io.*;
import java.util.*;

public class HighlightsBagofWordsClassifier 
{
	//the top n grams in the other-only files.
	static int POSITIVETOPN = 100;
	static int SIGNIFICANTTOPN = 100;
	static int COMPARISONTOPN = 100;
	
	//the threshold to select the top-ranked grams in rank files. 
	static double POSITIVETHRESHOLD = 20.0;
	static double SIGNIFICANTTHRESHOLD = 20.0;
	static double COMPARISONTHRESHOLD = 20.0;
	
	//the threshold to consider the sentence as type X if the number of X-Facilitated-Grams in the sentence contains is more than X-COUNTTHREHOLD. 
	static int POSITIVECOUNTTHRESHOLD = 2;
	static int SIGNIFICANTCOUNTTHRESHOLD = 2;
	static int COMPARISONCOUNTTHRESHOLD = 2;
	
	//The number of divided files; n in n-folder cross validation
	static final int number = 10;
	
	//The max number of N-Grams
	static final int MAXN = 3;
	
	//directories
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_20_2012/TrainAndTest/";
	
	//this hash set stores all the unigrams of highlights in rank file
	static HashSet<String> positiveunigramsset;
	static HashSet<String> significantunigramsset;
	static HashSet<String> comparisonunigramsset;
	
	//this hash set stores all the bigrams of highlights in rank file
	static HashSet<String> positivebigramsset;
	static HashSet<String> significantbigramsset;
	static HashSet<String> comparisonbigramsset;
	
	//this hash set stores all the trigrams of highlights in rank file
	static HashSet<String> positivetrigramset;
	static HashSet<String> significanttrigramset;
	static HashSet<String> comparisontrigramset;
	
	//this hash map stores the answer keys for the input data
	static HashMap<String, String> answermap;
	
	//this hash map stores the predicted results for the input data
	static HashMap<String, String> resultmap;
	
	static int positivecorrect = 0;
	static int positivesystem = 0;
	static int positiveanswer = 68;
	
	static int comparisoncorrect = 0;
	static int comparisonsystem = 0;
	static int comparisonanswer = 31;
	
	static int significantcorrect = 0;
	static int significantsystem = 0;
	static int significantanswer = 8;
	
	static String wronginstancesfile = home + "wronginstancesfile.txt";
	static String correctinstancesfile = home + "correctinstancesfile.txt";
	
//	static String numberfile = home + "numbers.txt";
//	static PrintWriter numberwriter;
	
	static PrintWriter wrongwriter;
	static PrintWriter correctwriter;
	
	static int countcorrectpositive = 0;
	static int countcorrectsignificant = 0;
	static int countcorrectcomparison = 0;
	static int countcorrectrelatedwork = 0;
	
	static int total = 0;
	
	//count only predicted as POSTIVE, SIGNIFICANT, COMPARISON;
	static int subtotal = 0;
	
	public HighlightsBagofWordsClassifier(int ptopn, int stopn, int ctopn, double pthreshold, double sthreshold, double cthreshold, int pcount, int scount, int ccount) throws Exception
	{
		POSITIVETOPN = ptopn;
		SIGNIFICANTTOPN = stopn;
		COMPARISONTOPN = ctopn;
		
		POSITIVETHRESHOLD = pthreshold;
		SIGNIFICANTTHRESHOLD = sthreshold;
		COMPARISONTHRESHOLD = cthreshold;
		
		POSITIVECOUNTTHRESHOLD = pcount;
		SIGNIFICANTCOUNTTHRESHOLD = scount;
		COMPARISONCOUNTTHRESHOLD = ccount;
		
		String args [] = {};
		
		total = 0;
		
		countcorrectpositive = 0;
		countcorrectsignificant = 0;
		countcorrectcomparison = 0;
		countcorrectrelatedwork = 0;
		
		main(args);
	}
	
	public static void main(String args []) throws Exception
	{
		wrongwriter = new PrintWriter(wronginstancesfile, encoding);
		correctwriter = new PrintWriter(correctinstancesfile, encoding);
		
//		numberwriter = new PrintWriter(numberfile, encoding);
		
		for(int i = 1; i <= number; ++i)
		{
			String subdirectory = home + "Test" + i + "/";
			
//			System.out.println("Processing : " + subdirectory + " ... ");
			
			String testingfile = subdirectory + i + ".testing";

			positiveunigramsset = new HashSet<String>();
			positivebigramsset = new HashSet<String>();
			positivetrigramset = new HashSet<String>();
			
			significantunigramsset = new HashSet<String>();
			significantbigramsset = new HashSet<String>();
			significanttrigramset = new HashSet<String>();
			
			comparisonunigramsset = new HashSet<String>();
			comparisonbigramsset = new HashSet<String>();
			comparisontrigramset = new HashSet<String>();

			answermap = new HashMap<String, String>();
			resultmap = new HashMap<String, String>();
			
			readPositiveFacilitatedGrams(subdirectory);
			readSignificantFacilitatedGrams(subdirectory);
			readComparisonFacilitatedGrams(subdirectory);
			
			readData(testingfile);
			classify();
		}
		
		double positiveprecision = (double)positivecorrect / (double)positivesystem;
		double positiverecall = (double)positivecorrect / (double)positiveanswer;
		double positivefscore = 2*positiveprecision*positiverecall / (positiveprecision + positiverecall);
		
		System.out.println("Number of correct predictions for positive : " + positivecorrect);
		System.out.println("Number of predictions for positive : " + positivesystem);
		System.out.println("Positive Precision : " + positiveprecision);
		System.out.println("Positive Recall : " + positiverecall);
		System.out.println("Positive F-measure : " + positivefscore);
		
		double comparisonprecision = (double)comparisoncorrect / (double)comparisonsystem;
		double comparisonrecall = (double)comparisoncorrect / (double)comparisonanswer;
		double comparisonfscore = 2*comparisonprecision*comparisonrecall / (comparisonprecision + comparisonrecall);
		
		System.out.println("\nNumber of correct predictions for comparison  : " + comparisoncorrect);
		System.out.println("Number of predictions for comparison : " + comparisonsystem);
		System.out.println("Comparison Precision : " + comparisonprecision);
		System.out.println("Comparison Recall : " + comparisonrecall);
		System.out.println("Comparison F-measure : " + comparisonfscore);
		
		double significantprecision = (double)significantcorrect / (double)significantsystem;
		double significantrecall = (double)significantcorrect / (double)significantanswer;
		double significantfscore = 2*significantprecision*significantrecall / (significantprecision + significantrecall);
		
		System.out.println("\nNumber of correct predictions for significant : " + significantcorrect);
		System.out.println("Number of predictions for significant : " + significantsystem);
		System.out.println("Significant Precision : " + significantprecision);
		System.out.println("Significant Recall : " + significantrecall);
		System.out.println("Significant F-measure : " + significantfscore);
		
//		System.out.println("\nNumber of correct predictions for related_work  : " + countcorrectrelatedwork);
//		System.out.println("Total : " + total);
//		System.out.println("Precision : " + (double)(countcorrectpositive + countcorrectsignificant + countcorrectcomparison + countcorrectrelatedwork)/(double)total);
		
		double precision = (double)(countcorrectpositive+countcorrectsignificant+countcorrectcomparison)/(double)(subtotal);
		double recall = (double)(countcorrectpositive+countcorrectsignificant+countcorrectcomparison)/(double)(68+31+8);
		double fmeasure = 2*precision*recall/(precision+recall);
		
		System.out.println("\nNumber of correct predictions : " + (countcorrectpositive+countcorrectsignificant+countcorrectcomparison));
		System.out.println("Number of total predictions : " + subtotal);
		System.out.println("Precision : " + precision + "\tRecall : " + recall + "\tF-measure : " + fmeasure);
		
		wrongwriter.close();
		correctwriter.close();
		
//		numberwriter.close();
	}
	
	public static void readPositiveFacilitatedGrams(String subdirectory) throws Exception
	{	
		String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_positive_Highlight.txt";
		String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_positive_Highlight.txt";
		String trigramotheronlyinput = subdirectory+ "3Grams/3gram_data_only_in_positive_Highlight.txt";
		
		String unigramrankinput = subdirectory + "1Grams/1gram_data_positive_Rank.txt";
		String bigramrankinput = subdirectory + "2Grams/2gram_data_positive_Rank.txt";
		String trigramrankinput = subdirectory + "3Grams/3gram_data_positive_Rank.txt";
		
		BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
		BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
		BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));
		
		BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
		BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
		BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

		String line = "";
		int linenumber = 1;
		while((line = unigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= POSITIVETOPN)
			{
				String fields [] = line.split("\\t");
				positiveunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = bigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= POSITIVETOPN)
			{
				String fields [] = line.split("\\t");
				positivebigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = trigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= POSITIVETOPN)
			{
				String fields [] = line.split("\\t");
				positivetrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		
		while((line = unigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD)
			{
				positiveunigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = bigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD)
			{
				positivebigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = trigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= POSITIVETHRESHOLD)
			{
				positivetrigramset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		unigramotheronlyreader.close();
		bigramotheronlyreader.close();
		trigramotheronlyreader.close();
		
		unigramrankreader.close();
		bigramrankreader.close();
		trigramrankreader.close();
	}

	public static void readSignificantFacilitatedGrams(String subdirectory) throws Exception
	{	
		String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_significant_Highlight.txt";
		String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_significant_Highlight.txt";
		String trigramotheronlyinput = subdirectory+ "3Grams/3gram_data_only_in_significant_Highlight.txt";
		
		String unigramrankinput = subdirectory + "1Grams/1gram_data_significant_Rank.txt";
		String bigramrankinput = subdirectory + "2Grams/2gram_data_significant_Rank.txt";
		String trigramrankinput = subdirectory + "3Grams/3gram_data_significant_Rank.txt";
		
		BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
		BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
		BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));
		
		BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
		BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
		BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

		String line = "";
		int linenumber = 1;
		while((line = unigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= SIGNIFICANTTOPN)
			{
				String fields [] = line.split("\\t");
				significantunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = bigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= SIGNIFICANTTOPN)
			{
				String fields [] = line.split("\\t");
				significantbigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = trigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= SIGNIFICANTTOPN)
			{
				String fields [] = line.split("\\t");
				significanttrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		
		while((line = unigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD)
			{
				significantunigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = bigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD)
			{
				significantbigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = trigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= SIGNIFICANTTHRESHOLD)
			{
				significanttrigramset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		unigramotheronlyreader.close();
		bigramotheronlyreader.close();
		trigramotheronlyreader.close();
		
		unigramrankreader.close();
		bigramrankreader.close();
		trigramrankreader.close();
	}
	
	public static void readComparisonFacilitatedGrams(String subdirectory) throws Exception
	{	
		String unigramotheronlyinput = subdirectory + "1Grams/1gram_data_only_in_comparison_Highlight.txt";
		String bigramotheronlyinput = subdirectory + "2Grams/2gram_data_only_in_comparison_Highlight.txt";
		String trigramotheronlyinput = subdirectory+ "3Grams/3gram_data_only_in_comparison_Highlight.txt";
		
		String unigramrankinput = subdirectory + "1Grams/1gram_data_comparison_Rank.txt";
		String bigramrankinput = subdirectory + "2Grams/2gram_data_comparison_Rank.txt";
		String trigramrankinput = subdirectory + "3Grams/3gram_data_comparison_Rank.txt";
		
		BufferedReader unigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramotheronlyinput), encoding));
		BufferedReader bigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramotheronlyinput), encoding));
		BufferedReader trigramotheronlyreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramotheronlyinput), encoding));
		
		BufferedReader unigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(unigramrankinput), encoding));
		BufferedReader bigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(bigramrankinput), encoding));
		BufferedReader trigramrankreader = new BufferedReader(new InputStreamReader(new FileInputStream(trigramrankinput), encoding));

		String line = "";
		int linenumber = 1;
		while((line = unigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= COMPARISONTOPN)
			{
				String fields [] = line.split("\\t");
				comparisonunigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = bigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= COMPARISONTOPN)
			{
				String fields [] = line.split("\\t");
				comparisonbigramsset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		linenumber = 1;
		
		while((line = trigramotheronlyreader.readLine())!=null)
		{
			if(linenumber <= COMPARISONTOPN)
			{
				String fields [] = line.split("\\t");
				comparisontrigramset.add(fields[0]);
//				System.out.println(linenumber + fields[0]);
				++linenumber;
			}
			else
				break;
		}
		
		line = "";
		
		while((line = unigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD)
			{
				comparisonunigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = bigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD)
			{
				comparisonbigramsset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		line = "";
		
		while((line = trigramrankreader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			if(Double.parseDouble(fields[1]) >= COMPARISONTHRESHOLD)
			{
				comparisontrigramset.add(fields[0]);
//				System.out.println(line);
			}
		}
		
		unigramotheronlyreader.close();
		bigramotheronlyreader.close();
		trigramotheronlyreader.close();
		
		unigramrankreader.close();
		bigramrankreader.close();
		trigramrankreader.close();
	}
	
	public static void readData(String inputfile) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			++total;
			String fields [] = line.split("\\t");
			
			if(resultmap.containsKey(line))
				System.out.println(line);
			
			answermap.put(line, fields[6]);
			resultmap.put(line, "none");
		}
		
		reader.close();
	}
	
	public static void classify() throws Exception
	{			
		int positivepredictionnum = 0;
		int significantpredictionnum = 0;
		int comparisonpredictionnum = 0;
		int relatedworkpredictionnum = 0;
		
		int positivecorrectpredictionnum = 0;
		int significantcorrectpredictionnum = 0;
		int comparisoncorrectpredictionnum = 0;
		int relatedworkcorrectpredictionnum = 0;
		
		for(String key : resultmap.keySet())
		{
			int positivecount = 0;
			int significantcount = 0;
			int comparisoncount = 0;
			
			String fields [] = key.split("\\t");
			String sentencefield = fields[8];
			String sentence = sentencefield.substring(9, sentencefield.length());
			
//			System.out.println(sentence);
			
			positivecount = countPositiveFacilitatedGrams(sentence);
			significantcount = countSignificantFacilitatedGrams(sentence);
			comparisoncount = countComparisonFacilitatedGrams(sentence);
			
			if(positivecount >= POSITIVECOUNTTHRESHOLD)
			{
				resultmap.put(key, "SENTIMENT=POSITIVE");
				++positivepredictionnum;
				++positivesystem;
				++subtotal;
				
				if(answermap.get(key).equals("SENTIMENT=POSITIVE"))
				{
					++countcorrectpositive;
					++positivecorrect;
					++positivecorrectpredictionnum;
					correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
				}
				else
				{
					wrongwriter.println(key);
				}
			}
			else if(significantcount >= SIGNIFICANTCOUNTTHRESHOLD)
			{
				resultmap.put(key, "SENTIMENT=SIGNIFICANT");
				++significantpredictionnum;
				++significantsystem;
				++subtotal;
				
				if(answermap.get(key).equals("SENTIMENT=SIGNIFICANT"))
				{
					++countcorrectsignificant;
					++significantcorrect;
					++significantcorrectpredictionnum;
					correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
				}
				else
				{
					wrongwriter.println(key);
				}
			}
			else if(comparisoncount >= COMPARISONCOUNTTHRESHOLD)
			{
				resultmap.put(key, "SENTIMENT=COMPARISON");
				++comparisonpredictionnum;
				++comparisonsystem;
				++subtotal;
				
				if(answermap.get(key).equals("SENTIMENT=COMPARISON"))
				{
					++countcorrectcomparison;
					++comparisoncorrectpredictionnum;
					++comparisoncorrect;
					correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Other : " + count);
				}
				else
				{
					wrongwriter.println(key);
				}
			}
			else
			{
				resultmap.put(key, "SENTIMENT=RELATED_WORK");
				++relatedworkpredictionnum;
				
				if(answermap.get(key).equals("SENTIMENT=RELATED_WORK"))
				{
					++countcorrectrelatedwork;
					++relatedworkcorrectpredictionnum;
					correctwriter.println(key);
//					printngrams(sentence);
//					System.out.println("Assert : " + count);
				}
				else
				{
					wrongwriter.println(key);
				}
			}
		}
		
//		System.out.println(positivecorrectpredictionnum + "\t" + positivepredictionnum + "\t\t" + significantcorrectpredictionnum + "\t" + significantpredictionnum + 
//				"\t\t" + comparisoncorrectpredictionnum + "\t" + comparisonpredictionnum + "\t\t" + relatedworkcorrectpredictionnum + "\t" + relatedworkpredictionnum);

	}
	
	public static int countPositiveFacilitatedGrams(String sentence) throws Exception
	{
		int unigramcount = 0;
		int bigramcount = 0;
		int trigramcount = 0;
		
		String tokens [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
		
		ArrayList<String> unigramarray = new ArrayList<String>();
		ArrayList<String> bigramarray = new ArrayList<String>();
		ArrayList<String> trigramarray = new ArrayList<String>();
		
		for(int i = 0; i < tokens.length; ++i)
		{
			String unigram = tokens[i];
			unigramarray.add(unigram);
		}
		
		for(int i = 0 ; i < tokens.length-1; ++i)
		{
			String bigram = tokens[i]+" "+tokens[i+1];
			bigramarray.add(bigram);
		}
		
		for(int i = 0 ; i < tokens.length-2; ++i)
		{
			String tribgram = tokens[i] + " " + tokens[i+1] + " " + tokens[i+2];
			trigramarray.add(tribgram);
		}
		
		for(int i = 0; i < unigramarray.size(); ++i)
		{
			if(positiveunigramsset.contains(unigramarray.get(i)))
			{
				++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < bigramarray.size(); ++i)
		{
			if(positivebigramsset.contains(bigramarray.get(i)))
			{
				++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < trigramarray.size(); ++i)
		{
			if(positivetrigramset.contains(trigramarray.get(i)))
			{
				++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
//		return (unigramcount + bigramcount + trigramcount);
		return (bigramcount + trigramcount);
//		return (unigramcount);
	}
	
	public static int countSignificantFacilitatedGrams(String sentence) throws Exception
	{
		int unigramcount = 0;
		int bigramcount = 0;
		int trigramcount = 0;
		
		String tokens [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
		
		ArrayList<String> unigramarray = new ArrayList<String>();
		ArrayList<String> bigramarray = new ArrayList<String>();
		ArrayList<String> trigramarray = new ArrayList<String>();
		
		for(int i = 0; i < tokens.length; ++i)
		{
			String unigram = tokens[i];
			unigramarray.add(unigram);
		}
		
		for(int i = 0 ; i < tokens.length-1; ++i)
		{
			String bigram = tokens[i]+" "+tokens[i+1];
			bigramarray.add(bigram);
		}
		
		for(int i = 0 ; i < tokens.length-2; ++i)
		{
			String tribgram = tokens[i] + " " + tokens[i+1] + " " + tokens[i+2];
			trigramarray.add(tribgram);
		}
		
		for(int i = 0; i < unigramarray.size(); ++i)
		{
			if(significantunigramsset.contains(unigramarray.get(i)))
			{
				++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < bigramarray.size(); ++i)
		{
			if(significantbigramsset.contains(bigramarray.get(i)))
			{
				++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < trigramarray.size(); ++i)
		{
			if(significanttrigramset.contains(trigramarray.get(i)))
			{
				++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
//		return (unigramcount + bigramcount + trigramcount);
		return (bigramcount + trigramcount);
//		return (unigramcount);
	}
	
	public static int countComparisonFacilitatedGrams(String sentence) throws Exception
	{
		int unigramcount = 0;
		int bigramcount = 0;
		int trigramcount = 0;
		
		String tokens [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
		
		ArrayList<String> unigramarray = new ArrayList<String>();
		ArrayList<String> bigramarray = new ArrayList<String>();
		ArrayList<String> trigramarray = new ArrayList<String>();
		
		for(int i = 0; i < tokens.length; ++i)
		{
			String unigram = tokens[i];
			unigramarray.add(unigram);
		}
		
		for(int i = 0 ; i < tokens.length-1; ++i)
		{
			String bigram = tokens[i]+" "+tokens[i+1];
			bigramarray.add(bigram);
		}
		
		for(int i = 0 ; i < tokens.length-2; ++i)
		{
			String tribgram = tokens[i] + " " + tokens[i+1] + " " + tokens[i+2];
			trigramarray.add(tribgram);
		}
		
		for(int i = 0; i < unigramarray.size(); ++i)
		{
			if(comparisonunigramsset.contains(unigramarray.get(i)))
			{
				++unigramcount;
//				System.out.print(unigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < bigramarray.size(); ++i)
		{
			if(comparisonbigramsset.contains(bigramarray.get(i)))
			{
				++bigramcount;
//				System.out.print(bigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < trigramarray.size(); ++i)
		{
			if(comparisontrigramset.contains(trigramarray.get(i)))
			{
				++trigramcount;
//				System.out.print(trigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
//		return (unigramcount + bigramcount + trigramcount);
		return (bigramcount + trigramcount);
//		return (unigramcount);
	}
	
	public static void printngrams(String sentence) throws Exception
	{
		int unigramcount = 0;
		int bigramcount = 0;
		int trigramcount = 0;
		
		String tokens [] = sentence.split("( )?( |\\,|\\?|\\;|\\!|\"|\\'\\'|\\`\\`|\\(|\\)|\\_|\\t)+( )?");
		
		ArrayList<String> unigramarray = new ArrayList<String>();
		ArrayList<String> bigramarray = new ArrayList<String>();
		ArrayList<String> trigramarray = new ArrayList<String>();
		
		for(int i = 0; i < tokens.length; ++i)
		{
			String unigram = tokens[i];
			unigramarray.add(unigram);
		}
		
		for(int i = 0 ; i < tokens.length-1; ++i)
		{
			String bigram = tokens[i]+" "+tokens[i+1];
			bigramarray.add(bigram);
		}
		
		for(int i = 0 ; i < tokens.length-2; ++i)
		{
			String tribgram = tokens[i] + " " + tokens[i+1] + " " + tokens[i+2];
			trigramarray.add(tribgram);
		}
		
		for(int i = 0; i < unigramarray.size(); ++i)
		{
			if(positiveunigramsset.contains(unigramarray.get(i)))
			{
				++unigramcount;
				correctwriter.print(unigramarray.get(i) + " | ");
				System.out.print(unigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < bigramarray.size(); ++i)
		{
			if(positivebigramsset.contains(bigramarray.get(i)))
			{
				++bigramcount;
				correctwriter.print(bigramarray.get(i) + " | ");
				System.out.print(bigramarray.get(i)+"\t");
			}
		}
		
//		System.out.print("\n");
		
		for(int i = 0; i < trigramarray.size(); ++i)
		{
			if(positivetrigramset.contains(trigramarray.get(i)))
			{
				++trigramcount;
				correctwriter.print(trigramarray.get(i) + " | ");
				System.out.print(trigramarray.get(i)+"\t");
			}
		}
		
		correctwriter.print("\n");
//		System.out.print("\n");
	}
}
