//This class is used to analyze the predicated results by comparing the predicted results with the answer key.

package FuseJet.Sentiment.JargonSentiment;

import java.util.*;
import java.io.*;
import java.util.regex.*;

public class AnalyzeMaxEntResult 
{

	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_21_2012/MaxEnt/TrainAndTest/";
	
	static String wronginstancesfile = home + "wronginstancesfile.txt";
	static String correctinstancesfile = home + "correctinstancesfile.txt";
	
	static int positivecorrect = 0;
	static int positivesystem = 0;
	static int positiveanswer = 68;
	
	static int comparisoncorrect = 0;
	static int comparisonsystem = 0;
	static int comparisonanswer = 31;
	
	static int significantcorrect = 0;
	static int significantsystem = 0;
	static int significantanswer = 8;
	
	//The number of divided files; n in n-folder cross validation
	static final int number = 10;
	
	static int countcorrectpositive = 0;
	static int countcorrectsignificant = 0;
	static int countcorrectcomparison = 0;
	static int countcorrectrelatedwork = 0;
	
	static int correctnumb = 0;
	static int systemtotal = 0;
	static int answertotal = 0;
	
	public AnalyzeMaxEntResult() throws Exception
	{
		countcorrectpositive = 0;
		countcorrectsignificant = 0;
		countcorrectcomparison = 0;
		countcorrectrelatedwork = 0;
		
		correctnumb = 0;
		systemtotal = 0;
		answertotal = 0;
				
		String [] args ={}; 
		
		main(args);
	}
	
	public static void main(String[] args) throws Exception
	{
		analyze();
	}

	public static void analyze() throws Exception
	{
		correctnumb = 0;
		
		countcorrectpositive = 0;
		countcorrectsignificant = 0;
		countcorrectcomparison = 0;
		countcorrectrelatedwork = 0;
		
		systemtotal = 0;
		answertotal = 0;
		
		PrintWriter wrongwriter = new PrintWriter(wronginstancesfile, encoding);
		PrintWriter correctwriter = new PrintWriter(correctinstancesfile, encoding);
		
		for(int i = 1; i<=number;++i)
		{	
			ArrayList<String> answerarray = new ArrayList<String>();
			ArrayList<String> resultarray = new ArrayList<String>();
			ArrayList<String> originalarray = new ArrayList<String>();
			
			String subdirectory = home + "Test" + i + "/";
			
			String answerfilename = subdirectory + i + ".answerkey";
			String resultfilename = subdirectory + i + ".result";
			String originalfilename = subdirectory + i + ".original";
			
			BufferedReader answerreader = new BufferedReader(new InputStreamReader(new FileInputStream(answerfilename), encoding));
			BufferedReader resultreader = new BufferedReader(new InputStreamReader(new FileInputStream(resultfilename), encoding));
			BufferedReader originalreader = new BufferedReader(new InputStreamReader(new FileInputStream(originalfilename), encoding));

			String originalline = "";
			
			while((originalline = originalreader.readLine())!=null)
			{
				originalarray.add(originalline);
			}
			
			String line = "";
			
			while((line = answerreader.readLine())!=null)
			{
				answerarray.add(line);
			}
			
			line = "";
			
			while((line = resultreader.readLine())!=null)
			{	
				Pattern relatedworkpattern = Pattern.compile("SENTIMENT=RELATED_WORK\\[(0\\.\\d*)\\]");
				Pattern positivepattern = Pattern.compile("SENTIMENT=POSITIVE\\[(0\\.\\d*)\\]");
				Pattern significantpattern = Pattern.compile("SENTIMENT=SIGNIFICANT\\[(0\\.\\d*)\\]");
				Pattern comparisonpattern = Pattern.compile("SENTIMENT=COMPARISON\\[(0\\.\\d*)\\]");
				
				Matcher relatedworkmatcher = relatedworkpattern.matcher(line);
				Matcher positivematcher = positivepattern.matcher(line);
				Matcher significantmatcher = significantpattern.matcher(line);
				Matcher comparisonmatcher = comparisonpattern.matcher(line);
				
				relatedworkmatcher.find();
				positivematcher.find();
				significantmatcher.find();
				comparisonmatcher.find();
				
				double relatedworkprob = Double.parseDouble(relatedworkmatcher.group(1));
				double positiveprob = Double.parseDouble(positivematcher.group(1));
				double significantprob = Double.parseDouble(significantmatcher.group(1));
				double comparisonprob = Double.parseDouble(comparisonmatcher.group(1));
				
//				System.out.println(line);
//				System.out.println("R = " + relatedworkprob + "\tP = " + positiveprob + "\tS = " + significantprob + "\tC = " + comparisonprob);
				
//				if(positiveprob>significantprob && positiveprob>comparisonprob && positiveprob>=0.2)
//					resultarray.add("SENTIMENT=POSITIVE");
//				else if(comparisonprob>positiveprob && comparisonprob>significantprob && comparisonprob>=0.7)
//					resultarray.add("SENTIMENT=COMPARISON");
//				else if(significantprob>positiveprob && significantprob>comparisonprob && significantprob>=0.7)
//					resultarray.add("SENTIMENT=SIGNIFICANT");
//				else
//				{
					if(positiveprob>significantprob && positiveprob>comparisonprob && positiveprob>relatedworkprob)
						resultarray.add("SENTIMENT=POSITIVE");
					else if(comparisonprob>positiveprob && comparisonprob>significantprob && comparisonprob>relatedworkprob)
						resultarray.add("SENTIMENT=COMPARISON");
					else if(significantprob>positiveprob && significantprob>comparisonprob && significantprob>relatedworkprob)
						resultarray.add("SENTIMENT=SIGNIFICANT");
					else
						resultarray.add("SENTIMENT=RELATED_WORK");
//				}
				
//				resultarray.add(line);
			}
			
			for(int j = 0 ; j < resultarray.size() ; ++j)
			{
				if(resultarray.get(j).equalsIgnoreCase("SENTIMENT=POSITIVE"))
				{
					++positivesystem;
					++systemtotal;
					
					if(answerarray.get(j).equalsIgnoreCase("SENTIMENT=POSITIVE"))
					{
						++countcorrectpositive;
						++positivecorrect;
					}
					else
					{
						wrongwriter.println("SENTIMENT=POSITIVE\t" + originalarray.get(j));
					}
				}
				else if(resultarray.get(j).equalsIgnoreCase("SENTIMENT=COMPARISON"))
				{
					++comparisonsystem;
					++systemtotal;
					
					if(answerarray.get(j).equalsIgnoreCase("SENTIMENT=COMPARISON"))
					{
						++countcorrectcomparison;
						++comparisoncorrect;
					}
					else
					{
						wrongwriter.println("SENTIMENT=COMPARISON\t" + originalarray.get(j));
					}
				}
				else if(resultarray.get(j).equalsIgnoreCase("SENTIMENT=SIGNIFICANT"))
				{
					++significantsystem;
					++systemtotal;
					
					if(answerarray.get(j).equalsIgnoreCase("SENTIMENT=SIGNIFICANT"))
					{
						++significantcorrect;
						++countcorrectsignificant;
					}
					else
					{
						wrongwriter.println("SENTIMENT=SIGNIFICANT\t" + originalarray.get(j));
					}
				}
			}
			
			answerreader.close();
			resultreader.close();
			originalreader.close();
		}
		
		double positiveprecision = (double)positivecorrect / (double)positivesystem;
		double positiverecall = (double)positivecorrect / (double)positiveanswer;
		double positivefscore = 2*positiveprecision*positiverecall / (positiveprecision + positiverecall);
		
		System.out.println("Number of correct predictions for positive : " + positivecorrect);
		System.out.println("Number of predictions for positive : " + positivesystem);
		System.out.println("Positive Precision : " + positiveprecision);
		System.out.println("Positive Recall : " + positiverecall);
		System.out.println("Positive F-measure : " + positivefscore);
		
		double comparisonprecision = (double)comparisoncorrect / (double)comparisonsystem;
		double comparisonrecall = (double)comparisoncorrect / (double)comparisonanswer;
		double comparisonfscore = 2*comparisonprecision*comparisonrecall / (comparisonprecision + comparisonrecall);
		
		System.out.println("\nNumber of correct predictions for comparison  : " + comparisoncorrect);
		System.out.println("Number of predictions for comparison : " + comparisonsystem);
		System.out.println("Comparison Precision : " + comparisonprecision);
		System.out.println("Comparison Recall : " + comparisonrecall);
		System.out.println("Comparison F-measure : " + comparisonfscore);
		
		double significantprecision = (double)significantcorrect / (double)significantsystem;
		double significantrecall = (double)significantcorrect / (double)significantanswer;
		double significantfscore = 2*significantprecision*significantrecall / (significantprecision + significantrecall);
		
		System.out.println("\nNumber of correct predictions for significant : " + significantcorrect);
		System.out.println("Number of predictions for significant : " + significantsystem);
		System.out.println("Significant Precision : " + significantprecision);
		System.out.println("Significant Recall : " + significantrecall);
		System.out.println("Significant F-measure : " + significantfscore);
		
		double precision = (double)(countcorrectpositive+countcorrectsignificant+countcorrectcomparison)/(double)(systemtotal);
		double recall = (double)(countcorrectpositive+countcorrectsignificant+countcorrectcomparison)/(double)(68+31+8);
		double fmeasure = 2*precision*recall/(precision+recall);
		
		System.out.println("\nNumber of correct predictions : " + (countcorrectpositive+countcorrectsignificant+countcorrectcomparison));
		System.out.println("Number of total predictions : " + systemtotal);
		System.out.println("Precision : " + precision + "\tRecall : " + recall + "\tF-measure : " + fmeasure);
		
		wrongwriter.close();
		correctwriter.close();
	}
}
