package FuseJet.Sentiment.JargonSentiment;

import opennlp.maxent.GIS;
import opennlp.maxent.GISModel;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.*;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 10/30/12
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class JargonSentiment {

    private static GISModel trainedModel = null;

    private PorterStemmer stemmer = new PorterStemmer();

    public List<Event> readData(String fileName) {
        List<Event> contextDataList = new ArrayList<Event>();

        try {
            BufferedReader r = new BufferedReader(new FileReader(fileName));
            String line = null;
            while ((line = r.readLine()) != null) {
                Event datum = readDatumFromString(line);
                if (datum != null) {
                    contextDataList.add(datum);
                }
            }
        }
        catch (IOException e) {
            System.err.println("Error reading file:" + fileName);
        }
        return contextDataList;
    }

    public void trainModel(List<Event> trainingData, String modelFileName) {
        try {
            //OnePassDataIndexer di = new OnePassDataIndexer(new ListEventStream(trainingData), 1);
            GISModel model = GIS.trainModel(new ListEventStream(trainingData), 200, 1);
            File outputFile = new File(modelFileName);
            GISModelWriter w = new SuffixSensitiveGISModelWriter(model, outputFile);
            w.persist();
            w.close();
        }
        catch (Exception e) {
            System.err.println("Unable to train model. Stacktrace:");
            e.printStackTrace();
        }
    }

    public void loadModel(String modelFileName) {
        try {
            trainedModel =  (GISModel)new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
        }
        catch (IOException e) {
            System.err.println("Error loading model:" + modelFileName);
            e.printStackTrace();
        }
    }

    public String predict(String[] context) {
        double[] outcomes = trainedModel.eval(context);
        return trainedModel.getBestOutcome(outcomes);
    }

    public void test(List<Event> testData) {
        Map<String, Integer> labelMap = new HashMap<String, Integer>();
        PRFCalculator calc = new PRFCalculator();
        int count = 0;
        int correct = 0;
        for (Event testDatum : testData) {
            String trueLabel = testDatum.getOutcome();
            String predictedLabel = predict(testDatum.getContext());
            calc.addData(predictedLabel, trueLabel);
            if (trueLabel.equals(predictedLabel)) {
                if (labelMap.containsKey(trueLabel)) {
                    labelMap.put(trueLabel, labelMap.get(trueLabel)+1);
                }
                else {
                    labelMap.put(trueLabel, 1);
                }
                correct++;
            }
            count++;
        }
        System.out.println("Accuracy:" + (double)correct/count);
        System.out.println("Predicted labels:");
        for (String label : labelMap.keySet()) {
            System.out.println(label + ":" + labelMap.get(label));
        }
        for (String label : calc.getLabels()) {
            System.out.println(label + " PREC:" +
            (calc.precisionFor(label) == -1 ? "N/A" : calc.precisionFor(label)) +
            " REC:" +
            (calc.recallFor(label) == -1 ? "N/A" : calc.recallFor(label)) +
            " F value:" +
            (calc.fValueFor(label) == -1 ? "N/A" : calc.fValueFor(label)));
        }
    }

    private Event readDatumFromString(String s) {
        String[] parts = s.split("\\|\\|\\|");
        String label = parts[3].substring("subtype:".length());
        String text = parts[4].substring("text:".length()).trim()+" ";
        String[] seString = parts[1].substring("ann:".length()).split("\\-");
        int start = Integer.valueOf(seString[0]);
        int end = Integer.valueOf(seString[1]);
        char[] chars = text.toCharArray();
        int i = 0;
        List<String> contextList = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        int afterEnd = -1;
        while (i < text.length()) {
            if (i == start) {
                if (sb.length() > 0) {
                    contextList.add(sb.toString());
                }
                int j = contextList.size();
                j = j > 3 ? 3 : j;
                while (j > 0) {
                    contextList.add("W-"+j+"="+contextList.get(contextList.size()-j));
                    //contextList.add("Stem-"+j+"="+stemmer.stem(contextList.get(contextList.size()-j)));
                    j--;
                }
                sb.setLength(0);
                afterEnd = 1;
                i = end;
                continue;
            }
            if (Character.isWhitespace(chars[i])) {
                if (sb.length() > 0) {
                    contextList.add(sb.toString());
                    if ((afterEnd > 0) && (afterEnd < 4)) {
                        contextList.add("W+"+afterEnd+"="+sb.toString());
                        //contextList.add("Stem+"+afterEnd+"="+stemmer.stem(sb.toString()));
                        afterEnd++;
                    }
                    sb.setLength(0);
                }
            }
            else {
                sb.append(chars[i]);
            }
            i++;
        }
        int j = 0;
        List<String> smallList = new ArrayList<String>();
        while (j < contextList.size()) {
            if (smallList.size() > 3) {
               break;
            }
            if (!contextList.get(j).startsWith("W-")) {
                smallList.add("P-" + smallList.size() + "=" + contextList.get(j).trim());
            }
            j++;
        }
        contextList.addAll(smallList);
        String[] context = contextList.toArray(new String[contextList.size()]);
        Event d = new Event(label, context);
        return d;
    }
//
//    public class ContextDatum {
//        public ContextDatum() {
//
//        }
//
//        public ContextDatum(String label, String[] context) {
//            _context = context;
//            _label = label;
//        }
//
//        private String[] _context = new String[0];
//        private String _label = "";
//
//        public String[] context() {
//            return _context;
//        }
//
//        public void setContext(String[] context) {
//            this._context = context;
//        }
//
//        public String label() {
//            return _label;
//        }
//
//        public void setLabel(String label) {
//            this._label = label;
//        }
//    }

    public static void main(String[] args) {
        JargonSentiment jargonSentiment = new JargonSentiment();
        //List<Event> trainEvents = jargonSentiment.readData("/Users/yhe/Dropbox/work/FUSE/Training_Data/dataset/training.11107");
        //jargonSentiment.trainModel(trainEvents, "/Users/yhe/Dropbox/work/FUSE/Training_Data/dataset/model.bin.gz");
        //jargonSentiment.loadModel("/Users/yhe/Dropbox/work/FUSE/Training_Data/dataset/model.bin.gz");
        //List<Event> testEvents = jargonSentiment.readData("/Users/yhe/Dropbox/work/FUSE/Training_Data/dataset/testset");
        System.err.println("RelationLearning.FuseJet.Sentiment.JargonSentiment ");
        List<Event> trainEvents = jargonSentiment.readData(args[1]);
        jargonSentiment.trainModel(trainEvents, args[0]);
        jargonSentiment.loadModel(args[0]);
        List<Event> testEvents = jargonSentiment.readData(args[2]);
        jargonSentiment.test(testEvents);
    }
}
