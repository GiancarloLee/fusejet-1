//This class is used to check whether the sentence contains some Figure or Formula.

package FuseJet.Sentiment.JargonSentiment;

import java.io.*;
import java.util.*;

public class FigureFormularFeature 
{
	static String encoding = "UTF-8";
	static String home = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/Data/Data_Apr_20_2012/FeatureValues/";
	
	static String figureformulawordlistfile = "/Document/Study/NYUNLP/FUSE/FuseJet.Sentiment.JargonSentiment/WordList/FigureFormulaWordList";
	
	static String inputfile = home + "Annotations_mar_4_2012_citation_sentences_mar_23_2012.txt";
	static String outputfile = home + "figurefeaturevalue.txt";

	static HashSet<String> figureformulaset = new HashSet<String>();
	
	public static void main(String[] args) throws Exception
	{
		readFigureFormulaWordList();
		generateFeatureValues();
	}
	
	public static void readFigureFormulaWordList() throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(figureformulawordlistfile), encoding));

		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			figureformulaset.add(line);
		}
		
		reader.close();
	}
	
	public static void generateFeatureValues() throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), encoding));
		PrintWriter writer = new PrintWriter(outputfile, encoding);
		
		String line = "";
		
		while((line = reader.readLine())!=null)
		{
			String fields [] = line.split("\\t");
			
			String sentencefield = fields[8];
			String sentence = sentencefield.replace("sentence=", "");
			
			boolean contains = false;
			
			for(String word : figureformulaset)
			{
				if(sentence.contains(word))
				{
					contains = true;
//					System.out.println(word + "\t" + line);
					break;
				}
			}
			
			if(contains)
			{
				writer.println("contains_Figure_Formula=true");
			}
			else
				writer.println("contains_Figure_Formula=false");

		}
		
		reader.close();
		
		writer.close();
	}

}
