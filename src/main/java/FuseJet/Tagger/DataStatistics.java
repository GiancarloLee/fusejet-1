package FuseJet.Tagger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataStatistics {
	static int total = 0;
	static double unknown_num = 0;

	public static void main(String[] args) throws IOException {
		String trainFeatureFile = "/Users/shashaliao/Research/workspace/FUSEProject/data/pos_feature.log";
		String corpusDir = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/";
		String listFile = corpusDir+"list.txt";
		List<String> words = readFeatureFileToCollectWords(trainFeatureFile);
	    BufferedReader reader = new BufferedReader(new FileReader(listFile));
	    String line;
	    while(( line = reader.readLine()) != null){
	    	if(line.length() == 0)
	    		continue;
	    	if(line.endsWith(".xml"))
	    		line = line.substring(0, line.length()-4);
	    	System.err.println("Processing document:"+line);
			String sysFile = corpusDir+line+".sgm.pos";
			String keyFile = corpusDir+line+".pos";   
			calUnknownWord(words, sysFile, keyFile);
	    }
		System.err.println(total+"\t"+unknown_num+"\t"+unknown_num/total);	

	}
	public static void calUnknownWord(List<String> words, String testFile, String keyFile) throws IOException{		
		BufferedReader reader1 = new BufferedReader(new FileReader(testFile));
		BufferedReader reader2 = new BufferedReader(new FileReader(keyFile));
        String line;
		while((line = reader1.readLine()) != null){
			String keyline = reader2.readLine();
			if(line.length() == 0)
				continue;
			String[] tokens = line.split(" |\t");
			String[] keys = keyline.split(" |\t");
			String word = tokens[0].toLowerCase();
			if(tokens[0].equals(keys[0])==false){				
				System.err.println(line+"\n"+keyline+"\n=====");
			}
			
			boolean match = POSEvaluator.samePOS(tokens[1], keys[1]);
			if(match)
				continue;
			double prob = Double.parseDouble(tokens[2]);
			total++;
			if(words.contains(word) ){
				//System.err.println("Known:\t"+word+"\t"+tokens[1]+"\t"+keys[1]+"\t"+prob);
				
			}
			else{
				//System.err.println("Unknown:"+prob);
				unknown_num++;
			}
		}
		reader1.close();
		reader2.close();		
	}
	
	public static List<String> readFeatureFileToCollectWords(String trainFeatureFile) throws IOException{
		List<String> words = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(trainFeatureFile));
		String line;
		while((line = reader.readLine()) != null){
			Datum d = new Datum(line);
			words.add(d.getFeatureVal("word"));
		}
		reader.close();
		return words;
	}

}
