package FuseJet.Tagger;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/1/12
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class Scorer {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("FuseJet.Tagger.Scorer sysFile keyFile");
            System.exit(-1);
        }
        String sysFile = args[0];
        String keyFile = args[1];

        try {
            Score s = POSEvaluator.eval(sysFile, keyFile);
            System.err.println(String.format("Match: %d Total: %d Acc: %.4f",
                    s.getMatch(), s.getTotal(), s.score()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


}
