package FuseJet.Tagger;

import java.io.*;

public class PennTreeBankProcessor {

	BufferedWriter writer ;
	public static void main(String[] args) throws IOException{
		String ofile = "/Users/shashaliao/Research/FUSE/data/PENN2/wsj_training.pos";
		String inputDir = "/Users/shashaliao/Research/FUSE/data/PENN2/tagged/wsj/";
		PennTreeBankProcessor tb = new PennTreeBankProcessor(ofile);
		File dir = new File(inputDir);
		File[] subdirs = dir.listFiles();
		for(File subdir:subdirs){
			if(subdir.isDirectory() == false)
				continue;
			File[] files = subdir.listFiles();
			for(File file:files){
				if(file.getName().endsWith(".pos")){
					System.err.println(file.getName());
					tb.processOneDocument(file);

				}
			}
		}
		tb.writer.close();
	}

	PennTreeBankProcessor(String ofile)throws IOException{
		writer = new BufferedWriter(new FileWriter(ofile));
	}

	public void processOneDocument(File f)throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String line;
		while((line = reader.readLine()) != null){
			if(line.length()>0 )
				break;
		}
		while((line = reader.readLine())!= null){
			//System.err.println(line);
			if(line.length() == 0){
				writer.write("\n");
			}
			else if(line.startsWith("====")){
				writer.write("\n");
			}
			else{				
				String[] tokens = line.split(" |\t");
				for(String token:tokens){
					if(token.length() == 0 || token.equals("[") || token.equals("]"))
						continue;
					int pos = token.indexOf("/");	
					//System.err.println(token);
					writer.write(token.substring(0,pos)+" "+token.substring(pos+1)+"\n");
				}
			}
		}
		writer.write("\n");
		reader.close();
	}

}
