package FuseJet.Tagger;

public class POS {
	String word;
	String pos;
	double prob;
	POS(String w, String p, double ps){
		word = w.trim();
		pos = p;
		prob = ps;
	}

	POS(String w, String p){
		word = w.trim();
		pos = p;
		prob = 1.0;
	}


	@Override
	public String toString() {
		return word+"\t"+pos+"\t"+prob;
	}

	static POS readPOS(String line, boolean withProb){		
		String[] tokens = line.split("\t");
		if(tokens.length < 2 || tokens.length > 3){
			System.err.println("Error in reading line "+line+"\t"+line.length()+" in class "+POS.class.getName());
			System.exit(1);
		}
		if(withProb == false || tokens.length == 2)
			return new POS(tokens[0], tokens[1]);
		return new POS(tokens[0], tokens[1],Double.parseDouble(tokens[2]));
	}

}
