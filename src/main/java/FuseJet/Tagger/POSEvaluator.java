package FuseJet.Tagger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class POSEvaluator {
	//map the newswire POS table to Genia POS table
	static Map<String, String> posMap = new HashMap<String, String>();
	static{
//		posMap.put(".", "PERIOD");
//		posMap.put(",", "COMMA");
//		posMap.put("-LRB-", "LRB");
//		posMap.put("-RRB-", "RRB");
//		posMap.put("NNP", "NN");
//		posMap.put("NNPS", "NNS");
	}

	public static void main(String[] args) throws IOException {
		//    test();
		String corpusDir = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/";
		String listFile = corpusDir+"list.txt";

		BufferedReader reader = new BufferedReader(new FileReader(listFile));
		String line;
		Score score = new Score();
		while(( line = reader.readLine()) != null){
			if(line.length() == 0)
				continue;
			if(line.endsWith(".xml"))
				line = line.substring(0, line.length()-4);
			System.err.println(line);
			Score s2 = eval(corpusDir+line+".sgm.pos", corpusDir+line+".pos");
			score.addScore(s2);
		}
		reader.close();
		System.err.println(score.score());
	}


	public static void test()throws IOException{
		String sysFile1 = "/Users/shashaliao/Research/FUSE/data/PennTreeBank_test.sys.pos";
		String keyFile1 = "/Users/shashaliao/Research/FUSE/data/PennTreeBank_test.pos.txt";
		Score s1 = eval(sysFile1, keyFile1);
		System.err.println(s1.score());

		String sysFile2 = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/8893011.sys.pos";
		String keyFile2 = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/8893011.pos";
		Score s2 = eval(sysFile2, keyFile2);
		System.err.println(s2.score());
	}


	public static Score eval(String sysFile, String keyFile) throws IOException{
		int match = 0;
		List<POS> sysTokens = getPOS(sysFile, true);
		List<POS> keyTokens = getPOS(keyFile, false);
		if(sysTokens.size() != keyTokens.size()){
			System.err.println("Error: there is tokenization problems...\n"+sysTokens.size()+"\t"+keyTokens.size());

			for(int i=0;i< Math.min(sysTokens.size(), keyTokens.size());i++){
				System.err.println(sysTokens.get(i).word+"\t"+keyTokens.get(i).word);
			}
			System.exit(1);
		}
		for(int i=0;i<sysTokens.size();i++){
			POS sysToken = sysTokens.get(i);
			POS keyToken = keyTokens.get(i);
			if(!sysToken.word.trim().equals(keyToken.word.trim())){
				System.err.println("Error: there is tokenization problems2..."
			+sysToken.word+"%%\t%%"+keyToken.word);
				System.exit(1);
			}
			if(samePOS(sysToken.pos, keyToken.pos))
				match++;
			/*else{
				System.err.println(sysToken.word+"\t"+sysToken.pos+"\t"+keyToken.pos+"\t"+sysToken.prob);
			}*/
		}
		return new Score(match, sysTokens.size());		
	}

	public static boolean samePOS(String word1, String word2){
		if(posMap.containsKey(word1))
			word1 = posMap.get(word1);
		if(posMap.containsKey(word2))
			word2 = posMap.get(word2);
		return word1.equals(word2);
	}

	public static List<POS> getPOS(String file,boolean withProb) throws IOException{
		System.err.println(file);
		List<POS> tokens = new ArrayList<POS>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line  = reader.readLine()) != null){
			if(line.length() == 0)
				continue;
			//System.err.println(line);
			POS p = POS.readPOS(line, withProb);
			tokens.add(p);
		}		
		reader.close();
		return tokens;
	}

}


