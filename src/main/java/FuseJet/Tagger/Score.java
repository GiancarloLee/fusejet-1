package FuseJet.Tagger;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/1/12
 * Time: 11:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class Score{
    public int getMatch() {
        return match;
    }

    public void setMatch(int match) {
        this.match = match;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    private int match;
    private int total;

    public Score(){
        match = 0;
        total = 0;
    }

    public Score(int m, int t){
        match = m;
        total = t;
    }

    public double score(){
        return (double)match/total;
    }

    public void addScore(Score s){
        match += s.match;
        total += s.total;
    }
}