package FuseJet.Tagger;

import FuseJet.Utils.FuseUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/8/12
 * Time: 9:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class WordCluster {
    private static Map<String, String> wordClusters = new HashMap<String, String>();

    private static boolean activated = false;

    public static boolean isActivated() {
        return activated;
    }

    public static void setActivated(boolean activated) {
        WordCluster.activated = activated;
    }

    public static void loadFromFile(String fileName) throws IOException {
        System.err.print("Loading word cluster file " + fileName + " for tagger ...");
        wordClusters.clear();
        setActivated(false);
        String[] lines = FuseUtils.readLines(fileName);
        int maxWordLen = 0;
        for (String line : lines) {
            String[] parts = line.split("\t");
            wordClusters.put(parts[1].trim(), parts[0].trim());
            if (parts[1].trim().length() > maxWordLen) {
                maxWordLen = parts[1].trim().length();
            }
        }
        for (String key : wordClusters.keySet()) {
            if (wordClusters.get(key).length() < maxWordLen) {
                int diff = maxWordLen - wordClusters.get(key).length();
                StringBuilder b = new StringBuilder();
                for (int i = 0; i < diff; i++) {
                    b.append("0");
                }
                wordClusters.put(key, wordClusters.get(key) + b.toString());
            }
        }
        setActivated(true);
        System.err.println(" done.");
    }

    public static String getClusterPrefix(String word, int len) {
        String candidate = wordClusters.get(word);
        if (candidate == null) return "0";
        if (candidate.length() < len) return candidate;
        return candidate.substring(0, len);
    }

    public static String getClusterId(String word) {
        String candidate = wordClusters.get(word);
        if (candidate == null) return "-1";
        return candidate;
    }
}
