package FuseJet.Tagger;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class PennTreeBank {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException{	
		
		String suffix = "/Users/shashaliao/Research/FUSE/data/PENN2/wsj_training.pos";		
		writeSgm(suffix+".pos.txt", suffix+".sgm");
		
		/*String pfile = "/Users/shashaliao/Research/FUSE/data/PENN2/wsj_training.pos";
		String dfile = "/Users/shashaliao/Research/FUSE/data/PennTreeBank.dic";
		getDictionary(pfile, dfile );*/	

	}
	

	
	public static void writeSgm(String posFile,String sgmfile)throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(posFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(sgmfile));
		String line;
		while (( line = reader.readLine()) != null){
			if(line.length() == 0)
				writer.write("\n");
			else{
				String[] tokens = line.split(" ");
				writer.write(tokens[0]+ " ");
			}
		}
		writer.write("\n");
		reader.close();
		writer.close();
	}
	
	public static void getDictionary(String file, String dicFile) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		Set<String> dics = new HashSet<String>();
		String line;
		while (( line = reader.readLine()) != null){
			if(line.length() != 0){
				String[] tokens = line.split(" ");
				String word = tokens[0].toUpperCase();
				if(dics.contains(word) == false){
					System.err.println(word);
				   dics.add(word);
				}
			}

		}
		reader.close();
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(dicFile));
		for(String word:dics){
			if(word.length() == 1)
				continue;
			boolean validword = true;
			for(int i=0;i<word.length();i++){
				char a = word.charAt(i);
				if(Character.isLetter(a) == false){
					validword = false;
					break;
				}
			}
			if(validword)
			writer.write(word+"\n");
		}
		writer.write("A\n");
		writer.close();
	}

}
