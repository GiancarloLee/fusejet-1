package FuseJet.Tagger;

import FuseJet.Utils.FuseUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 7/11/12
 * Time: 3:49 PM
 */
public class TagBasedClustering {

    private Map<String, Map<String, Integer>> wordPosMap = new HashMap<String, Map<String, Integer>>();
    private Map<String, Integer> posMap = new HashMap<String, Integer>();
    private List<WordVector> wordVectors = new ArrayList<WordVector>();
    private List<Cluster> clusters = null;
    private Random random = new Random(728);

    public void loadWordPosMap(String fileName) throws IOException{
        wordVectors.clear();
        String[] sentences = FuseUtils.readSentenceChunksFromConll(fileName);
        for (String sentence : sentences) {
            sentence = sentence.trim();
            if (sentence.length() == 0) continue;
            String[] wordsWithPos = sentence.split("\n");
            for (String wordWithPos : wordsWithPos) {
                wordWithPos = wordWithPos.trim();
                if (wordWithPos.length() == 0) continue;
                String[] parts = wordWithPos.split("\t");
                if (!wordPosMap.containsKey(parts[0])) {
                    wordPosMap.put(parts[0], new HashMap<String, Integer>());
                }
                Map<String, Integer> posCountMap = wordPosMap.get(parts[0]);
                int count = 1;
                if (posCountMap.containsKey(parts[1])) {
                    count = posCountMap.get(parts[1]);
                }
                posCountMap.put(parts[1], count + 1);
                posMap.put(parts[1], 0);
            }
        }
        int i = 0;
        for (String pos : posMap.keySet()) {
            posMap.put(pos, i);
            i++;
        }
        for (String word : wordPosMap.keySet()) {
            double[] vector = new double[posMap.size()];
            Map<String, Integer> posCountMap = wordPosMap.get(word);
            for (String pos : posCountMap.keySet()) {
                vector[posMap.get(pos)] = posCountMap.get(pos);
            }
            conditionalProb(vector);
            wordVectors.add(new WordVector(word, vector));
        }
    }

    public Cluster[] kMeansCluster(int k) {
        initializeKmeans(k);
        System.err.print(" Num of clusters: " + clusters.size());
        // KMeans clustering, quit when centroids do not change
        int iterId = 1;
        while (true) {
            double totDistance = 0;
            System.err.print(" Running iteration " + iterId);
            for (WordVector wordVector : wordVectors) {
                double minDistance = Double.MAX_VALUE;
                int minIdx = -1;
                for (int i = 0; i < clusters.size(); i++) {
                    double d = clusters.get(i).fromVector(wordVector);
                    if (d < minDistance) {
                        minDistance = d;
                        minIdx = i;
                    }
                }
                clusters.get(minIdx).addMember(wordVector);
                totDistance += minDistance;
            }
            // recalculate centroids
            System.err.print(" Num of clusters: " + clusters.size());
            System.err.println(" Sum(distance^2) = "+totDistance);
            boolean unchanged = true;
            for (Cluster c : clusters) {
                boolean unchangedCluster = c.updateCentroid();
                unchanged = unchanged && unchangedCluster;
            }
            if (unchanged) {
                System.err.println("Cluster centroids are stable. Quit.");
                return clusters.toArray(new Cluster[clusters.size()]);
            }
            else {
                for (Cluster c : clusters) {
                    c.clearMembers();
                }
            }
            iterId++;
        }

    }

    private void initializeKmeans(int k) {
        System.err.print("Start initialization...");
        clusters = new ArrayList<Cluster>();
        // K-Means++ intialization
        Cluster cluster = new Cluster();
        cluster.setCentroid(wordVectors.get(random.nextInt(wordVectors.size())).vector());
        clusters.add(cluster);
        int nextCentroidId = 1;
        while (nextCentroidId < k) {
            // Calculate min distances
            double[] minDistances = new double[wordVectors.size()];
            double[] accDistances = new double[wordVectors.size()];
            int i = 0;
            for (WordVector wordVector : wordVectors) {
                double minDistance = Double.MAX_VALUE;
                for (Cluster c : clusters) {
                    double d = c.fromVector(wordVector);
                    if (d < minDistance) {
                        minDistance = d;
                    }
                }
                minDistances[i] = minDistance;
                i++;
            }
            // Compute distribution to sample
            double sum = 0;
            for (i = 0; i < minDistances.length; i++) {
                sum += minDistances[i];
            }
            for (i = 0; i < minDistances.length; i++) {
                minDistances[i] /= sum;
            }
            sum = 0;
            for (i = 0; i < minDistances.length; i++) {
                sum += minDistances[i];
                accDistances[i] = sum;
            }
            // Sampling
            double pivot = random.nextFloat() * accDistances[accDistances.length-1];
            for (i = 0; i < accDistances.length; i++) {
                if (accDistances[i] > pivot) break;
            }
            cluster = new Cluster();
            cluster.setCentroid(wordVectors.get(i).vector());
            clusters.add(cluster);
            nextCentroidId++;
        }
        System.err.println("Done.");

    }

    private void conditionalProb(double[] v) {
        double sum = 0;
        for (int i = 0; i < v.length; i++) {
            sum += v[i];
        }
        for (int i = 0; i < v.length; i++) {
            v[i] /= sum;
        }
    }

    private double klDivergence(double[] p1, double[] p2) {

        double klDiv = 0.0;
        for (int i = 0; i < p1.length; ++i) {
            if (p1[i] == 0) { continue; }
            if (p2[i] == 0.0) { continue; } // Limin

            klDiv += p1[i] * Math.log( p1[i] / p2[i] );
        }

        return klDiv / Math.log(2.0); // moved this division out of the loop -DM
    }

    public class WordVector {
        private String w;
        private double[] v;

        public String word() {
            return w;
        }

        public double[] vector()  {
            return v;
        }

        public WordVector(String word, double[] vector) {
            this.w   = word;
            this.v = vector;
        }
    }

//    private double xEuclideanDistance(double[] p1, double[] p2) {
//        if (p1.length != p2.length) {
//            System.err.println("ERROR: p1 and p2 should have equal length in euclideanDistance()");
//            return -1;
//        }
//        double sum = 0;
//        for (int i = 0; i < p1.length; i++) {
//            double pow = (p1[i] - p2[i]) * (p1[i] - p2[i]);
//            sum += pow;
//        }
//        return Math.sqrt(sum);
//    }

    private double squareEuclideanDistance(double[] p1, double[] p2) {
        if (p1.length != p2.length) {
            System.err.println("ERROR: p1 and p2 should have equal length in euclideanDistance()");
            return -1;
        }
        double sum = 0;
        for (int i = 0; i < p1.length; i++) {
            double pow = (p1[i] - p2[i]) * (p1[i] - p2[i]);
            sum += pow;
        }
        return sum;
    }

    public static void main(String[] args) {
        TagBasedClustering clustering = new TagBasedClustering();
        //System.err.println(clustering.euclideanDistance(new double[]{1.0, 2.0, 1.0}, new double[]{0, 4, 0}));

        try {
            System.err.println("Reading word-POS mappings...");
            clustering.loadWordPosMap(args[0]);
            System.err.println("Start Kmeans...");

            Cluster[] clusters = clustering.kMeansCluster(128);
            PrintWriter w = new PrintWriter(new File(args[1]));
            int i = 0;
            for (Cluster c : clusters) {
                for (WordVector wordVector : c.members) {
                    w.println(i + "\t" + wordVector.word());
                }
                i++;
            }
            w.close();
        }
        catch (IOException e) {
            System.err.println("Unable to read/write file");
            e.printStackTrace();
        }
    }

    public class Cluster {
        private double[] centroid = null;

        private List<WordVector> members = new ArrayList<WordVector>();

        private double c2all() {
            double result = 0;
            for (WordVector m : members) {
                result += squareEuclideanDistance(centroid, m.v);
            }
            return result;
        }

        public void setCentroid(double[] c) {
            this.centroid = c;
        }
//
//        public double[] centroid() {
//            return centroid;
//        }

        public double fromVector(WordVector v) {
            return squareEuclideanDistance(centroid, v.vector());
        }

        public void addMember(WordVector v) {
            members.add(v);
        }

        public boolean updateCentroid() {
            //System.err.print(c2all() + " => ");
            double[] oldC = centroid;
            centroid = new double[centroid.length];
            for (WordVector m : members) {
                double[] v = m.vector();
                for (int i = 0; i < v.length; i++) {
                    centroid[i] += v[i];
                }
            }
            for (int i = 0; i < centroid.length; i++) {
                centroid[i] = centroid[i] / members.size();
            }
            //System.err.println(c2all());
            for (int i = 0; i < centroid.length; i++) {
                if (oldC[i] != centroid[i]) return false;
            }
            return true;
        }

        public void clearMembers() {
            members.clear();
        }
    }

}
