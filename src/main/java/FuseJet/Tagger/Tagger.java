package FuseJet.Tagger;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/1/12
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class Tagger {
    public static void main(String[] args) {
        if (args.length < 4) {
            System.err.println("FuseJet.Tagger.Trainer featureFile modelFile inputFile outputFile [clusterFile]");
            System.exit(-1);
        }
        String featureFile = args[0];
        String modelFile = args[1];
        String inputFile = args[2];
        String outputFile = args[3];

        if (args.length == 5) {
            try {
                WordCluster.loadFromFile(args[4]);
            }
            catch (IOException e) {
                System.err.println("Error reading cluster file");
                e.printStackTrace();
                System.exit(-1);
            }
        }

        POSTagger tagger = new POSTagger(featureFile, modelFile);
        try {
            tagger.testTokenFile(inputFile, outputFile);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
