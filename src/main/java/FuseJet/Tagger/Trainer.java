package FuseJet.Tagger;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/1/12
 * Time: 5:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class Trainer {

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("FuseJet.Tagger.Trainer featureFile modelFile dataFile [clusterFile]");
            System.exit(-1);
        }
        String featureFile = args[0];
        String modelFile = args[1];
        String dataFile = args[2];
        if (args.length == 4) {
            try {
                WordCluster.loadFromFile(args[3]);
            }
            catch (IOException e) {
                System.err.println("Error reading cluster file");
                e.printStackTrace();
                System.exit(-1);
            }
        }

        POSTagger tagger = new POSTagger(featureFile, modelFile);
        tagger.train(dataFile);
    }
}
