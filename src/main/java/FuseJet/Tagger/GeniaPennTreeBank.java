package FuseJet.Tagger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GeniaPennTreeBank {
	List<String> list = new ArrayList<String>();
	String corpusDir;
	
	static public void main(String[] args) throws IOException, SAXException, ParserConfigurationException{
		/*String ifile = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/7510691";
		writePOSInfo(ifile+".xml", ifile+".pos");
		writeOriginalText(ifile+".xml", ifile+".sgm");*/
		
		String cDir = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/";
		String listFile = cDir+"list.txt";
		GeniaPennTreeBank genia = new GeniaPennTreeBank(listFile, cDir);
		genia.processData();
	}
	
	GeniaPennTreeBank(String listFile, String cDir) throws IOException, SAXException, ParserConfigurationException{
         BufferedReader reader = new BufferedReader(new FileReader(listFile));
         String line;
         while(( line = reader.readLine()) != null){
        	 if(line.endsWith(".xml"))
        		 line = line.substring(0, line.length()-4);
        	 list.add(line);
         }
         reader.close();
         corpusDir = cDir;
	}
	
	public void processData() throws IOException, SAXException, ParserConfigurationException{        
        for(String l:list){
       	 writePOSInfo(corpusDir+l+".xml", corpusDir+l+".pos");
       	 writeOriginalText(corpusDir+l+".xml", corpusDir+l+".sgm");
        }
	}
	
	GeniaPennTreeBank() {
	}
	
	static public void writePOSInfo(String ifile, String ofile)throws IOException, SAXException, ParserConfigurationException{
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(ifile));
		
		NodeList sNodes = doc.getElementsByTagName("sentence");
		if(sNodes == null){
			System.err.println("Error, no Text in file "+ifile);
			System.exit(1);
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(ofile));
		for(int i=0;i<sNodes.getLength();i++){
			Element sentence = (Element)sNodes.item(i);
			NodeList tNodes = sentence.getElementsByTagName("tok");
			for(int m=0;m<tNodes.getLength();m++){
				Element t = (Element)tNodes.item(m);
				Token token = new Token (t.getTextContent(),t.getAttribute("cat"));
				writer.write(token.word+"\t"+token.pos+"\n");
			}			
			writer.write("\n");
		}
		writer.close();
	}
	
	static public void writeOriginalText(String ifile,String ofile)throws IOException, SAXException, ParserConfigurationException{
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(ifile));
		NodeList sNodes = doc.getElementsByTagName("sentence");
		if(sNodes == null){
			System.err.println("Error, no Text in file "+ifile);
			System.exit(1);
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(ofile));
		//writer.write("<TEXT>\n");
		for(int i=0;i<sNodes.getLength();i++){
			Element sentence = (Element)sNodes.item(i);
			NodeList tNodes = sentence.getElementsByTagName("tok");
			//writer.write("<P>");
			for(int m=0;m<tNodes.getLength();m++){
				Element t = (Element)tNodes.item(m);
				writer.write(t.getTextContent()+" ");
			}			
			writer.write("\n");
		}
		//writer.write("</TEXT>\n");
		writer.close();
	}

}


class Token{
	String word;
	String pos;
	
	Token(String w, String p){
		word = w;
		pos = p;
	}
}