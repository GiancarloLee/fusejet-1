package FuseJet.Tagger;

import AceJet.Ace;
import FuseJet.Annotators.FuseJetAnnotator;
import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Lex.Tokenizer;
import Jet.Tipster.Annotation;
import Jet.Tipster.Document;
import Jet.Tipster.ExternalDocument;
import Jet.Tipster.Span;
import Jet.Zoner.SentenceSplitter;
import Jet.Zoner.SpecialZoner;
import opennlp.maxent.BasicEventStream;
import opennlp.maxent.GIS;
import opennlp.maxent.GISModel;
import opennlp.maxent.PlainTextByLineDataStream;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.PlainTextGISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.EventStream;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class POSTagger implements FuseJetAnnotator {
	String featureFileName;
	String modelFileName;
    String unkFeatureFileName;
    String unkModelFileName;
	PrintStream featureWriter = null;
	GISModel model = null;
	int cutoff = 4;
	int iterations = 100;

    public POSTagger() {
    }

	/**
	 *  creates a new maximum entropy model, specifying files for both
	 *  the features and the resulting model.
	 *
	 *  @param featureFileName  the name of the file in which features will be
	 *                          stored during training
	 *  @param modelFileName    the name of the file in which the max ent
	 *                          model will be stored
	 */
	public POSTagger (String featureFileName, String modelFileName) {
		this.featureFileName = featureFileName;
		this.modelFileName = modelFileName;
	}

	public void initializeForTraining (String featureFileName) {
		this.featureFileName = featureFileName;
        this.unkFeatureFileName = featureFileName + "_unk";
		initializeForTraining ();
	}

	public void initializeForTraining () {
		if (featureFileName == null) {
			System.err.println ("MaxEntModel.initializeForTraining: no featureFileName specified");
		} else {
			try {
				featureWriter = new PrintStream (new FileOutputStream (featureFileName));
			} catch (IOException e) {
				System.err.print("Unable to create feature file: ");
				System.err.println(e);
			}
		}
	}

	public void addEvent (Datum d) {
		if (featureWriter == null)
			initializeForTraining ();
		featureWriter.println (d.toString());
	}

	public void setCutoff (int cutoff) {
		this.cutoff = cutoff;
	}

	public void setIterations (int iterations) {
		this.iterations = iterations;
	}
	public void buildModel () {
		boolean USE_SMOOTHING = false;
		double SMOOTHING_OBSERVATION = 0.1;
		boolean PRINT_MESSAGES = true;
		try {
			featureWriter.close();
			FileReader datafr = new FileReader(new File(featureFileName));
			EventStream es =
					new BasicEventStream(new PlainTextByLineDataStream(datafr));
			GIS.SMOOTHING_OBSERVATION = SMOOTHING_OBSERVATION;
			model = GIS.trainModel(es, iterations, cutoff, USE_SMOOTHING, PRINT_MESSAGES);
		} catch (Exception e) {
			System.err.print("Unable to create model due to exception: ");
			System.err.println(e);
		}
	}

	public void saveModel () {
		if (modelFileName == null) {
			System.err.println ("MaxEntModel.saveModel:  no modelFileName specified");
		} else {
			saveModel (modelFileName);
		}
	}

	public void saveModel (String modelFileName) {
		try {
			File outputFile = new File(modelFileName);
			GISModelWriter modelWriter = new SuffixSensitiveGISModelWriter(model, outputFile);
			modelWriter.persist();
		} catch (IOException e) {
			System.err.print("Unable to save model: ");
			System.err.println(e);
		}
	}

	public void saveModel (BufferedWriter writer) {
		try {
			GISModelWriter modelWriter = new PlainTextGISModelWriter(model, writer);
			modelWriter.persist();
		} catch (IOException e) {
			System.err.print("Unable to save model: ");
			System.err.println(e);
		}
	}

	public void loadModel () {
		if (modelFileName == null) {
			System.err.println ("MaxEntModel.loadModel:  no modelFileName specified");
		} else {
			if(model == null)
				loadModel (modelFileName);
		}
	}

	public void loadModel (String modelFileName) {

		try {
			File f = new File(modelFileName);
			model = (GISModel) new SuffixSensitiveGISModelReader(f).getModel();
			System.err.println ("GIS model " + f.getName() + " loaded.");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		String[] tags = new String[getNumOutcomes()];
		for(int i=0;i<tags.length;i++){
			tags[i] = getOutcome(i);
			//System.err.println(tags[i]);
		}
		Matrix.initialMatrix(tags);
	}

	public boolean isLoaded () {
		return model != null;
	}

	/**
	 *  (for a trained model) returns the probability that the Datum
	 *  <CODE>d</CODE> is classified as <CODE>value</CODE>.
	 */

	public double prob (Datum d, String value) {
		return model.eval(d.toArray())[model.getIndex(value)];
	}

	/**
	 *  (for a trained model) returns the most likely outcome for Datum
	 *  <CODE>d</CODE>.
	 */

	public String bestOutcome (Datum d) {
		return model.getBestOutcome(model.eval(d.toArray())).intern();
	}

	public int getNumOutcomes () {
		return model.getNumOutcomes();
	}

	public String getOutcome (int i) {
		return model.getOutcome(i);
	}

	public double[] getOutcomeProbabilities (Datum d) {
		return model.eval(d.toArray());
	}

	public static void main (String[] args)throws IOException{
		//testProgram();
		POSTagger tagger = new POSTagger("data/pos_feature.log","data/pos_model.log");
		String corpusDir = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/";
		String listFile = corpusDir+"list.txt";
		//tagger.testTokenFile(corpusDir+"8787953.pos", corpusDir+"8787953.sgm.pos");

		BufferedReader reader = new BufferedReader(new FileReader(listFile));
		String line;
		while(( line = reader.readLine()) != null){
			if(line.length() == 0)
				continue;
			if(line.endsWith(".xml"))
				line = line.substring(0, line.length()-4);
			System.err.println(line);
			tagger.testTokenFile(corpusDir+line+".pos", corpusDir+line+".sgm.pos");
		}
		reader.close();
		/*if(args.length == 2){
			tagger.testSgmFile(args[0], args[1]);
		}
		else if(args.length == 3){
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
			String line;
			while((line = reader.readLine()) != null){
				tagger.testSgmFile(args[1]+line+".sgm", args[2]+line+".sgm.pos");
			}
			reader.close();
		}*/

	}

	public static void testProgram()throws IOException{
		/*String trainData = "/Users/shashaliao/Research/FUSE/data/PENN2/wsj_training.pos";
		POSTagger tagger = new POSTagger("data/pos_feature.log","data/pos_model.log");
		tagger.train(trainData);*/

		POSTagger newtagger = new POSTagger("data/pos_feature.log","data/pos_model.log");

		String testData = "/Users/shashaliao/Research/FUSE/data/PennTreeBank_test.sgm";
		String outputFile = "/Users/shashaliao/Research/FUSE/data/PennTreeBank_test.sys.pos";
		newtagger.testTokenFile(testData,outputFile);

		testData = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/8893011.sgm";
		outputFile = "/Users/shashaliao/Research/FUSE/data/GENIA_treebank_v1/8893011.sys.pos";
		newtagger.testTokenFile(testData,outputFile);
	}

	void trainOneSentence(List<String> words,List<String> poses){
		String pretag = "$S";
        String prePretag = "$S";
		String preword = "$S";
		for(int i=0;i<words.size();i++){
			String posword = "$E";
			if(i < words.size()-1)
				posword = words.get(i+1);
            //System.err.println("\"" + words.get(i) + "\"" + ' ' + poses.get(i));
			Datum d = buildDatum(words.get(i),preword,posword);
			addPreTagInfo(prePretag, pretag, d);
			d.setOutCome(poses.get(i));
			addEvent(d);
            prePretag = pretag;
			pretag = d.outcome;
			preword = words.get(i);
		}
	}

	//train the module.
	public void train(String data){
		try {
			initializeForTraining();
			BufferedReader reader = new BufferedReader(new FileReader(data));
			String line;

			List<String> words = new ArrayList<String>();
			List<String> poses = new ArrayList<String>();
			while((line = reader.readLine()) != null ){
                line = line.trim();
				if(line.length() == 0){
					if(words.size() > 0){
						trainOneSentence(words,poses);
						words.clear();
						poses.clear();
					}
				}
				else{
					String[] tokens = line.split(" ");
                    if (tokens.length < 2) {
                        tokens = line.split("\t");
                    }
					words.add(tokens[0]);
					poses.add(tokens[1]);
				}
			}
			if(words.size() > 0){
				trainOneSentence(words,poses);
			}
			reader.close();
			buildModel();
			saveModel();
		} catch (Exception e) {
			System.out.print("Unable to create model due to exception: ");
			System.out.println(e);
            e.printStackTrace();
		}
	}

	Datum buildDatum(String word,String preword, String nextword){
		//System.err.println(line);
		Datum d = new Datum();
		//add all uni-gram features
		String baseword = word.toLowerCase();
		d.addFV("word",baseword);  
		//System.err.println("&&"+word+"&&");
		boolean cap = Character.isUpperCase(word.charAt(0));
		if(preword.equals("$S")){
			d.addFV("cap", "NA");
		}
		else{
			d.addFV("cap",cap+"");
		}

		boolean allcap = word.toUpperCase().equals(word);
		d.addFV("allcap",allcap+"");
		d.addFV("allcapword", allcap+":"+baseword);

		d.addFV("isNum", isNum(word)+"");
		d.addFV("isLex",isLexical(word)+"");
        d.addFV("hasNum", String.valueOf(hasNum(word)));
        d.addFV("hasHyphen", String.valueOf(hasHyphen(word)));

        // add prefix and suffix features
        for (int i = 1; i <= 5; i++) {
            d.addFV("prefix" + i, getPrefixN(word, i));
            d.addFV("suffix" + i, getSuffixN(word, i));
        }

        // add word cluster features if available
//        if (WordCluster.isActivated()) {
//            for (int i = 1; i <= 6; i++) {
//                d.addFV("clusterPrefix" + i, WordCluster.getClusterPrefix(word, i));
//            }
//        }
        if (WordCluster.isActivated()) {
            d.addFV("cluster", WordCluster.getClusterId(word));
        }


		//add bi-gram features
		preword = preword.toLowerCase();
		d.addFV("preword",preword); 
		d.addFV("preisNum", isNum(preword)+"");
		d.addFV("prebigram",baseword+":"+preword);
		//d.addFV("capPre", cap+":"+preword);

		nextword = nextword.toLowerCase();
		d.addFV("nextword",nextword); 
		d.addFV("nextisNum", isNum(nextword)+"");
		d.addFV("nextbigram",baseword+":"+nextword);
		return d;
	}

    private String getSuffixN(String s, int n) {
        int idx = 0;
        if (n < s.length()) {
            idx = s.length() - n;
        }
        return s.substring(idx);
    }

    private String getPrefixN(String s, int n) {
        int idx = s.length();
        if (n < s.length()) {
            idx = n;
        }
        return s.substring(0, idx);
    }

	private boolean isNum(String word){
        if (!(Character.isDigit(word.charAt(0)) || (word.charAt(0) == '+') || (word.charAt(0) == '-')
                || (word.charAt(0) == '.'))) {
            return false;
        }
		for(int i=1;i<word.length()-1;i++){
            if (!(Character.isDigit(word.charAt(i)) || (word.charAt(i) == '.') || (word.charAt(i) == ','))) {
                return false;
            }
		}
        return Character.isDigit(word.charAt(word.length()-1));
	}

    private boolean hasNum(String word){
        for(int i=0;i<word.length();i++){
            if(Character.isDigit(word.charAt(i))){
                return true;
            }
        }
        return false;
    }

	private boolean isLexical(String word){
		for(int i=0;i<word.length();i++){
			if(!Character.isLetter(word.charAt(i))){
				return false;
			}
		}
		return true;		
	}

    private boolean hasHyphen(String word) {
        for(int i=0;i<word.length();i++){
            if(word.charAt(i) == '-'){
                return true;
            }
        }
        return false;
    }

	//test the testdata	    
	void testSgmFile(String testfile, String outputfile) throws IOException{
		loadModel();
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				new File(outputfile)));	    
		Document doc = getDocument(testfile);
		List<Annotation> sentences = doc.annotationsOfType("sentence");
		if(sentences != null){
			for(Annotation sentence: sentences){
				List<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
				String[] words = new String[tokens.size()];
				for(int i=0;i<tokens.size();i++){
					words[i] = doc.text(tokens.get(i)).trim();
				}
				Union[] outcomes = tagSentence(words);
				for(int i=0;i<words.length;i++){
					POS pos = new POS(words[i], outcomes[i].getCurTag(), outcomes[i].prob);
					writer.write(pos.toString()+"\n");
				}
				writer.write("\n");
			}
		}
		writer.close();
	}	   

	void testTokenFile(String testfile, String outputfile) throws IOException{
		loadModel();
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				new File(outputfile)));
		BufferedReader reader = new BufferedReader(new FileReader(new File(testfile)));
		String line;
		List<String> tokens = new ArrayList<String>();
		while((line = reader.readLine()) != null){
			line = line.trim();
			if(line.length() == 0){
				if(tokens.size() > 0){
					String[] words = new String[tokens.size()];
					for(int i=0;i<tokens.size();i++){
						words[i] = tokens.get(i);
					}
					Union[] outcomes = tagSentence(words);
					for(int i=0;i<tokens.size();i++){
						POS pos = new POS(tokens.get(i), outcomes[i].getCurTag(), outcomes[i].prob);
						writer.write(pos.toString()+"\n");
					}				
					writer.write("\n");
					tokens = new ArrayList<String>();
				}
				continue;
			}
			else{
				POS p = POS.readPOS(line,false);
				tokens.add(p.word);
			}
		}

		if(tokens.size() > 0){
			String[] words = new String[tokens.size()];
			for(int i=0;i<tokens.size();i++){
				words[i] = tokens.get(i);
			}
			Union[] outcomes = tagSentence(words);
			for(int i=0;i<tokens.size();i++){
				POS pos = new POS(tokens.get(i), outcomes[i].getCurTag(), outcomes[i].prob);
				writer.write(pos.toString()+"\n");
			}			
			writer.write("\n");
		}
		writer.close();
		reader.close();		
	}

	public static Document getDocument(String sgmfile)throws IOException{
		ExternalDocument doc = new ExternalDocument("sgml", sgmfile);
		doc.setAllTags(true);
		doc.open();
		// find text segment(s)
		SpecialZoner.findSpecialZones (doc);
		// split into sentences
		Span textSpan = doc.fullSpan();
		Ace.monocase = Ace.allLowerCase(doc);
		SentenceSplitter.split (doc, textSpan);

		Span sp = doc.fullSpan();
		Tokenizer.tokenize (doc, sp);
		return doc;
	}

    public String[] tagTokens(String[] tokens) {
        Union[] outcomes = tagSentence(tokens);
        List<String> tags = new ArrayList<String>(outcomes.length);
        for (Union outcome : outcomes) {
            tags.add(outcome.getCurTag());
        }
        return tags.toArray(new String[tags.size()]);
    }

	Union[] tagSentence(String[] tokens){
		String preword = "$S";
		List<Datum> ds = new ArrayList<Datum>();
		for(int i=0;i<tokens.length;i++){
			String nextword = "$E";
			if(i < tokens.length-1)
				nextword = tokens[i+1];
			Datum d = buildDatum(tokens[i],preword, nextword);
			ds.add(d);
			preword = tokens[i];
		}

		Union[] outComes = new Union[ds.size()];
		String pretag = "$S";
        String prePretag = "$S";
		for(int i=0;i<outComes.length;i++){
			Datum d = ds.get(i);
			addPreTagInfo(prePretag, pretag, d);
			String tag = bestOutcome(d);
			outComes[i] = new Union(tag, prob(d, tag));
            prePretag = pretag;
			pretag = tag;
		}

		//		Viterbi vi = new Viterbi(ds, this);
		//		Union[] outComes = vi.decoding();

		return outComes;
	}

	static  void addPreTagInfo(String prePretag, String pretag, Datum d){
		d.addFV("pretag", pretag);
        d.addFV("prepretag", prePretag);
        d.addFV("combpretags", prePretag + ":" + pretag);
		d.addFV("pretagword", pretag+":"+d.getFeatureVal("word"));
	}

    private String[] tokenToWords(FuseDocument doc, Annotation sentence) {
        List<String> words = new ArrayList<String>();
        Vector<Annotation> tokensInCurrentDocument = doc.annotationsOfType("token");
        if (sentence != null) {
            Vector<Annotation> tokens = new Vector<Annotation>();
            for (Annotation token : tokensInCurrentDocument) {
                if (token.span().within(sentence.span()))  {
                    tokens.add(token);
                }
            }
            if (tokens != null) {
               for (Annotation token : tokens) {
                   words.add((String)token.get("tokenString"));
               }
            }
        }
        return words.toArray(new String[words.size()]);
    }

    private void addTags(FuseDocument doc, Annotation sentence, Union[] outcomes) {
        Vector<Annotation> tokensInCurrentDocument = doc.annotationsOfType("token");
        if (sentence != null) {
            Vector<Annotation> tokens = new Vector<Annotation>();
            for (Annotation token : tokensInCurrentDocument) {
                if (token.span().within(sentence.span()))  {
                    tokens.add(token);
                }
            }
            if (tokens != null) {
                int i = 0;
                for (Annotation token : tokens) {
                    Annotation pos = AnnotationFactory
                        .createSingleAttributeAnnotation("constit", doc, token.start(), token.end(), "cat", outcomes[i].getCurTag().toLowerCase());
                    doc.addAnnotation(pos);
                    Annotation posTag = AnnotationFactory
                            .createSingleAttributeAnnotation("tagger", doc, token.start(), token.end(), "cat", outcomes[i].getCurTag());
                    doc.addAnnotation(posTag);
                    i++;
                }
            }
        }
    }

    @Override
    public void initialize(String[] resourceInformation) {
        if (resourceInformation.length == 2) {
            try {
                WordCluster.loadFromFile(resourceInformation[1]);
            }
            catch (Exception e) {
                System.err.println("WARNING: Unable to load word cluster file for POSTagger");
            }
        }
        //this.featureFileName = resourceInformation[0];
        this.modelFileName = resourceInformation[0];
        try {
            loadModel();
        }
        catch (Exception e) {
            System.err.println("WARNING: Unable to load model for POSTagger");
        }
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) {
            System.err.println("WARNING: No sentence found in document.");
            return;
        }
        for (Annotation sentence : sentences) {
            String[] words = tokenToWords(doc, sentence);
            Union[] tags = tagSentence(words);
            addTags(doc, sentence, tags);
        }
    }
}

class Viterbi
{
	int sen_size;
	Matrix[] mats;
	List<Datum> ds;
	POSTagger tagger;

	Viterbi(List<Datum> ds, POSTagger tagger){
		this.ds = ds;
		this.tagger = tagger;
		sen_size = ds.size();
		mats = new Matrix[sen_size];
		for(int i=0;i<sen_size;i++){
			mats[i] = new Matrix(ds.get(i), tagger);
		}
	}

	//viterbi algorithm
	Union[] decoding(){
		Matrix preMat = null;
		for(int i= 0;i<sen_size;i++){			
			mats[i].reCalProbs(preMat);
			preMat = mats[i];
		}
		//find viterbi chain
		Union[] outComes = new Union[sen_size];
		outComes[sen_size -1 ] = mats[sen_size-1].getBestProb();
		for(int i = sen_size-2;i > -1; i --){
			outComes[i] = mats[i].getBestProbForCurTag(outComes[i+1].i, outComes[i+1].j);
		}
		return outComes;
	}

}

class Matrix{
	static boolean initialized = false;
	static List<String> tags;
	static int size;
	double[][][] probs;
	//we keep the original probability for future use
	double[][][] ori_probs;

	static void initialMatrix(String[] ts){
		if(initialized)
			return;
		size = ts.length + 1;
		tags = new ArrayList<String>();
		tags.add( "$S");
		for(int i=0;i<ts.length;i++){
			tags.add(ts[i]);
		}	
		initialized = true;
	}

	Matrix(Datum t,POSTagger tagger){
		if(!initialized){
			System.err.println("Error: you have to initialize the matrix property...");
			System.exit(1);
		}
		//probs = new double[size][size];
		ori_probs = new double[size][size][size];
		probs = new double[size][size][size];
		for(int i=0;i<size;i++){
			String prePretag = tags.get(i);
			// POSTagger.addPreTagInfo(pretag, t);;
			//for(int j= 1;j<size;j++){
            for (int j = 0; j < size; j++) {
                if ((i > 0) && (j == 0)) continue;
                String pretag = tags.get(j);
                POSTagger.addPreTagInfo(prePretag, pretag, t);
                for (int k = 1; k < size; k++) {
                    String curtag = tags.get(k);
                    ori_probs[i][j][k] = tagger.prob(t, curtag);
                }
			}
		}		
	}

	Union getBestProbForCurTag(int j, int k){
		double best = 0.0;
		int pos_i = -1;
        int pos_j = -1;
		for(int i=0;i<size;i++){
                if(probs[i][j][k] > best){
                    best = probs[i][j][k];
                    pos_i = i;
                    pos_j = j;
                }

		}
		if ((pos_i < 0) || (pos_j < 0)) {
			System.err.println("Error: getBestProbForCurTag - i or j < 0");
		}
		return new Union(this, pos_i, pos_j, k);
	}


	Union getBestProb(){
		double best = 0.0;
		int best_i = -1;
		int best_j = -1;
        int best_k = -1;
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
                for (int k = 0; k < size; k++) {
                    if(probs[i][j][k] > best){
                        best = probs[i][j][k];
                        best_i = i;
                        best_j = j;
                        best_k = k;
                    }

                }

			}
		}
		return new Union(this, best_i, best_j, best_k);
	}

	void reCalProbs(Matrix preMat){
		if(preMat == null){
			for(int k=1;k<size;k++)
				probs[0][0][k] = ori_probs[0][0][k];
		}
		else{
			for(int i = 0;i<size;i++){
                for (int j = 1; j < size; j++) {
                    Union u = preMat.getBestProbForCurTag(i, j);
                    double preprobs = preMat.probs[u.i][u.j][u.k];
                    for (int k=1; k < size; k++) {
                        probs[i][j][k] = ori_probs[i][j][k] * preprobs;
                    }
                }
			}
		}
		/*for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				System.err.print(probs[i][j]+" ");
			}
			System.err.println("");
		}*/
	}


}

class Union{	
	int i;
	int j;
    int k;
	double prob;

	Union(Matrix mat, int i, int j, int k){
		this.i = i;
		this.j = j;
        this.k = k;
		prob = mat.probs[i][j][k];
	}

	Union(String tag, double p ){
		i = 0;
        j = 0;
		k = Matrix.tags.indexOf(tag);
		prob = p;
	}

	String getCurTag(){
		return Matrix.tags.get(k);
	}
}


