package FuseJet.Tagger;


import java.util.HashMap;
import java.util.Map;

/**
 *  a data point, consisting of a set of features and an outcome, intended
 *  as part of the training set for a classifier.
 */

class Datum{
	Map<String, String> features = new HashMap<String, String>();
	String outcome = null;        

	Datum(){

	}
	
	Datum(String line){
		//System.err.println(line);
		String[] tokens = line.split(" ");
		for(int i=0;i<tokens.length-2;i++){
			String token = tokens[i];
			int pos = token.indexOf("=");
			if(pos <0){
				System.err.println(token);
				System.exit(1);
			}
			features.put(token.substring(0,pos), token.substring(pos+1));
		}	
		outcome = tokens[tokens.length-1];
	}
	
	void addFV(String f, String v){
		features.put(f,v);			
	}

	void setOutCome(String t){
		outcome = t;
	}

	String  getOutCome(){
		return outcome;
	}

	@Override
	public String toString() {
		String tmp = "";
		for(String feature:features.keySet()){
			tmp += feature+"="+features.get(feature)+" ";
		}
		tmp += outcome;
		return tmp;
	}

	public String getFeatureVal(String fe){
		if(features.containsKey(fe))
			return features.get(fe);
		else{
			System.err.println("Can not find feature "+fe+" in Datum :"+toString());
			return null;
		}
	}
	
	String[] toArray(){
		String[] array = new String[features.size()];
		int num = 0;
		for(String feature:features.keySet()){
			array[num++] = feature+"="+features.get(feature);
		}
		return array;
	}

}

