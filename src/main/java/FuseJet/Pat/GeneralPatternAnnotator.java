package FuseJet.Pat;

import FuseJet.Annotators.FuseJetAnnotator;
import FuseJet.Models.FuseDocument;
import FuseJet.Utils.Console;
import Jet.Pat.PatternCollection;
import Jet.Pat.PatternSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 6/17/12
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class GeneralPatternAnnotator implements FuseJetAnnotator {
    private PatternCollection patternCollection;

    private PatternSet patternSet;

    public void setPatternSet(PatternSet patternSet) {
        this.patternSet = patternSet;
    }

    public GeneralPatternAnnotator() {

    }

    public GeneralPatternAnnotator(String fileName) throws IOException {
        patternCollection = new PatternCollection();
        patternCollection.readPatternCollection(new FileReader(fileName));
        patternCollection.makePatternGraph();
    }

    public FuseDocument annotateWithAllPatternSet(FuseDocument doc) {
        Set patternSetNames = patternCollection.getPatternSetNames();
        for (Object patternSetName : patternSetNames) {
            try {
                System.err.println(patternSetName);
                patternCollection.apply((String)patternSetName, doc);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return doc;
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences != null) {
            for (Annotation sentence : sentences) {
                patternSet.apply(doc, sentence.span());
            }
        }
    }

    @Override
    public void initialize(String[] resourceInformation) {
        setPatternSet(Console.pc.getPatternSet(resourceInformation[0]));
    }
}
