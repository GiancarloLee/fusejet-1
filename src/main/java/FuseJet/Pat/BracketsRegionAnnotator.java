package FuseJet.Pat;

import FuseJet.Annotators.FuseJetAnnotator;
import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/**
 * User: yhe
 * Date: 6/7/12
 * Time: 5:17 PM
 */
public class BracketsRegionAnnotator implements FuseJetAnnotator {

    @Override
    public void initialize(String[] resourceInformation) {

    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> tokenAnnotations;

        for (Annotation sentence : doc.annotationsOfType("sentence")) {
            tokenAnnotations = doc.annotationsOfType("token", sentence.span());
            Stack<Annotation> leftPos = new Stack<Annotation>();
            for (Annotation tokenAnnotation : tokenAnnotations) {
                if (doc.text(tokenAnnotation.span()).trim().equals("(")) {
                    leftPos.push(tokenAnnotation);
                }
                if (doc.text(tokenAnnotation.span()).trim().equals(")") &&
                        !leftPos.empty()) {
                    Annotation leftAnn = leftPos.pop();
                    Annotation ann = AnnotationFactory.createDoubleAttributeAnnotation(
                            "bracketregion",
                            doc,
                            leftAnn.start(),
                            tokenAnnotation.end(),
                            "id",
                            doc.getNextAnnotationId(),
                            "text",
                            doc.text(new Span(leftAnn.end(), tokenAnnotation.start()))
                    );
                    doc.addAnnotation(ann);
                }
            }
        }
    }
}
