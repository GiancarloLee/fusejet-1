package FuseJet.Pat;

import FuseJet.Annotators.FuseJetAnnotator;
import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 6/7/12
 * Time: 5:13 PM
 * Fixes tokenizaton so that it conforms to existing XML elements
 */
public class NonConflictTokenAnnotator implements FuseJetAnnotator {
    @Override
    public void initialize(String[] resourceInformation) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void annotate(FuseDocument doc) {
        Vector<Annotation> tokenAnnotations = doc.annotationsOfType("token");
        boolean[] isSeparatingIdx = doc.findSeparatingIdx();
        for (Annotation tokenAnnotation : tokenAnnotations) {
            Span span = tokenAnnotation.span();
            int start = span.start();
            //int end   = span.end();
            List<Annotation> separatedTokens = new ArrayList<Annotation>();
            boolean isSplit = false;
            for (int i = span.start() + 1; i < span.end(); i++) {
                isSplit = isSplit || isSeparatingIdx[i];
                if (isSeparatingIdx[i]) {
                        Annotation separatedToken = AnnotationFactory.createSingleAttributeAnnotation(
                                "token",
                                doc,
                                start,
                                i,
                                "id",
                                doc.getNextAnnotationId()
                        );
                        separatedTokens.add(separatedToken);
                        start = i;
                }
            }
            if (start > span.start()) {
                Annotation separatedToken = AnnotationFactory.createSingleAttributeAnnotation(
                        "token",
                        doc,
                        start,
                        span.end(),
                        "id",
                        doc.getNextAnnotationId()
                );
                separatedTokens.add(separatedToken);
            }
            if (isSplit) {
                doc.removeAnnotation(tokenAnnotation);
                for (Annotation separatedToken : separatedTokens) {
                    doc.addAnnotation(separatedToken);
                }
            }
        }
    }


}
