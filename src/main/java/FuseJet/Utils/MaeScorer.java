package FuseJet.Utils;

import FuseJet.IO.MaeDocumentReader;
import FuseJet.Models.FuseDocument;

import java.io.IOException;

/**
 * User: yhe
 * Date: 11/11/12
 * Time: 6:11 PM
 */
public class MaeScorer {
    public static void main(String[] args) {
        MaeDocumentReader maeReader  = new MaeDocumentReader();
        try {
            String[] filePairs = FuseUtils.readLines(args[1]);
            for (String filePair : filePairs) {
                String[] files = filePair.split(";");
                FuseDocument goldDoc = maeReader.read(files[0]);
                FuseDocument testDoc = maeReader.read(files[1]);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
