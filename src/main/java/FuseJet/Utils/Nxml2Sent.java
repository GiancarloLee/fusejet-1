package FuseJet.Utils;

import FuseJet.IO.JournalArchiveXMLDocumentReader;
import FuseJet.Models.FuseDocument;

/**
 * User: yhe
 * Date: 6/14/12
 * Time: 10:45 AM
 */
public class Nxml2Sent {
    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.err.println("Nxml2Sent inputFileName outputFileName");
                System.exit(-1);
            }
            String inputFileName  = args[0];
            String outputFileName = args[1];
            JournalArchiveXMLDocumentReader reader = new JournalArchiveXMLDocumentReader();
            FuseDocument doc = reader.readXML(inputFileName);
            doc.saveOneLinePerSent(outputFileName);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
