package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Does not output FuseEntities but only FuseRelations.
 * FuseRelations will have START and END.
 */
public class ChineseTermWriter extends BAEDocumentWriter {
    @Override
    public void write(FuseDocument doc, String outputFileName) {
        Map<FuseAnnotation, String> entities = new HashMap<FuseAnnotation, String>();
        //Map<FuseRelation, Integer> relations = new HashMap<FuseRelation, Integer>();
        // For now, we don't control spans, as spans are already
        // we don't care about validAnnTypes also
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }

        List<Annotation> terms = doc.annotationsOfType("term");

        for (Annotation term : terms) {
            w.println(String.format("TERM START=%d END=%d TEXT=\"%s\"",
                    term.start(), term.end(), doc.text(term).replaceAll("\\s+", "")));
        }

        w.close();
    }

}
