package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import com.google.common.collect.ImmutableSet;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.ansj.util.recognition.NatureRecognition;
import tagger.*;
import domaintagger.*;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 * User: yhe
 * Date: 8/14/12
 * Time: 12:28 PM
 */
public class SimpleZhSentDocumentWriter extends InitializableFuseDocumentWriter {

    private String tagToParse = "";

    private boolean SEGMENTATION_DOUBLE_CHECK = true;

    private TaggerRunner tagger = null;
    @Override
    public void initialize(String[] resourceInformation) {
        if (resourceInformation.length != 2) {
            System.err.println("Chinese tagger initialization error: unable to obtain correct path.");
            System.exit(-1);
        }
        tagToParse = resourceInformation[0];
        if (tagToParse.trim().equals("")) {
            tagToParse = "fullspan";
        }
        tagger = TaggerRunner.instance(resourceInformation[1]);
    }

    private static final Set<String> NEW_LINE_ELEMENTS = ImmutableSet.of(
            "citation",
            "invention-title",
            "abstract",
            "heading",
            "p",
            "references-cited",
            "bibliographic-data",
            "description",
            "summary",
            "description-of-drawings",
            "detailed-desc",
            "claims",
            "claim");
//    private static final Set<String> NO_TRIM_ELEMENTS = ImmutableSet.of(
//            "claim-text",
//            "claim-ref",
//            "b",
//            "cross-ref"
//    );
    private static final Set<Character> SENTENCE_END_CHARS = ImmutableSet.of(
            '。',
        '；'
    );


    @Override
    public void write(FuseDocument doc, String outputFileName) {
        //TaggerRunner tagger = TaggerRunner.instance("/Users/yhe/Dropbox/work/FUSE/Release/Models/ChineseTagger/model.tag");
        boolean blankLine = true;
        char[] docChars = doc.text().toCharArray();
        doc.addAnnotation(new Annotation("fullspan", doc.fullSpan(), new FeatureSet()));
        PrintWriter w = null;
        StringBuilder builder = null;
        long taggingTime = 0, ansjTaggingTime = 0, ansjStartTime, startTime, endTime;
        try {
            w = new PrintWriter(new File(outputFileName));
            builder = new StringBuilder();
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
        Vector<Annotation> spanAnns = doc.annotationsOfType(tagToParse);
        if (spanAnns == null) {
            w.close();
            return;
        }
        else {
            for (Annotation spanAnn : spanAnns) {
                int i = spanAnn.start();
                while (i < spanAnn.end()) {
                    int j = skipAnnotationTo(doc, i);
                    if (j > i) {
                        if (!blankLine) {
                            //w.println();
                            //System.out.println(wordTag2String(tagger.tagString(builder.toString().trim())));
                            startTime = System.currentTimeMillis();
                            String taggedStr = buildTaggedString(builder.toString().trim());
                            endTime = System.currentTimeMillis();
                            taggingTime += (endTime-startTime);

                            w.print(taggedStr);
                            builder.setLength(0);
                            blankLine = true;
                        }
                        i = j;
                        continue;
                    }
//            if (docChars[i] == '\n') {
//                i++;
//                continue;
//            }
                    if (Character.isWhitespace(doc.charAt(i))) {
                        i++;
                        continue;
                    }
                    else {
                        if (!hasNewLineElements(doc.annotationsAt(i)) &&
                                !hasNewLineElements(doc.annotationsEndingAt(i)) &&
                                !SENTENCE_END_CHARS.contains(docChars[i])) {
                            //w.print(docChars[i]);
                            builder.append(docChars[i]);
                            blankLine = false;
                        }
                        else {
                            if (!blankLine) {
                                if (SENTENCE_END_CHARS.contains(docChars[i])) {
                                    builder.append(docChars[i]);
                                    blankLine = true;
                                }
                                startTime = System.currentTimeMillis();
                                String taggedStr = buildTaggedString(builder.toString().trim());
                                endTime = System.currentTimeMillis();
                                taggingTime += (endTime-startTime);

                                w.print(taggedStr);
                                //w.println();
                                //System.out.println(wordTag2String(tagger.tagString(builder.toString().trim())));
                                builder.setLength(0);
                                if (hasNewLineElements(doc.annotationsAt(i)) ||
                                        hasNewLineElements(doc.annotationsEndingAt(i))) {
                                    builder.append(docChars[i]);
                                    blankLine = false;
                                }
                            }
                            else {
                                builder.append(docChars[i]);
                                blankLine = false;
                            }
                        }
                    }
                    i++;

                }
            }
        }

        System.err.print("(tagging: "+taggingTime+" ms)\t");
        w.close();
    }

    public String buildTaggedString(String s) {
        TaggerRunner.WordTag[] wordTags = tagger.tagString(s);
        List<Term> ansjTaggedStr = ToAnalysis.paser(s);
        int[] ansjSegments = getSegmentIndices(ansjTaggedStr);
        //new NatureRecognition(ansjTaggedStr).recogntion();
        String taggedStr = wordTag2String(wordTags, ansjSegments);
        return taggedStr;
    }

    private int[] getSegmentIndices(List<Term> taggedString) {
        List<Integer> segmentIndices = new ArrayList<Integer>();
        segmentIndices.add(0);
        int len = 0;
        for (Term t : taggedString) {
            //System.out.println(t.getName());
            len += t.getName().length();
            segmentIndices.add(len);
        }
        int[] result = new int[segmentIndices.size()];
        for (int i = 0; i < segmentIndices.size(); i++) {
            result[i] = segmentIndices.get(i);
        }
        return result;
    }

    private String wordTag2String(TaggerRunner.WordTag[] wordTags, int[] segmentIndices) {
        boolean isFirstWordInNP = true;
        StringBuilder builder = new StringBuilder();
        int furthest = 0;
        int len = 0;
        int bufLen = 0;
        List<TaggerRunner.WordTag> wordTagBuffer = new ArrayList<TaggerRunner.WordTag>();
        for (TaggerRunner.WordTag wordTag : wordTags) {
            if (!isPartOfNP(wordTag)) {
                if (!wordTagBuffer.isEmpty()) {
                    //if ((numInArr(len-bufLen, 0, segmentIndices)) &&
                    //        (numInArr(len, 0, segmentIndices))) {
                    if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) ) {
                        furthest = len + bufLen;
                        if (isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                            appendBuilder(wordTagBuffer, "B-NP", builder);
                        }
                        else {
                            appendBuilder(wordTagBuffer, "O", builder);
                        }
                    }
                    else {
                        appendBuilder(wordTagBuffer, "O", builder);
                    }
                    wordTagBuffer.clear();
                    bufLen = 0;
                }
                appendBuilder(wordTag, "O", builder);
                len += wordTag.getWord().length();
            }
            else {
                wordTagBuffer.add(wordTag);
                len += wordTag.getWord().length();
                bufLen += wordTag.getWord().length();
            }
        }
        if (!wordTagBuffer.isEmpty()) {
            //if ((numInArr(len - bufLen, 0, segmentIndices)) &&
            //        (numInArr(len, 0, segmentIndices)) &&
            if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) &&
                    isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                appendBuilder(wordTagBuffer, "B-NP", builder);
            }
            else {
                appendBuilder(wordTagBuffer, "O", builder);
            }
            wordTagBuffer.clear();
            bufLen = 0;
        }
        builder.append("\n");
        return builder.toString();
    }

    private boolean segmentationAgreed(List<TaggerRunner.WordTag> wordTags, int offset, int[] segmentationIndices) {
        if (!SEGMENTATION_DOUBLE_CHECK) return true;
        int segPoint = offset;
        if (!numInArr(segPoint, 0, segmentationIndices)) return false;
        for (TaggerRunner.WordTag wordTag : wordTags) {
            int i = segPoint + 1;
            segPoint += wordTag.getWord().length();
            for (;i < segPoint; i++) {
                if (numInArr(i, 0, segmentationIndices)) return false;
            }
            if (!numInArr(segPoint, 0, segmentationIndices)) return false;
        }
        return true;
    }

    private void appendBuilder(List<TaggerRunner.WordTag> wordTags, String tag, StringBuilder b) {
        for (TaggerRunner.WordTag wordTag : wordTags) {
            b.append(wordTag.getWord())
                    .append("\t")
                    .append(wordTag.getWord())
                    .append("\t")
                    .append(tag.equals("O") ? "O" : wordTag.getTag())
                    .append("\t")
                    .append(tag)
                    .append("\n");
            tag = tag.equals("O") ? "O" : "I-NP";
        }
    }

    private void appendBuilder(TaggerRunner.WordTag wordTag, String tag, StringBuilder b) {
        b.append(wordTag.getWord())
                .append("\t")
                .append(wordTag.getWord())
                .append("\t")
                .append(tag.equals("O") ? "O" : wordTag.getTag())
                .append("\t")
                .append(tag)
                .append("\n");
    }

    private boolean numInArr(int num, int furthest, int[] arr) {
        for (int i = furthest; i < arr.length; i++) {
            if (num == arr[i]) return true;
        }
        return false;
    }

    private boolean isPartOfNP(TaggerRunner.WordTag wordTag) {
        return (wordTag.getTag().equals("NN") ||
            wordTag.getTag().equals("NR") ||
            wordTag.getTag().equals("JJ"));
    }

    private boolean isEndOfNP(TaggerRunner.WordTag wordTag) {
        return wordTag.getTag().equals("NN") ||
                wordTag.getTag().equals("NR");
    }

    private boolean hasNewLineElements(Vector<Annotation> anns) {
        if (anns == null) return false;
        for (Annotation ann : anns) {
            if (NEW_LINE_ELEMENTS.contains(ann.type())) {
                return true;
            }
        }
        return false;
    }

    private int skipAnnotationTo(FuseDocument doc, int i) {
        Vector<Annotation> anns = doc.annotationsAt(i);
        if (anns == null) return i;
        for (Annotation ann : anns) {
            if ((ann.get("lang") != null) && (((String)ann.get("lang")).trim().equals("eng"))) {
                return ann.end();
            }
        }
        return i;
    }

}
