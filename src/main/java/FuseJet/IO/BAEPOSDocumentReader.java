package FuseJet.IO;

import FuseJet.Models.FuseDocument;

import java.io.IOException;

/**
 * User: yhe
 * Date: 6/27/12
 * Time: 3:53 PM
 */
public class BAEPOSDocumentReader extends BAEDocumentReader {

    public FuseDocument read(String txtFileName, String factFileName, String posFileName) {
        FuseDocument doc = read(txtFileName, factFileName);
        try {
            doc.loadGeniaAnnotations(posFileName);
        }
        catch (IOException e) {
            System.err.println("WARNING: Error loading POS and chunking results. Relation Extraction might not work.");
        }
        return doc;
    }

    @Override
    public FuseDocument read(String fileIdentifier) {
        String[] parts = fileIdentifier.split(";");
        if (parts.length != 3) {
            System.err.println("A BAEPOS file identifier should contain the txt filename, the fact filename, " +
                    "and the pos-tagged filename" +
                    "separated by ;");
            return null;
        }
        else {
            String txtFileName = parts[0];
            String factFileName = parts[1];
            String posFileName = parts[2];
            return read(txtFileName, factFileName, posFileName);
        }
    }
}
