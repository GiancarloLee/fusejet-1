package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * User: yhe
 * Date: 7/16/12
 * Time: 4:59 PM
 */
public class TokenizedSentDocumentWriter implements FuseDocumentWriter {

    @Override
    public void write(FuseDocument doc, String outputFileName) {

        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (IOException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }

        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) {
            w.close();
            return;
        }
        for (Annotation sent : sentences) {
            if (doc.annotationsOfType("token", sent.span()).size() > FuseDocument.MAX_SENTENCE_LENGTH) {
                continue;
            }
            StringBuilder sb = new StringBuilder();
            Vector<Annotation> tokens = doc.annotationsOfType("token", sent.span());
            for (Annotation token : tokens) {
                sb.append(token.get("tokenString")).append(" ");
            }
            w.println(sb.toString().trim());
        }


        w.close();

    }
}
