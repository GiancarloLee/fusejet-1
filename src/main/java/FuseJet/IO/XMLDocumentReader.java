package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * User: yhe
 * Date: 5/30/12
 * Time: 12:14 PM
 */
public interface XMLDocumentReader {
    public FuseDocument readXML(String fileName) throws ParserConfigurationException, IOException, SAXException;
}
