package FuseJet.IO;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseEntitySpan;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import com.google.common.collect.ImmutableSet;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 6/3/12
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class LexisNexisPatentXMLDocumentReader implements XMLDocumentReader, FuseDocumentReader {
    private static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    private static final String NAME_OF_BIB_NODE = "bibliographic-data";

    private static final Set<String> IGNORABLE_ELEMENTS_IN_TEXT = ImmutableSet.of(
            "legal-data");
    private static final Set<String> BIB_ELEMENTS_IN_TEXT = ImmutableSet.of(
            "references-cited",
            "invention-title");
    private static final Set<String> NEW_LINE_ELEMENTS = ImmutableSet.of(
            "citation",
            "invention-title",
            "abstract",
            "heading",
            "p",
            "references-cited",
            "bibliographic-data",
            "description",
            "summary",
            "description-of-drawings",
            "detailed-desc",
            "claims",
            "claim");
    private static final Set<String> NO_TRIM_ELEMENTS = ImmutableSet.of(
            "claim-text",
            "claim-ref",
            "b",
            "cross-ref"
    );

    private FuseDocument fuseDocument;

    @Override
    public FuseDocument read(String fileIdentifier) {
        try {
            return readXML(fileIdentifier);
        }
        catch (Exception e) {
            System.err.println("Error reading " + fileIdentifier + "  Stack trace:");
            e.printStackTrace();
            return new FuseDocument();
        }
    }

    @Override
    public FuseDocument readXML(String fileName) throws ParserConfigurationException, IOException, SAXException {
        fuseDocument = new FuseDocument();

        builderFactory.setValidating(false);

        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        builder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                return new InputSource(new StringReader(""));
            }
        });
        org.w3c.dom.Document doc = builder.parse(new File(fileName));
        Element e = doc.getDocumentElement();
        StringBuilder s = new StringBuilder();
        readFromNode(e, fuseDocument, s);
        fuseDocument.setText(s.toString());
        addMetaData(fuseDocument);
        //fuseDocument.annotate("text", fuseDocument.fullSpan(), new FeatureSet());
        //fuseDocument.splitAndTokenize();
        return fuseDocument;
    }


    private Set<String> findNames(FeatureSet fs) {
        Set<String> result = new HashSet<String>();
        String fname = fs.get("FNAME") == null ? "" : ((String)fs.get("FNAME")).trim();
        String lname = fs.get("LNAME") == null ? "" : ((String)fs.get("LNAME")).trim();
        result.add((fname + " " + lname).trim());
        result.add((lname + " " + fname).trim());
        return result;
    }

    private void addMetaData(FuseDocument doc) {
        // document
        FuseAnnotation ann = new FuseAnnotation("meta-data",
                new FuseEntitySpan(0, 0),
                new FeatureSet("CATEGORY", "DOCUMENT", "COLLECTION", "LEXISNEXIS", "LANGUAGE", "EN"),
                FuseAnnotation.AnnotationCategory.NA,
                FuseAnnotation.AnnotationType.NA);
        doc.addAnnotation(ann);
        // paper
        String pubId = extractDocId(doc, "publication-reference");
        String appId = extractDocId(doc, "application-reference");
        String year = extractYear(doc, "application-reference");
        String title = doc.text(doc.annotationsOfType("invention-title").get(0)).trim();
        FeatureSet fs = new FeatureSet();
        fs.put("CATEGORY", "PAPER");
        fs.put("ID", appId);
        fs.put("PUB_ID", pubId);
        fs.put("APP_ID", appId);
        fs.put("TITLE", title);
        fs.put("YEAR", year);
        ann = new FuseAnnotation("meta-data",
                new FuseEntitySpan(0, 0),
                fs,
                FuseAnnotation.AnnotationCategory.NA,
                FuseAnnotation.AnnotationType.NA);
        doc.addAnnotation(ann);

        // AUTHOR
        int count = 0;
        Set<String> forbiddenNames = new HashSet<String>();
        List<FeatureSet> authors = extractAuthors(doc, "applicants", "applicant");
        for (FeatureSet author : authors) {
            author.put("ID" ,"NYU_AUTHOR_" +appId + "_" + count);
            count++;
            //forbiddenNames.add(((String)fs.get("FNAME")).trim() + " " +
            //        ((String)fs.get("LNAME")).trim());
            forbiddenNames.addAll(findNames(fs));
            ann = new FuseAnnotation("meta-data",
                    new FuseEntitySpan(0, 0),
                    author,
                    FuseAnnotation.AnnotationCategory.NA,
                    FuseAnnotation.AnnotationType.NA);
            doc.addAnnotation(ann);
        }
        authors = extractAuthors(doc, "inventors", "inventor");
        for (FeatureSet author : authors) {
            author.put("ID" ,"NYU_AUTHOR_" +appId + "_" + count);
            count++;
            ann = new FuseAnnotation("meta-data",
                    new FuseEntitySpan(0, 0),
                    author,
                    FuseAnnotation.AnnotationCategory.NA,
                    FuseAnnotation.AnnotationType.NA);
            doc.addAnnotation(ann);
        }

        // ORG
        count = 0;
        List<FeatureSet> orgs = extractOrgs(doc, "applicants", "applicant");
        for (FeatureSet org : orgs) {
            if (forbiddenNames.contains(((String)org.get("NAME")).trim())) {
                continue;
            }
            org.put("ID" ,"NYU_ORG_" +appId + "_" + count);
            count++;
            ann = new FuseAnnotation("meta-data",
                    new FuseEntitySpan(0, 0),
                    org,
                    FuseAnnotation.AnnotationCategory.NA,
                    FuseAnnotation.AnnotationType.NA);
            doc.addAnnotation(ann);
        }
        orgs = extractOrgs(doc, "inventors", "inventor");
        for (FeatureSet org : orgs) {
            org.put("ID" ,"NYU_ORG_" +appId + "_" + count);
            count++;
            ann = new FuseAnnotation("meta-data",
                    new FuseEntitySpan(0, 0),
                    fs,
                    FuseAnnotation.AnnotationCategory.NA,
                    FuseAnnotation.AnnotationType.NA);
            doc.addAnnotation(ann);
        }

    }

    private List<FeatureSet> extractOrgs(FuseDocument doc, String groupTag, String orgTag) {
        List<FeatureSet> results = new ArrayList<FeatureSet>();
        Vector<Annotation> apps = doc.annotationsOfType(groupTag);
        if (apps != null) {
            Vector<Annotation> applicants = doc.annotationsOfType(orgTag, apps.get(0).span());
            if (applicants != null) {
                for (Annotation applicant : applicants) {
                    Vector<Annotation> orgnames = doc.annotationsOfType("orgname", applicant.span());
                    if (orgnames != null) {
                        String orgName = doc.text(orgnames.get(0)).trim();
                        FeatureSet fs = new FeatureSet("CATEGORY", "ORG", "NAME", orgName, "ROLE", orgTag.toUpperCase());
                        results.add(fs);
                    }
                }
            }
        }
        return results;
    }

    private List<FeatureSet> extractAuthors(FuseDocument doc, String groupTag, String orgTag) {
        List<FeatureSet> results = new ArrayList<FeatureSet>();
        Vector<Annotation> apps = doc.annotationsOfType(groupTag);
        if (apps != null) {
            Vector<Annotation> applicants = doc.annotationsOfType(orgTag, apps.get(0).span());
            if (applicants != null) {
                for (Annotation applicant : applicants) {
                    Vector<Annotation> firstNames = doc.annotationsOfType("first-name", applicant.span());
                    if (firstNames != null) {
                        String firstName = doc.text(firstNames.get(0)).trim();
                        Vector<Annotation> lastNames = doc.annotationsOfType("last-name", applicant.span());
                        if (lastNames != null) {
                            String lastName = doc.text(lastNames.get(0)).trim();
                            FeatureSet fs = new FeatureSet("CATEGORY", "AUTHOR", "FNAME", firstName, "LNAME", lastName);
                            fs.put("ROLE", orgTag.toUpperCase());
                            results.add(fs);
                        }
                    }
                }
            }
        }
        return results;
    }

    private String extractDocId(FuseDocument doc, String annType) {
        String result = "";
        Vector<Annotation> publicationRefs = doc.annotationsOfType(annType);
        if (publicationRefs != null) {
            Vector<Annotation> countries = doc.annotationsOfType("country", publicationRefs.get(0).span());
            if (countries != null) {
                String country = doc.text(countries.get(0)).trim();
                Vector<Annotation> docNums = doc.annotationsOfType("doc-number", publicationRefs.get(0).span());
                if (docNums != null) {
                    result = country + doc.text(docNums.get(0)).trim();
                }
            }
        }
        return result;
    }

    private String extractYear(FuseDocument doc, String annType) {
        String result = "";
        Vector<Annotation> publicationRefs = doc.annotationsOfType(annType);
        if (publicationRefs != null) {
            Vector<Annotation> dates = doc.annotationsOfType("date", publicationRefs.get(0).span());
            if (dates != null) {
                String date = doc.text(dates.get(0)).trim();
                if (date.length() >= 4)
                    result = date.substring(0, 4);
            }
        }
        return result;
    }

    private void readFromNode(Node node, FuseDocument doc, StringBuilder s) {
        if (node instanceof Element) {
            // Add annotation:
            int start = s.length();
            if (node.getNodeName().equals(NAME_OF_BIB_NODE)) {
                NodeList nodes = node.getChildNodes();
                Node item;
                for (int i = 0; i < nodes.getLength(); i++) {
                    item = nodes.item(i);
                    //if (BIB_ELEMENTS_IN_TEXT.contains(item.getNodeName())) {
                    if (true) {
                        readFromNode(item, doc, s);
                    }
                }
            }
            else {
                //if (!IGNORABLE_ELEMENTS_IN_TEXT.contains(node.getNodeName())) {
                if (true) {
                    NodeList nodes = node.getChildNodes();
                    Node item;

                    for (int i = 0; i < nodes.getLength(); i++) {
                        item = nodes.item(i);
                        readFromNode(item, doc, s);
                    }
                }
            }
            int end = s.length();


            Annotation ann = AnnotationFactory.createAnnotation(node.getNodeName().trim().toLowerCase(),
                    doc,
                    start,
                    end);

            // record id for invention-title id="title_eng" and
            // abstract id="abstr_eng"
            if (node.getNodeName().trim().toLowerCase().equals("invention-title")) {
                String id = ((Element) node).getAttribute("lang");
                if (id != null) {
                    ann.put("lang", id);
                }
                else {
                    ann.put("lang", "");
                }
            }
            if (node.getNodeName().trim().toLowerCase().equals("abstract")) {
                String id = ((Element) node).getAttribute("lang");
                if (id != null) {
                    ann.put("lang", id);
                }
                else {
                    ann.put("lang", "");
                }
            }
            doc.addAnnotation(ann);

        }
        else {
            String textString = node.getTextContent();
            s.append(textString);

        }

    }

    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.err.println("java -cp jet.jar LexisNexisPatentXMLDocumentReader inputFile outputMaeFile");
                return;
            }
            String inputFile = args[0];
            String outputMaeFile = args[1];
            LexisNexisPatentXMLDocumentReader reader = new LexisNexisPatentXMLDocumentReader();
            FuseDocument doc = reader.readXML(inputFile);
            doc.saveToMaeFile(outputMaeFile);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
