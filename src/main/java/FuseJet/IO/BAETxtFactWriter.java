package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 3/2/13
 * Time: 4:09 PM
 */
public class BAETxtFactWriter implements FuseDocumentWriter  {
    private int cnt = 0;
    @Override
    public void write(FuseDocument fuseDocument, String s) {
        cnt = 0;
        FuseDocument doc = fuseDocument;
        String[] parts = s.split(";");
        String textFile = parts[0];
        String factFile = parts[1];
        PrintWriter textWriter, factWriter;
        try {
            textWriter = new PrintWriter(new File(textFile));
            factWriter = new PrintWriter(new File(factFile));
        } catch (FileNotFoundException e) {
            return;
        }
        String text = fuseDocument.text();
        textWriter.write(text);
        textWriter.close();

        Vector<Annotation> metadata = doc.annotationsOfType("meta-data");
        if (metadata != null) {
            for (Annotation metaDatum : metadata) {
                factWriter.print(metaDatum.get("CATEGORY"));
                FeatureSet fs = metaDatum.attributes();
                Enumeration it = fs.keys();
                while (it.hasMoreElements()) {
                    String key = (String)it.nextElement();
                    if (key.equals("CATEGORY")) {
                        continue;
                    }
                    if (key.equals("YEAR")) {
                        factWriter.print(" YEAR=" + fs.get(key));
                    }
                    else {
                        factWriter.print(" " + key + "=\"" + fs.get(key) + "\"");
                    }
                }
                factWriter.println();
            }
        }

        Vector<Annotation> sections = new Vector<Annotation>();
        Vector<Annotation> docSegments = doc.annotationsOfType("doc_segment");
        if (docSegments != null) {
            sections = docSegments;
        }
        else {
            docSegments = doc.annotationsOfType("section");
            if (docSegments != null) {
                sections = docSegments;
            }
        }
        Vector<Annotation> structures = doc.annotationsOfType("structure");
        if (structures != null) {
            sections.addAll(structures);
        }

        for (Annotation section : sections) {
            printSection(section, factWriter);
        }
        int i = 1;
        for (FuseAnnotation entity : fuseDocument.getFuseEntities()) {
            //System.err.println("Found citation...");
            factWriter.println(String.format("CITATION ID=\"NYU_PATENT_ARTICLE_CITE_%d\" " +
                    "START=%d " +
                    "END=%d " +
                    "CITE_CLASS=\"article\"",
                    i, entity.start(), entity.end()));
            i++;
        }
        List<Annotation> citations = doc.annotationsOfType("CITATION");
        if (citations != null) {
            for (Annotation citation : citations) {
                //System.err.println("Found citation...");
                factWriter.println(String.format("CITATION ID=\"NYU_PATENT_ARTICLE_CITE_%d\" " +
                        "START=%d " +
                        "END=%d " +
                        "CITE_CLASS=\"article\"",
                        i, citation.start(), citation.end()));
                i++;
            }
        }

        factWriter.close();
    }

    private void printSection(Annotation ann, PrintWriter w) {
        String title = "";
        String sectionId = "";
        if (ann.get("title") != null) {
            title = ((String)ann.get("title")).trim();
        }
        if (ann.get("id") != null) {
            sectionId = ((String)ann.get("id")).trim();
        }
        String type = (String)ann.get("TYPE");
        if (type == null) {
            type = "";
        }
        if (ann.type().equals("section")) {
            type = "doc_segment";
        }
        if (ann.type().equals("section"))  {
            w.println(String.format("SECTION ID=\"%s\" TYPE=\"%s\" TITLE=\"%s\" START=%d END=%d",
                    "NYU_PATENT_SEC_" + cnt, "SECTION", title, ann.start(), ann.end()));
            cnt++;
        }
        if (ann.type().equals("structure")) {
            w.println(String.format("STRUCTURE TYPE=\"%s\" START=%d END=%d",
                    type.toUpperCase(), ann.start(), ann.end()));
        }

    }
}
