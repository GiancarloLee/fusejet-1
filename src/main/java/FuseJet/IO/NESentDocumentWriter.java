package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import FuseJet.Utils.AnnotationStartComparator;
import FuseJet.Utils.Console;
import FuseJet.Utils.FuseUtils;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import sun.swing.SwingUtilities2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 6/26/12
 * Time: 2:48 PM
 */
public class NESentDocumentWriter implements FuseDocumentWriter {
    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        String[] annTypes = {"ENAMEX"};
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }

        Vector<Annotation> sections = new Vector<Annotation>();
        Vector<Annotation> docSegments = doc.annotationsOfType("doc_segment");
        if (docSegments != null) {
            sections = docSegments;
        }
        else {
            docSegments = doc.annotationsOfType("section");
            if (docSegments != null) {
                sections = docSegments;
            }
        }
        Vector<Annotation> footnotes = doc.annotationsOfType("footnote");
        if (footnotes != null) {
            sections.addAll(footnotes);
        }

//        Vector<Annotation> texts = doc.annotationsOfType("text");
//        if (texts != null) {
//            sections.addAll(texts);
//        }

        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) {
            sentences = new Vector<Annotation>(0);
        }

        Comparator<Annotation> annotationComparator = new SectionAnnotationComparator();

        Collections.sort(sections, annotationComparator);
        Collections.sort(sentences, annotationComparator);
        //System.err.println(sections.size());
        //System.err.println(sentences.size());

        int i = 0;
        int j = 0;
        int prevSentenceStart = -1;
        while ((i < sections.size()) || (j < sentences.size())) {
            if (i == sections.size())  {
                while (j < sentences.size()) {
                    Annotation sentence = sentences.get(j);
                    if (sentence.start() > prevSentenceStart) {
                        printSentence(doc, sentence, w);
                        prevSentenceStart = sentence.start();
                    }
                    j++;
                }
                break;
            }
            if (j == sentences.size()) {
                while (i < sections.size()) {
                    Annotation section = sections.get(i);
                    printSection(section, w);
                    i++;
                }
                break;
            }
            if (sentences.get(j).start() >= sections.get(i).start()) {
                printSection(sections.get(i), w);
                i++;

            }
            else {
                if (sentences.get(j).start() > prevSentenceStart) {
                    printSentence(doc, sentences.get(j), w);
                    prevSentenceStart = sentences.get(j).start();
                }
                j++;
            }
        }

//        Set<Integer> usedStart = new HashSet<Integer>();
//        for (Annotation ann : anns) {
//            Span span = ann.span();
//            usedStart.clear();
//            Vector<Annotation> sentences = doc.annotationsOfType("sentence", span);
//            if (sentences == null) {
//                return;
//            }
//            Annotation.sort(sentences);
//            Collections.reverse(sentences);
//            String title = "";
//            String sectionId = "";
//            if (ann.get("title") != null) {
//                title = ((String)ann.get("title")).trim();
//            }
//            if (ann.get("id") != null) {
//                sectionId = ((String)ann.get("id")).trim();
//            }
//            w.println(String.format("SECTION ID=\"%s\" TYPE=\"%s\" TITLE=\"%s\" START=\"%d\" END=\"%d\"",
//                    sectionId, ann.type(), title, ann.start(), ann.end()));
//            for (Annotation sentence : sentences) {
//                if (usedStart.contains(sentence.start())) {
//                    continue;
//                }
//                w.println(sentence.start() + " " +
//                        doc.writeSGML(validAnnTypes, sentence.span().start(), sentence.span().end()).toString().replace('\n', ' '));
//                // usedStart.add(sentence.start());
//            }
//        }

        w.close();
    }

    private void printSection(Annotation ann, PrintWriter w) {
        String title = "";
        String sectionId = "";
        if (ann.get("title") != null) {
            title = ((String)ann.get("title")).trim();
        }
        if (ann.get("id") != null) {
            sectionId = ((String)ann.get("id")).trim();
        }
        String type = ann.type();
        if (ann.type().equals("section")) {
            type = "doc_segment";
        }
        w.println(String.format("SECTION ID=\"%s\" TYPE=\"%s\" TITLE=\"%s\" START=\"%d\" END=\"%d\"",
                sectionId, type, title, ann.start(), ann.end()));
    }

    private void printSentence(FuseDocument doc, Annotation sentence , PrintWriter w) {
        w.println(sentence.start() + " " +doc.writeSGML(new String[]{"ENAMEX"}, sentence.span().start(), sentence.span().end()).toString().replace('\n', ' '));
    }

    public class SectionAnnotationComparator extends AnnotationStartComparator {

        public int compare(Annotation annotation1, Annotation annotation2) {
            Map<String, Integer> annValue = new HashMap<String, Integer>(3);
            annValue.put("section", 1);
            annValue.put("doc_segment", 2);
            annValue.put("footnote", 3);
            annValue.put("text", 4);
            int result = super.compare(annotation1, annotation2);
            if (result != 0) return result;
            else {
                return annValue.get(annotation1.type().toLowerCase()) - annValue.get(annotation2.type().toLowerCase());
            }
        }
    }
}
