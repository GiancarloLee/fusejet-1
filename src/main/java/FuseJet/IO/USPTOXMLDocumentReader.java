package FuseJet.IO;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import com.google.common.collect.ImmutableSet;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 3/3/13
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class USPTOXMLDocumentReader implements FuseDocumentReader {
    private static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    private static final String NAME_OF_BIB_NODE = "bibliographic-data";
//    private static final Set<String> IGNORABLE_ELEMENTS_IN_TEXT = ImmutableSet.of(
//            "legal-data");
//    private static final Set<String> BIB_ELEMENTS_IN_TEXT = ImmutableSet.of(
//            "references-cited",
//            "invention-title");
//    private static final Set<String> NEW_LINE_ELEMENTS = ImmutableSet.of(
//            "citation",
//            "invention-title",
//            "abstract",
//            "heading",
//            "p",
//            "references-cited",
//            "bibliographic-data",
//            "description",
//            "summary",
//            "description-of-drawings",
//            "detailed-desc",
//            "claims",
//            "claim");
//    private static final Set<String> NO_TRIM_ELEMENTS = ImmutableSet.of(
//            "claim-text",
//            "claim-ref",
//            "b",
//            "cross-ref"
//    );

    private FuseDocument fuseDocument;

    @Override
    public FuseDocument read(String fileIdentifier) {
        try {
            return readXML(fileIdentifier);
        } catch (Exception e) {
            System.err.println("Error reading " + fileIdentifier + "  Stack trace:");
            e.printStackTrace();
            return new FuseDocument();
        }
    }

    public FuseDocument readXML(String fileName) throws ParserConfigurationException, IOException, SAXException {
        fuseDocument = new FuseDocument();

        builderFactory.setValidating(false);

        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        builder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                return new InputSource(new StringReader(""));
            }
        });
        org.w3c.dom.Document doc = builder.parse(new File(fileName));
        Element e = doc.getDocumentElement();
        StringBuilder s = new StringBuilder();
        readFromNode(e, fuseDocument, s);
        fuseDocument.setText(s.toString());
        fuseDocument.annotate("text", fuseDocument.fullSpan(), new FeatureSet());
        fuseDocument.splitAndTokenize();
        return fuseDocument;
    }

    private void readFromNode(Node node, FuseDocument doc, StringBuilder s) {
        if (node instanceof Element) {
            // Add annotation:
            int start = s.length();

            //if (!IGNORABLE_ELEMENTS_IN_TEXT.contains(node.getNodeName())) {
            if (true) {
                NodeList nodes = node.getChildNodes();
                Node item;

                for (int i = 0; i < nodes.getLength(); i++) {
                    item = nodes.item(i);
                    readFromNode(item, doc, s);
                }
            }
            int end = s.length();
            Annotation ann = AnnotationFactory.createAnnotation(node.getNodeName().trim().toLowerCase(),
                    doc,
                    start,
                    end);
            // record id for invention-title id="title_eng" and
            // abstract id="abstr_eng"
            if (node.getNodeName().trim().toLowerCase().equals("invention-title")) {
                String id = ((Element) node).getAttribute("lang");
                if (id != null) {
                    ann.put("lang", id);
                } else {
                    ann.put("lang", "");
                }
            }
            if (node.getNodeName().trim().toLowerCase().equals("abstract")) {
                String id = ((Element) node).getAttribute("lang");
                if (id != null) {
                    ann.put("lang", id);
                } else {
                    ann.put("lang", "");
                }
            }
            doc.addAnnotation(ann);

        }
        else {
            String textString = node.getTextContent();
            s.append(textString);
        }
    }

    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.err.println("java -cp jet.jar LexisNexisPatentXMLDocumentReader inputFile outputMaeFile");
                return;
            }
            String inputFile = args[0];
            String outputMaeFile = args[1];
            LexisNexisPatentXMLDocumentReader reader = new LexisNexisPatentXMLDocumentReader();
            FuseDocument doc = reader.readXML(inputFile);
            doc.saveToMaeFile(outputMaeFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


