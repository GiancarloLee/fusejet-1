package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * User: yhe
 * Date: 7/18/12
 * Time: 3:21 PM
 */
public class CONLLTagDocumentWriter implements FuseDocumentWriter {
    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) {
            w.close();
            return;
        }
        for (Annotation sentence : sentences) {
            w.println(sentence.start());
            Vector<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
            if (tokens != null) {
                String pos = "NN";
                int tagStart = -1;
                int tagEnd   = -1;
                String chunk = "O";
                for (Annotation token : tokens) {
                    Vector<Annotation> posAnns = doc.annotationsAt(token.start(), "tagger");
                    if (posAnns == null) pos = "NN";
                    else pos = (String)posAnns.get(0).get("cat");
                    if (token.start() > tagEnd) {
                        Vector<Annotation> ngAnns = doc.annotationsAt(token.start(), "ng");
                        if (ngAnns != null) {
                            tagStart = ngAnns.get(0).start();
                            tagEnd = ngAnns.get(0).end();
                        }
                    }
                    if (tagStart == token.start()) {
                        chunk = "B-NP";
                    }
                    else {
                        if ((tagStart < token.start()) && (tagEnd >= token.end())) {
                            chunk = "I-NP";
                        }
                        else {
                            chunk = "O";
                        }
                    }
                    w.println(String.format("%s\t%s\t%s\t%s",
                            ((String)token.get("tokenString")).trim(),
                            ((String)token.get("tokenString")).trim(),
                            pos,
                            chunk));
                }
            }
            w.println();
        }
        w.close();
    }
}
