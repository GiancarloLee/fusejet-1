package FuseJet.IO;

import FuseJet.Models.*;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Date: 10/28/12
 * Time: 5:35 PM
 * @author yhe & gcl
 */
public class MaeDocumentReader implements FuseDocumentReader {
    private static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    private Map<String, FuseAnnotation> annMap = new HashMap<String, FuseAnnotation>();

    @Override
    public FuseDocument read(String fileIdentifier) {
        FuseDocument doc = new FuseDocument();
        builderFactory.setValidating(false);
        try {
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            builder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId)
                        throws SAXException, IOException {
                    return new InputSource(new StringReader(""));
                }
            });
            org.w3c.dom.Document xmlDoc = builder.parse(new File(fileIdentifier));
            Element e = xmlDoc.getDocumentElement();
            String text = e.getElementsByTagName("TEXT").item(0).getTextContent();
            Element tagsElem = (Element)e.getElementsByTagName("TAGS").item(0);
            doc.setText(text);
            Annotation textRegionAnn = AnnotationFactory.createAnnotation("text_region", doc, 0, doc.text().length());
            doc.addAnnotation(textRegionAnn);
            Annotation textAnn = AnnotationFactory.createAnnotation("text", doc, 0, doc.text().length());
            doc.addAnnotation(textAnn);
            doc.splitAndTokenize();
            readFromNode(tagsElem, doc);
            Annotation fileNameAnn = new Annotation("fileName", new Span(0, 0), new FeatureSet("fileName", fileIdentifier));
            doc.addAnnotation(fileNameAnn);
            return doc;
        }
        catch (Exception e) {
            System.err.println("Error reading Mae XML file:" + fileIdentifier);
            e.printStackTrace();
            return null;
        }
    }

    private void readFromNode(Element e, FuseDocument doc) {
        annMap.clear();
        List<Node> relationNodesList = new ArrayList<Node>();
        NodeList annNodeList = e.getChildNodes();
        for (int i = 0; i < annNodeList.getLength(); i++) {
            Node n = annNodeList.item(i);
            if (!(n instanceof Element)) continue;
            String tagName = n.getNodeName();
            if (tagName.equals("JARGON") || tagName.equals("ENAMEX") || tagName.equals("SIGNAL")) {
                FuseAnnotation ann = readAnnotationFromNode(n);
                if (ann != null) {
                    annMap.put((String)ann.get("id"), ann);
//                    System.err.println("Annotation added, text:" + doc.text(ann.span()));
                }
            }
            else {
                relationNodesList.add(n);
            }
        }
        for (Map.Entry<String, FuseAnnotation> entry: annMap.entrySet()) {
            doc.addAnnotation(entry.getValue());
        }
        //doc.addAnnotation(FuseDocument.THIS_ARTICLE);
        for (Node n : relationNodesList) {
            FuseRelation r = readRelationFromNode(n);
            if (r != null) {
                doc.addRelation(r);
            }
        }
//        if (node instanceof Element) {
//            // Add annotation:
//            int start = s.length();
//            if (node.getNodeName().equals(NAME_OF_BIB_NODE)) {
//                NodeList nodes = node.getChildNodes();
//                Node item;
//                for (int i = 0; i < nodes.getLength(); i++) {
//                    item = nodes.item(i);
//                    if (BIB_ELEMENTS_IN_TEXT.contains(item.getNodeName())) {
//                        readFromNode(item, doc, s);
//                    }
//                }
//            }
//            else {
//                if (!IGNORABLE_ELEMENTS_IN_TEXT.contains(node.getNodeName())) {
//                    NodeList nodes = node.getChildNodes();
//                    Node item;
//
//                    for (int i = 0; i < nodes.getLength(); i++) {
//                        item = nodes.item(i);
//                        readFromNode(item, doc, s);
//                    }
//                }
//            }
//            int end = s.length();
//
//
//            Annotation ann = AnnotationFactory.createAnnotation(node.getNodeName().trim().toLowerCase(),
//                    doc,
//                    start,
//                    end);
//
//            // record id for invention-title id="title_eng" and
//            // abstract id="abstr_eng"
//            if (node.getNodeName().trim().toLowerCase().equals("invention-title")) {
//                String id = ((Element) node).getAttribute("lang");
//                if (id != null) {
//                    ann.put("lang", id);
//                }
//                else {
//                    ann.put("lang", "");
//                }
//            }
//            if (node.getNodeName().trim().toLowerCase().equals("abstract")) {
//                String id = ((Element) node).getAttribute("lang");
//                if (id != null) {
//                    ann.put("lang", id);
//                }
//                else {
//                    ann.put("lang", "");
//                }
//            }
//            doc.addAnnotation(ann);
//
//        }
//        else {
//            String textString = node.getTextContent();
//            s.append(textString);
//
//        }

    }

    private FuseRelation readRelationFromNode(Node n) {
        try {
            String id = n.getAttributes().getNamedItem("id").getTextContent();
            String arg1Id = n.getAttributes().getNamedItem("fromID").getTextContent();
            String arg2Id = n.getAttributes().getNamedItem("toID").getTextContent();
            String signalId = "";
            String textSignal = "";
            String type = n.getNodeName();
            String arg3 = "";
            if (n.getAttributes().getNamedItem("signalID") != null) {
                signalId = n.getAttributes().getNamedItem("signalID").getTextContent();
            }else{
                textSignal = n.getAttributes().getNamedItem("TEXT_SIGNAL").getTextContent();
            }
            if (n.getAttributes().getNamedItem("ARG3") != null) {
                arg3 = n.getAttributes().getNamedItem("ARG3").getTextContent().trim();
            }
            String arg1Implied = "";
            if (n.getAttributes().getNamedItem("ARG1_IMPLIED") != null) {
                arg1Implied = n.getAttributes().getNamedItem("ARG1_IMPLIED").getTextContent().trim();
            }
            FuseAnnotation arg1 = arg1Implied.equals("*ThisArticle*") ?
                    FuseDocument.THIS_ARTICLE : annMap.get(arg1Id);
            FuseAnnotation arg2 = annMap.get(arg2Id);
            FuseAnnotation annotatedSignal = null;
            if (!signalId.equals("")){
                annotatedSignal = annMap.get(signalId);
            }
            if ((arg1 == null) || (arg2 == null)) {
                System.err.println("Unable to find arg1/arg2...");
                return null;
            }
            FuseRelation.RelationType rType = FuseRelation.RelationType.valueOf(type);
            FuseRelation fuseRelation = new FuseRelation(id, arg1, arg2, rType);
            fuseRelation.setArg3(arg3);
            fuseRelation.setTextSignal(textSignal);
            fuseRelation.setAnnotatedSignal(annotatedSignal);
            return fuseRelation;

        }
        catch (Exception e) {
            System.err.println("Failed to parse FuseRelation...");
            return null;
        }
    }

    private FuseAnnotation readAnnotationFromNode(Node n) {
        String tagName = n.getNodeName();
        try {
            int start = Integer.valueOf(n.getAttributes().getNamedItem("start").getTextContent());
            int end = Integer.valueOf(n.getAttributes().getNamedItem("end").getTextContent());
            String id = n.getAttributes().getNamedItem("id").getTextContent();
            String typeString = "";
            String citeClass = "";
            String gramSignal = "";
            if (n.getAttributes().getNamedItem("type") != null) {
                typeString = n.getAttributes().getNamedItem("type").getTextContent().trim();
            }
            FuseEntitySpan span = new FuseEntitySpan(start, end);
            FuseAnnotation.AnnotationCategory category;
            FuseAnnotation.AnnotationType type;
            if (tagName.equals("JARGON")) {
                category = FuseAnnotation.AnnotationCategory.JARGON;
                type = FuseAnnotation.AnnotationType.NA;
            } else if (tagName.equals("SIGNAL")){
                category = FuseAnnotation.AnnotationCategory.SIGNAL;
                type = FuseAnnotation.AnnotationType.NA;
                if (start == -1){
                    gramSignal = n.getAttributes().getNamedItem("text").getTextContent().trim();
                }
            }
            else {
                category = FuseAnnotation.AnnotationCategory.ENAMEX;
                type = FuseAnnotation.AnnotationType.valueOf(typeString);
                if (n.getAttributes().getNamedItem("CITE_CLASS") != null){
                    citeClass = n.getAttributes().getNamedItem("CITE_CLASS").getTextContent().trim();
                }
            }
            FuseAnnotation fuseAnnotation = new FuseAnnotation("fuse-entity", span, new FeatureSet("id", id),
                    category, type);
            if (!citeClass.equals("")){
                fuseAnnotation.put("CITE_CLASS", citeClass);
            }
            if (!gramSignal.equals("")){
                fuseAnnotation.put("GRAM_SIGNAL",gramSignal);
            }
            return fuseAnnotation;
        }
        catch (Exception e) {
            System.err.println("Unable to parse Annotation...");
            return null;
        }
    }
}