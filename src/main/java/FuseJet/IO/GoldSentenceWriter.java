package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 10/29/12
 * Time: 6:22 PM
 */
public class GoldSentenceWriter implements FuseDocumentWriter {
    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        Set<FuseRelation> fuseRelations = doc.getRelations();
        List<FuseRelation> opinionRelations = new ArrayList<FuseRelation>();
        Set<FuseAnnotation> usedAnns = new HashSet<FuseAnnotation>();
        Set<FuseAnnotation> unusedAnns = new HashSet<FuseAnnotation>();
        List<FuseRelation> otherRelations = new ArrayList<FuseRelation>();
        for (FuseRelation relation : fuseRelations) {
            boolean relationChosen = false;
            if (relation.getType() == FuseRelation.RelationType.OPINION) {
                if (relation.getArg2().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                    opinionRelations.add(relation);
                    usedAnns.add(relation.getArg2());
                    relationChosen = true;
                }
            }
            if (relation.getType() == FuseRelation.RelationType.ORIGINATE) {
                if (relation.getArg1().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                    opinionRelations.add(relation);
                    usedAnns.add(relation.getArg1());
                    relationChosen = true;
                }
            }
            if (relation.getType() == FuseRelation.RelationType.RELATED_WORK) {
                if (relation.getArg3().trim().toUpperCase().equals("BASED_ON")) {
                    if (relation.getArg2().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                        opinionRelations.add(relation);
                        usedAnns.add(relation.getArg2());
                        relationChosen = true;
                    }
                }
                if (relation.getArg3().trim().toUpperCase().equals("BETTER_THAN")) {
                    if (relation.getArg1().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                        opinionRelations.add(relation);
                        usedAnns.add(relation.getArg1());
                        relationChosen = true;
                    }
                    if (relation.getArg2().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                        opinionRelations.add(relation);
                        usedAnns.add(relation.getArg2());
                        relationChosen = true;
                    }

                }
                if (relation.getArg3().trim().toUpperCase().equals("CORROBORATION")) {
                    if (relation.getArg1().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                        opinionRelations.add(relation);
                        usedAnns.add(relation.getArg1());
                        relationChosen = true;
                    }
                    if (relation.getArg2().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                        opinionRelations.add(relation);
                        usedAnns.add(relation.getArg2());
                        relationChosen = true;
                    }

                }

            }
            if (!relationChosen) {
                otherRelations.add(relation);
            }
        }
        Set<FuseAnnotation> allAnns = doc.getFuseEntities();
        for (FuseAnnotation ann : allAnns) {
            if (ann.getEntityType() != FuseAnnotation.AnnotationType.CITATION) {
                continue;
            }
            if (!usedAnns.contains(ann)) {
                unusedAnns.add(ann);
            }
        }
        if (sentences != null) {
            for (Annotation sentence : sentences) {
                boolean printed = false;
                for (FuseRelation fuseRelation : opinionRelations) {
                    FuseAnnotation arg1 = fuseRelation.getArg1();
                    FuseAnnotation arg2 = fuseRelation.getArg2();

                    if (fuseRelation.getType() == FuseRelation.RelationType.OPINION) {
                        if (arg2.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                            if (fuseRelation.getArg3().trim().toUpperCase().equals("NEGATIVE")) {
                                printExample("NEGATIVE", doc, w, sentence, fuseRelation, arg2);
                            }
                            else {
                                printExample("POSITIVE", doc, w, sentence, fuseRelation, arg2);
                            }
                        }
                    }
                    if (fuseRelation.getType() == FuseRelation.RelationType.ORIGINATE) {
                        if (fuseRelation.getArg1().getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                            printExample("POSITIVE", doc, w, sentence, fuseRelation, arg2);
                        }
                    }
                    if (fuseRelation.getType() == FuseRelation.RelationType.RELATED_WORK) {
                        if (fuseRelation.getArg3().trim().toUpperCase().equals("BASED_ON")) {
                            if (arg2.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                                printExample("POSITIVE", doc, w, sentence, fuseRelation, arg2);
                            }
                        }
                        if (fuseRelation.getArg3().trim().toUpperCase().equals("BETTER_THAN")) {
                            if (arg1.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                                printExample("POSITIVE", doc, w, sentence, fuseRelation, arg1);
                            }
                            if (arg2.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                                printExample("NEGATIVE", doc, w, sentence, fuseRelation, arg2);
                            }
                        }
                        if (fuseRelation.getArg3().trim().toUpperCase().equals("CORROBORATION")) {
                            if (arg1.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                                printExample("POSITIVE", doc, w, sentence, fuseRelation, arg1);
                            }
                            if (arg2.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                                printExample("POSITIVE", doc, w, sentence, fuseRelation, arg2);
                            }
                        }
                    }

                }
                for (FuseAnnotation unusedAnn : unusedAnns) {
                    if ((unusedAnn.start() >= sentence.start())
                            && (unusedAnn.end() <= sentence.end())) {
                        String subtype = "NEUTRAL";
                        String textSignal = "";
                        for (FuseRelation otherRelation : otherRelations) {
                            if (otherRelation.getArg1().equals(unusedAnn) ||
                                    otherRelation.getArg2().equals(unusedAnn)) {
                                subtype = otherRelation.getType().toString() + ":"
                                        + otherRelation.getArg3();
                                textSignal = otherRelation.getTextSignal();
                            }
                        }
                        w.println(String.format("offset:%d|||ann:%d-%d|||type:%s|||subtype:%s|||text:%s|||signal:%s",
                                sentence.start(),
                                unusedAnn.start() - sentence.start(),
                                unusedAnn.end() - sentence.start(),
                                "NEUTRAL",
                                subtype,
                                doc.text(sentence.span()),
                                textSignal).trim().replaceAll("\\r|\\n", " ").replaceAll(" +", " "));
                    }
                }
            }

        }
        w.close();
    }

    private void printExample(String type, FuseDocument doc, PrintWriter w, Annotation sentence, FuseRelation fuseRelation, FuseAnnotation arg2) {
        if ((arg2.start() >= sentence.start())
                && (arg2.end() <= sentence.end())) {
            w.println(String.format("offset:%d|||ann:%d-%d|||type:%s|||subtype:%s|||text:%s|||signal:%s",
                    sentence.start(),
                    arg2.start()-sentence.start(),
                    arg2.end()-sentence.start(),
                    type,
                    fuseRelation.getType().toString() + ":" +
                    fuseRelation.getArg3(),
                    doc.text(sentence.span()),
                    fuseRelation.getTextSignal()).replaceAll("\n", " ")
                    );
        }
    }
}
