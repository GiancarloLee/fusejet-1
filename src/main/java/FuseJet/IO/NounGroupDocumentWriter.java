package FuseJet.IO;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import FuseJet.Utils.AnnotationStartComparator;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 8/16/12
 * Time: 4:59 PM
 */
public class NounGroupDocumentWriter implements FuseDocumentWriter {

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        Vector<Annotation> tokens = doc.annotationsOfType("token");

        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if ((tokens == null) || (sentences == null)) {
            w.close();
            return;
        }
        Collections.sort(tokens, new AnnotationStartComparator());
        Collections.sort(sentences, new AnnotationStartComparator());

        //TODO: change to stream process
        for (Annotation sentence : sentences) {
            Vector<Annotation> tokensBySentence = doc.annotationsOfType("token", sentence.span());
            printSentence(tokensBySentence, doc, w);
        }
        w.close();
    }

    private void printSentence(List<Annotation> tokens, FuseDocument doc, PrintWriter w) {
        List<String[]> buf = new ArrayList<String[]>();
        tokens.add(AnnotationFactory.createAnnotation("token", null, -1, -1));
        for (Annotation token : tokens) {
            String pos = "NN";
            Vector<Annotation> posAnns = doc.annotationsAt(token.start(), "tagger");
            if (posAnns == null) pos = "UN";

            else pos = (String) posAnns.get(0).get("cat");
            if ((pos.equals("JJ")) || (pos.startsWith("NN"))) {
                buf.add(new String[]{doc.text(token.span()), (String) token.get("tokenString"), pos});
            } else {
                if (buf.size() > 0) {
                    boolean chunkBegin = true;
                    String chunk = "B-NP";
                    for (String[] info : buf) {
                        if (buf.get(buf.size()-1)[2].startsWith("NN")) {
                            if (chunkBegin) {
                                chunkBegin = false;
                            } else {
                                chunk = "I-NP";
                            }
                        }
                        else {
                            chunk = "O";
                        }
                        w.println(String.format("%s\t%s\t%s\t%s", info[0].replace('\n', ' '), info[1].replace('\n', ' '), info[2], chunk));
                    }
                    buf.clear();
                    if (token.start() > -1)
                        w.println(String.format("%s\t%s\t%s\t%s",
                                doc.text(token.span()).replace('\n', ' '),
                                ((String) token.get("tokenString")).replace('\n', ' '), pos, "O"));
                }
                else {
                    if (token.start() > -1)
                        w.println(String.format("%s\t%s\t%s\t%s",
                                doc.text(token.span()).replace('\n', ' '),
                                ((String) token.get("tokenString")).replace('\n', ' '), pos, "O"));
                }
            }
        }

        w.println();
    }
}
