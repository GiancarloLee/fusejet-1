package FuseJet.IO;

import FuseJet.IO.FuseDocumentWriter;
import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: yhe
 * Date: 7/2/13
 * Time: 5:08 PM
 */
public class JetFactWriter extends InitializableFuseDocumentWriter {
    private Map<String, String> tagsToOutput = new HashMap<String, String>();

    @Override
    public void initialize(String[] resourceInformation) {
        tagsToOutput.clear();
        if (resourceInformation != null) {
            for (String entry : resourceInformation) {
                String[] parts = entry.split(":");
                tagsToOutput.put(parts[0], parts[1]);
            }
        }
    }

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        try {
            PrintWriter w = new PrintWriter(new FileWriter(outputFileName));
            for (String type : tagsToOutput.keySet()) {
                int count = 0;
                List<Annotation> anns = doc.annotationsOfType(type);
                for (Annotation ann : anns) {
                    w.println(String.format("%s ID=\"NYU_%s_%d\" START=%d END=%d",
                            tagsToOutput.get(type),
                            tagsToOutput.get(type),
                            count++,
                            ann.start(),
                            ann.end()));
                }
            }
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
