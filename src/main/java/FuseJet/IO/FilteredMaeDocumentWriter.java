package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import FuseJet.Utils.FuseUtils;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 11/7/12
 * Time: 4:48 PM
 */
public class FilteredMaeDocumentWriter extends InitializableFuseDocumentWriter {
    private Set<String> stopSet = new HashSet<String>();
    private Set<String> strongStopSet = new HashSet<String>();
    private Map<FuseAnnotation, String> annIdMap = new HashMap<FuseAnnotation, String>();

    @Override
    public void initialize(String[] resourceInformation) {
        String fileName = resourceInformation[0];
        try {
            stopSet = FuseUtils.readWordListWithThreshold(fileName, 20);
            strongStopSet = FuseUtils.readWordListWithThreshold(fileName, 10000);
        }
        catch (IOException e) {
            System.err.println("Error loading stop word list...");
        }
    }

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        String start = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<JargonTask>\n" +
                "<TEXT><![CDATA[";
        String middle = "]]></TEXT>\n" +
                "<TAGS>";
        String end = "</TAGS>\n" +
                "</JargonTask>";
        StringBuilder sb = new StringBuilder(doc.text().length()*2);
        sb.append(start).append(doc.text()).append(middle);
        Set<FuseAnnotation> annotations = doc.getFuseEntities();
        annotations.add(FuseDocument.THIS_ARTICLE);
        List<FuseAnnotation> annotationList = new ArrayList<FuseAnnotation>(annotations.size());
        annotationList.addAll(annotations);
        Collections.sort(annotationList, new FuseAnnotationByTypeComparator());
        Set<FuseRelation> relations = doc.getRelations();
        List<FuseRelation> relationList = new ArrayList<FuseRelation>(relations.size());
        for (FuseRelation relation : relations) {
            if (filter(doc, relation)) {
                relationList.add(relation);
            }
        }
        //relationList.addAll(relations);
        Collections.sort(relationList, new FuseRelationComparator());
        annIdMap.clear();
        int cnt = 0;
        for (FuseAnnotation ann : annotationList) {
            String id = ann.getCategory() == FuseAnnotation.AnnotationCategory.JARGON ?
                    "J" + cnt : "E"+cnt;
            annIdMap.put(ann, id);
            if ((ann.start() > -1) && (ann.end() > -1)) {
                sb.append("\n").append(annotationToString(ann, doc.text(ann.span()), id));
            }
            else {
                sb.append("\n").append(String.format(
                        "<ENAMEX id=\"%s\" start=\"-1\" end=\"-1\" text=\"*ThisArticle*\" type=\"CITATION\" POS=\"\" comment=\"*ThisArticle*\"/>",
                        id));
            }
            cnt++;
        }
        cnt = 0;
        for (FuseRelation relation : relationList) {
            String id = "R"+cnt;
            sb.append("\n").append(relationToString(relation, doc, id));
            cnt++;
        }
        sb.append("\n").append(end);
        w.println(sb.toString());
        w.close();
    }

    private String annotationToString(FuseAnnotation ann, String text, String id) {
        String entityId = ann.get("id") == null ? "" : (String)ann.get("id");
        String subtype = ann.get("subtype") == null ? "": (String)ann.get("subtype");
        return String.format("<%s id=\"%s\" " +
                "start=\"%d\" " +
                "end=\"%d\" " +
                "text=\"%s\" " +
                "type=\"%s\" " +
                "subtype=\"%s\" " +
                "ENTITY_ID=\"%s\" " +
                "comment=\"\" />",
                ann.getCategory().toString(),
                id,
                ann.start(),
                ann.end(),
                text,
                ann.getEntityType().toString(),
                subtype,
                entityId);
    }

    private String relationToString(FuseRelation relation, FuseDocument doc, String id) {
        String fromId = "";
        if (annIdMap.containsKey(relation.getArg1())) {
            fromId = annIdMap.get(relation.getArg1());
        }
        String fromText = textFromDocSpan(doc, relation.getArg1().span());
        String toId = "";
        if (annIdMap.containsKey(relation.getArg2())) {
            toId = annIdMap.get(relation.getArg2());
        }
        String toText = textFromDocSpan(doc, relation.getArg2().span());
        String arg3 = relation.getArg3();
        String arg1Implied = relation.getArg1() == FuseDocument.THIS_ARTICLE ? "*ThisArticle*" : "";
        return String.format("<%s id=\"%s\" " +
                "fromID=\"%s\" " +
                "fromText=\"%s\" " +
                "toID=\"%s\" " +
                "toText=\"%s\" " +
                "ARG3=\"%s\" " +
                "ARG1_IMPLIED=\"%s\" " +
                "TEXT_SIGNAL=\"%s\" " +
                "GRAM_SIGNAL=\"%s\" " +
                "comment=\"AUTOMATICALLY GENERATED\" />",
                relation.getType().toString(),
                id,
                fromId,
                fromText,
                toId,
                toText,
                arg3,
                arg1Implied,
                relation.getTextSignal(),
                relation.getGramSignal());
    }

    private String textFromDocSpan(FuseDocument doc, Span span) {
        if ((span == null) || span.start()-span.end() > 0 || span.start() < 0 || span.end() < 0)  {
            return "";
        }
        else {
            return doc.text(span);
        }
    }

    public boolean filter(FuseDocument doc, FuseRelation relation) {
        Vector<Annotation> tokens = null;
        Vector<Annotation> strongTokens = null;
        if (relation.getArg3().toUpperCase().equals("MANUFACTURE")) {
            char[] chars = doc.text(relation.getArg1().span()).toCharArray();
            boolean hasComma = false;
            for (char c : chars) {
                if (c == ',') {
                    hasComma = true;
                }
            }
            if (!hasComma) return false;
        }
        if (relation.getArg3().toUpperCase().equals("PRACTICAL")) {
             tokens = doc.annotationsOfType("token", relation.getArg2().span());
        }
        else {
            if (relation.getType() == FuseRelation.RelationType.EXEMPLIFY) {
                tokens = doc.annotationsOfType("token", relation.getArg1().span());
                strongTokens = doc.annotationsOfType("token", relation.getArg2().span());
            }
        }
        if (tokens == null) {
            return true;
        }
        else {
            for (Annotation token : tokens) {
                if (stopSet.contains(doc.text(token.span()).trim())) {
                    return false;
                }
            }
            if (strongTokens != null) {
                for (Annotation token : strongTokens) {
                    if (strongStopSet.contains(doc.text(token.span()).trim())) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public class FuseAnnotationByTypeComparator implements Comparator<FuseAnnotation> {
        @Override
        public int compare(FuseAnnotation annotation1, FuseAnnotation annotation2) {
            int ordDiff = annotation1.getEntityType().ordinal() - annotation2.getEntityType().ordinal();
            if (ordDiff != 0) return ordDiff;
            return annotation1.start() - annotation2.start();
        }
    }

    public class FuseRelationComparator implements Comparator<FuseRelation> {
        @Override
        public int compare(FuseRelation annotation1, FuseRelation annotation2) {
            return annotation1.getArg1().start() - annotation2.getArg1().start();
        }
    }

}
