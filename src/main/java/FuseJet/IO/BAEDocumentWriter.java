package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * User: yhe
 * Date: 6/28/12
 * Time: 11:48 AM
 */
public class BAEDocumentWriter implements FuseDocumentWriter {
    public static final boolean SHOULD_PRINT_SIGNAL = false;
    protected static final ImmutableMap<String, String[]> ARG_NAME_MAP = new ImmutableMap.Builder<String, String[]>()
            .put("EXEMPLIFY.", new String[]{"SUBCLASS","SUPERCLASS"})
            .put("ORIGINATE.", new String[]{"INVENTOR","INVENTION"})
            .put("ORIGINATE.MANUFACTURE", new String[]{"MAKER","PRODUCT"})
            .put("ORIGINATE.SUPPLY", new String[]{"SUPPLIER","PRODUCT"})
            .put("ABBREVIATE.", new String[]{"FULLNAME","SHORTNAME"})
            .put("ABBREVIATE.ALIAS", new String[]{"FULLNAME","FULLNAME"})
            .put("RELATED_WORK.", new String[]{"THEME","THEME"})
            .put("RELATED_WORK.BASED_ON", new String[]{"DERIVED","ORIGINAL"})
            .put("RELATED_WORK.BETTER_THAN", new String[]{"BETTER","WORSE"})
            .put("OPINION.", new String[]{"JUDGE","THEME"})
            .build();

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        Map<FuseAnnotation, String> entities = new HashMap<FuseAnnotation, String>();
        //Map<FuseRelation, Integer> relations = new HashMap<FuseRelation, Integer>();
        // For now, we don't control spans, as spans are already
        // we don't care about validAnnTypes also
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        entities.put(FuseDocument.THIS_ARTICLE, "PAPER");
        int eid = 1;
        for (FuseAnnotation entity : doc.getFuseEntities()) {
            if (!SHOULD_PRINT_SIGNAL) {
                if (entity.getCategory() == FuseAnnotation.AnnotationCategory.SIGNAL) continue;
            }
            String text = "";
            if (entity != FuseDocument.THIS_ARTICLE) {
                text = doc.text(entity.span()).trim().replace("\n", " ");
            }
            String baeId = null;
            if (entity.get("bae_id") != null) {
                 baeId = (String)entity.get("bae_id");
            }
            String id = new StringBuilder("NYU_").append(entity.getCategory()).append(eid).toString();
            if ((entity.get("id") != null) && (!((String)entity.get("id")).trim().equals("")))
                id = (String)entity.get("id");
            if (baeId == null) {
                w.println(String.format("%s ID=\"%s\" TYPE=\"%s\" START=%d END=%d TEXT=\"%s\"",
                        entity.getCategory(),
                        id,
                        entity.getEntityType(),
                        entity.start(),
                        entity.end(),
                        text
                ));
            }
            else {
                w.println(String.format("%s ID=\"%s\" BAE_ID=\"%s\" TYPE=\"%s\" START=%d END=%d TEXT=\"%s\"",
                        entity.getCategory(),
                        id,
                        baeId,
                        entity.getEntityType(),
                        entity.start(),
                        entity.end(),
                        text
                ));
            }
            entities.put(entity, id);
            eid++;
        }
        int rid = 1;
        for (FuseRelation relation : doc.getRelations()) {
            String[] argNames;
            if (ARG_NAME_MAP.containsKey(relation.getType().toString()+"."+relation.getArg3().toUpperCase())) {
                argNames = ARG_NAME_MAP.get(relation.getType().toString()+"."+relation.getArg3().toUpperCase());
            }
            else {
                argNames = ARG_NAME_MAP.get(relation.getType().toString()+".");
            }
            w.println(String.format("RELATION ID=\"%s\" TYPE=\"%s\" ARG1=\"%s\" ARG1_NAME=\"%s\" ARG2=\"%s\" ARG2_NAME=\"%s\" SUBTYPE=\"%s\" TEXT_SIGNAL=\"%s\"",
                    String.format("NYU_RELATION_%s_%d",
                            relation.getArg3().trim().equals("") ? relation.getType().toString() : relation.getType().toString()+"_"+relation.getArg3(),
                            rid),
                    relation.getType().toString(),
                    entities.get(relation.getArg1()),
                    argNames[0],
                    entities.get(relation.getArg2()),
                    argNames[1],
                    relation.getArg3().toUpperCase(),
                    relation.getTextSignal()
                    ));
            rid++;
        }

        w.close();
    }
}
