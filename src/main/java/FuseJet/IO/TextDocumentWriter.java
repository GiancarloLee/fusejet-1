package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.PrintWriter;

/**
 * User: yhe
 * Date: 8/9/12
 * Time: 2:42 PM
 */
public class TextDocumentWriter implements FuseDocumentWriter  {

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
        w.println(doc.text().trim());
        w.close();
    }
}
