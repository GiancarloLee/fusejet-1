package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.sun.xml.internal.rngom.ast.builder.Annotations;

import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 7/9/13
 * Time: 10:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class RelationContextWriter implements FuseDocumentWriter {
    private Set<String> stopWords;
    public RelationContextWriter() {
        //TODO: change this to use config files
        try {
            stopWords = new HashSet<String>();
            BufferedReader r = new BufferedReader(new FileReader("en.freq"));
            String l = null;
            while ((l = r.readLine()) != null) {
                String[] parts = l.split("\t");
                if (Integer.valueOf(parts[1]) < 10000) {
                    break;
                }
                stopWords.add(parts[0].trim().toLowerCase());
            }
            r.close();
            stopWords.add("reference");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        List<Annotation> sentences = doc.annotationsOfType("sentence");
        Map<FuseAnnotation, String> entities = new HashMap<FuseAnnotation, String>();
        //Map<FuseRelation, Integer> relations = new HashMap<FuseRelation, Integer>();
        // For now, we don't control spans, as spans are already
        // we don't care about validAnnTypes also
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        entities.put(FuseDocument.THIS_ARTICLE, "PAPER");
        for (FuseRelation relation : doc.getRelations()) {
            int s = relation.getArg1().start();
            int e = relation.getArg2().end();
            if (relation.getArg1().start() == -1) {
                s = relation.getArg2().start();
                e = relation.getArg2().end();
            }
            else {
                if (s > e) {
                    s = relation.getArg2().start();
                    e = relation.getArg1().end();
                }
            }
            String textSignal = "";
            if (relation.getAnnotatedSignal() != null) {
                FuseAnnotation signal = relation.getAnnotatedSignal();
                if (signal.start() > 0 && signal.end() > 0) {
                    textSignal = doc.text(signal).trim();
                }
            }
            Annotation sentence = null;
            Annotation ann = relation.getArg2();
            List<Annotation> tokens = doc.annotationsOfType("token", ann.span());
            if (tokens == null) continue;
            if (doc.text(tokens.get(0)).trim().equals("the") ||
                    doc.text(tokens.get(0)).trim().equals("a")  ||
                    doc.text(tokens.get(0)).trim().equals("an")) {
                ann = new Annotation(ann.type(), new Span(tokens.get(0).end(), ann.end()), new FeatureSet());
            }
            tokens = doc.annotationsOfType("token", ann.span());
            if (tokens == null) continue;
            boolean stop = false;
            for (Annotation token : tokens) {
                if (stopWords.contains(doc.text(token).trim().toLowerCase())) {
                    stop = true;
                    break;
                }
            }
            //if (stop) continue;
            for (Annotation sent : sentences) {
                if (sent.start() <= ann.start() && sent.end() >= ann.end()) {
                    sentence = sent;
                    break;
                }

            }
            if (sentence == null) continue;

            w.println(String.format("FILE=%s ||| TERM=%s ||| OFFSET=%d ||| START=%d ||| END=%d ||| SENTENCE=%s",
                    outputFileName,
                    doc.text(ann).replaceAll("\\s+", " "),
                    sentence.start(),
                    ann.start() - sentence.start(),
                    ann.end() - sentence.start(),
                    doc.text(sentence).replaceAll("\\s", " ")));
        }

        w.close();
    }
}
