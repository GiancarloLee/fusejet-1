package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author yhe & gcl
 */
public class MaeDocumentWriter implements FuseDocumentWriter {

    private Map<FuseAnnotation, String> annIdMap = new HashMap<FuseAnnotation, String>();

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        String start = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<JargonTask>\n" +
                "<TEXT><![CDATA[";
        String middle = "]]></TEXT>\n" +
                "<TAGS>";
        String end = "</TAGS>\n" +
                "</JargonTask>";
        StringBuilder sb = new StringBuilder(doc.text().length()*2);
        sb.append(start).append(doc.text()).append(middle);
        Set<FuseAnnotation> annotations = doc.getFuseEntities();
        annotations.add(FuseDocument.THIS_ARTICLE);
        List<FuseAnnotation> annotationList = new ArrayList<FuseAnnotation>(annotations.size());
        annotationList.addAll(annotations);
        Collections.sort(annotationList, new FuseAnnotationByTypeComparator());
        Set<FuseRelation> relations = doc.getRelations();
        List<FuseRelation> relationList = new ArrayList<FuseRelation>(relations.size());
        relationList.addAll(relations);
        Collections.sort(relationList, new FuseRelationComparator());
        annIdMap.clear();
        int cnt = 0;
        for (FuseAnnotation ann : annotationList) {
            String id = ann.getCategory() == FuseAnnotation.AnnotationCategory.JARGON ?
                    "J" + cnt : ann.getCategory() == FuseAnnotation.AnnotationCategory.ENAMEX ?
                    "E"+cnt : "S"+cnt;
            annIdMap.put(ann, id);
            if ((ann.start() > -1) && (ann.end() > -1)) {
                sb.append("\n").append(annotationToString(ann, doc.text(ann.span()), id));
            }
            else {
                sb.append("\n").append(String.format(
                        "<ENAMEX id=\"%s\" start=\"-1\" end=\"-1\" text=\"*ThisArticle*\" type=\"CITATION\" POS=\"\" comment=\"*ThisArticle*\"/>",
                        id));
            }
            cnt++;
        }
        cnt = 0;
        for (FuseRelation relation : relationList) {
            String id = "R"+cnt;
            sb.append("\n").append(relationToString(relation, doc, id));
            cnt++;
        }
        sb.append("\n").append(end);
        w.println(sb.toString());
        w.close();
    }

    private String annotationToString(FuseAnnotation ann, String text, String id) {
        text = text.replaceAll("&","&amp;");
        String entityId = ann.get("id") == null ? "" : (String)ann.get("id");
        String subtype = ann.get("subtype") == null ? "": (String)ann.get("subtype");
        String citeClass = ann.get("cite_class") == null ? "": (String)ann.get("cite_class");
        String auto = ann.get("auto") == null ? "" : (String)ann.get("auto");
        return String.format("<%s id=\"%s\" " +
                "auto=\"%s\" " +
                "start=\"%d\" " +
                "end=\"%d\" " +
                "text=\"%s\" " +
                "type=\"%s\" " +
                "subtype=\"%s\" " +
                "ENTITY_ID=\"%s\" " +
                "CITE_CLASS=\"%s\" " +
                "comment=\"\" />",
                ann.getCategory().toString(),
                id,
                auto,
                ann.start(),
                ann.end(),
                text,
                ann.getEntityType().toString(),
                subtype,
                entityId,
                citeClass);
    }

    private String relationToString(FuseRelation relation, FuseDocument doc, String id) {
        String fromId = "";
        if (annIdMap.containsKey(relation.getArg1())) {
            fromId = annIdMap.get(relation.getArg1());
        }
        String fromText = textFromDocSpan(doc, relation.getArg1().span()).replaceAll("&","&amp;");
        String toId = "";
        if (annIdMap.containsKey(relation.getArg2())) {
            toId = annIdMap.get(relation.getArg2());
        }
        String toText = textFromDocSpan(doc, relation.getArg2().span()).replaceAll("&","&amp;");
        String signalID = "";
        String annotatedSignalText = "";
        if (annIdMap.containsKey(relation.getAnnotatedSignal())){
            signalID = annIdMap.get(relation.getAnnotatedSignal());
            annotatedSignalText = textFromDocSpan(doc, relation.getAnnotatedSignal().span());
        }else if (relation.getTextSignal().equals("")){
            annotatedSignalText = relation.getGramSignal();
        }else{
            annotatedSignalText = relation.getTextSignal();
        }
        String arg3 = relation.getArg3();
        String arg1Implied = relation.getArg1() == FuseDocument.THIS_ARTICLE ? "*ThisArticle*" : "";
        return String.format("<%s id=\"%s\" " +
                "auto=\"%s\" " +
                "fromID=\"%s\" " +
                "fromText=\"%s\" " +
                "toID=\"%s\" " +
                "toText=\"%s\" " +
                "signalID=\"%s\" " +
                "signal=\"%s\" " +
                "ARG3=\"%s\" " +
                "ARG1_IMPLIED=\"%s\" " +
                "comment=\"AUTOMATICALLY GENERATED\" />",
                relation.getType().toString(),
                id,
                relation.getAuto(),
                fromId,
                fromText,
                toId,
                toText,
                signalID,
                annotatedSignalText,
                arg3,
                arg1Implied);
    }

    private String textFromDocSpan(FuseDocument doc, Span span) {
        if ((span == null) || span.start()-span.end() > 0 || span.start() < 0 || span.end() < 0)  {
            return "";
        }
        else {
            return doc.text(span);
        }
    }

    public class FuseAnnotationByTypeComparator implements Comparator<FuseAnnotation> {
        @Override
        public int compare(FuseAnnotation annotation1, FuseAnnotation annotation2) {
            int ordDiff = annotation1.getEntityType().ordinal() - annotation2.getEntityType().ordinal();
            if (ordDiff != 0) return ordDiff;
            return annotation1.start() - annotation2.start();
        }
    }

    public class FuseRelationComparator implements Comparator<FuseRelation> {
        @Override
        public int compare(FuseRelation annotation1, FuseRelation annotation2) {
            return annotation1.getArg1().start() - annotation2.getArg1().start();
        }
    }
}