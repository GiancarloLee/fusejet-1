package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 * User: yhe
 * Date: 4/12/13
 * Time: 12:48 PM
 */
public class BAECitationTxtFactWriter extends BAETxtFactWriter {
    private int cnt = 0;
    @Override
    public void write(FuseDocument fuseDocument, String s) {
        cnt = 0;
        FuseDocument doc = fuseDocument;
        String[] parts = s.split(";");
        String textFile = parts[0];
        String factFile = parts[1];
        PrintWriter textWriter, factWriter;
        try {
            textWriter = new PrintWriter(new File(textFile));
            factWriter = new PrintWriter(new File(factFile));
        } catch (FileNotFoundException e) {
            return;
        }
        String text = fuseDocument.text();
        textWriter.write(text);
        textWriter.close();

        int i = 1;
        Set<FuseAnnotation> citations = fuseDocument.getFuseEntities();
        if (citations == null) {
            factWriter.close();
            return;
        }
        for (FuseAnnotation citation : citations) {
            if (!citation.getEntityType().equals(FuseAnnotation.AnnotationType.CITATION)) {
                continue;
            }
            String citeClass = citation.get("CITE_CLASS") != null ? (String)citation.get("CITE_CLASS") : "";
            factWriter.println(String.format("CITATION ID=\"NYU_ARTICLE_CITE_%s\" " +
                    "START=%d " +
                    "END=%d " +
                    "CITE_CLASS=\"%s\"",
                    citation.get("id") != null ? (String)citation.get("id") : String.valueOf(i),
                    citation.start(),
                    citation.end(),
                    citeClass));
            i++;
        }
        factWriter.close();
    }
}
