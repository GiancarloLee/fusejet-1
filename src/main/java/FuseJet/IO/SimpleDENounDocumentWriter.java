package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableSet;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 8/8/12
 * Time: 2:33 PM
 */
public class SimpleDENounDocumentWriter extends InitializableFuseDocumentWriter {

    private String tagToParse = "";

    @Override
    public void initialize(String[] resourceInformation) {
        if ((resourceInformation == null) || (resourceInformation.length == 0)) {
            tagToParse = "fullspan";
        }
        else {
            tagToParse = resourceInformation[0];
        }
    }

    private static final ImmutableSet<String> ABBR_WORDS = new ImmutableSet.Builder<String>()
            .add("a.a.O.")
            .add("Abb.")
            .add("Abf.")
            .add("Abk.")
            .add("Abs.")
            .add("Abt.")
            .add("abzgl.")
            .add("a.D.")
            .add("a.M.")
            .add("amtl.")
            .add("Anh.")
            .add("Ank.")
            .add("Anl.")
            .add("Anm.")
            .add("a.Rh.")
            .add("A.T.")
            .add("Bd.")
            .add("beil.")
            .add("bes.")
            .add("Best.-Nr.")
            .add("Betr.")
            .add("Bez.")
            .add("Bhf.")
            .add("b.w.")
            .add("bzgl.")
            .add("bzw.")
            .add("ca.")
            .add("Chr.")
            .add("d.Ä.")
            .add("dgl.")
            .add("d.h.")
            .add("Dipl.-Ing.")
            .add("Dipl.-Kfm.")
            .add("Dir.")
            .add("d.J.")
            .add("Dr.med.")
            .add("Dr.phil.")
            .add("dt.")
            .add("Dtzd.")
            .add("e.h.")
            .add("ehem.")
            .add("eigtl.")
            .add("einshl.")
            .add("entspr.")
            .add("erb.")
            .add("erw.")
            .add("Erw.")
            .add("ev.")
            .add("e.V.")
            .add("evtl.")
            .add("e.Wz.")
            .add("exkl.")
            .add("Fa.")
            .add("Fam.")
            .add("F.f.")
            .add("Ffm.")
            .add("Forts.f.")
            .add("Fr.")
            .add("Frl.")
            .add("frz.")
            .add("geb.")
            .add("Gebr.")
            .add("gedr.")
            .add("gegr.")
            .add("gek.")
            .add("Ges.")
            .add("gesch.")
            .add("gest.")
            .add("gez.")
            .add("ggf.")
            .add("ggfs.")
            .add("Hbf.")
            .add("hpts.")
            .add("Hptst.")
            .add("Hr.")
            .add("Hrn.")
            .add("Hrsg.")
            .add("i.A.")
            .add("i.b.")
            .add("i.B.")
            .add("i.H.")
            .add("i.J.")
            .add("Ing.")
            .add("Inh.")
            .add("inkl.")
            .add("i.R.")
            .add("i.V.")
            .add("jew.")
            .add("Jh.")
            .add("jhrl.")
            .add("Kap.")
            .add("kath.")
            .add("Kfm.")
            .add("kfm.")
            .add("kgl.")
            .add("Kl.")
            .add("k.o.")
            .add("K.o.")
            .add("k.u.k.")
            .add("led.")
            .add("m.E.")
            .add("Mio.")
            .add("möbl.")
            .add("Mrd.")
            .add("Msp.")
            .add("mtl.")
            .add("m.W.")
            .add("MwSt.")
            .add("MWST.")
            .add("näml.")
            .add("n.Chr.")
            .add("Nr.")
            .add("n.u.Z.")
            .add("o.A.")
            .add("o.B.")
            .add("obb.")
            .add("od.")
            .add("o.g.")
            .add("österr.")
            .add("p.Adr.")
            .add("Pfd.")
            .add("Pl.")
            .add("Reg.-Bez.")
            .add("r.k.")
            .add("r.-k.")
            .add("röm.")
            .add("röm.-kath.")
            .add("s.a.")
            .add("Sa.")
            .add("schles.")
            .add("schwäb.")
            .add("schweiz.")
            .add("s.o.")
            .add("So.")
            .add("sog.")
            .add("St.")
            .add("Str.")
            .add("StR.")
            .add("s.u.")
            .add("südd.")
            .add("tägl.")
            .add("u.a.")
            .add("u.ä.")
            .add("u.Ä.")
            .add("u.a.m.")
            .add("u.A.w.g.")
            .add("usw.")
            .add("u.v.a.")
            .add("u.U.")
            .add("v.Chr.")
            .add("Verf.")
            .add("verh.")
            .add("verw.")
            .add("vgl.")
            .add("v.H.")
            .add("vorm.")
            .add("v.R.w.")
            .add("v.T.")
            .add("v.u.Z.")
            .add("z.B.")
            .add("z.Hd.")
            .add("Zi.")
            .add("zur.")
            .add("zus.")
            .add("z.T.")
            .add("Ztr.")
            .add("zzgl.")
            .add("z.Z.")
            .build();

    private static final ImmutableSet<Character> LEFT_PUNCT_CHARS = new ImmutableSet.Builder<Character>()
            .add('„')
            .add('\'')
            .add('"')
            .add('«')
            .add('(')
            .add('[')
            .add('{')
            .add('`')
            .add(':')
            .build();

    private static final ImmutableSet<Character> RIGHT_PUNCT_CHARS = new ImmutableSet.Builder<Character>()
            .add('“')
            .add('\'')
            .add('"')
            .add('»')
            .add(')')
            .add(']')
            .add('}')
            .add('…')
            .add('!')
            .add('’')
            .add('-')
            .add('?')
            .add('—')
            .add(',')
            .add('.')
            .add(';')
            .add(':')
            .build();

    private static final Set<String> NEW_LINE_ELEMENTS = ImmutableSet.of(
            "citation",
            "invention-title",
            "abstract",
            "heading",
            "p",
            "references-cited",
            "bibliographic-data",
            "description",
            "summary",
            "description-of-drawings",
            "detailed-desc",
            "claims",
            "claim");

    private static final ImmutableSet<Character> SENTENCE_END_CHARS = new ImmutableSet.Builder<Character>()
            .add('!')
            .add('?')
            .add('.')
            .add(';')
            .build();

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        //TaggerRunner tagger = TaggerRunner.instance("/Users/yhe/Dropbox/work/FUSE/Release/Models/ChineseTagger/model.tag");
        boolean blankLine = true;
        char[] docChars = doc.text().toCharArray();
        PrintWriter w = null;
        StringBuilder builder = null;
        try {
            w = new PrintWriter(new File(outputFileName));
            builder = new StringBuilder();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        Vector<Annotation> spanAnns = doc.annotationsOfType(tagToParse);
        if (spanAnns == null) {
            w.close();
            return;
        }
        else {
            for (Annotation spanAnn : spanAnns) {
                int i = spanAnn.start();
                while (i < spanAnn.end()) {
                    int j = skipAnnotationTo(doc, i);
                    if (j > i) {
                        if (builder.length() > 0) {
                            String output = word2CONLLString(builder.toString().trim(), blankLine);
                            if (output.endsWith("\n\n")) {
                                w.print(output);
                            } else {
                                w.println(output);
                            }
                            builder.setLength(0);
                            blankLine = true;
                        }
                        i = j;
                        continue;
                    }
                    String output;
                    if (Character.isWhitespace(doc.charAt(i))) {
                        if (builder.length() > 0) {
                            output = word2CONLLString(builder.toString().trim(), blankLine);
                            w.print(output);
                            builder.setLength(0);
                            blankLine = output.endsWith("\n\n");
                        }
                    }
                    else {
                        builder.append(docChars[i]);
                        blankLine = false;
                    }

                    if (hasNewLineElements(doc.annotationsAt(i)) &&
                            hasNewLineElements(doc.annotationsEndingAt(i))) {
                        if (!blankLine) {
                            if (builder.length() > 0) {
                                w.println(word2CONLLString(builder.toString().trim(), blankLine).trim());
                                builder.setLength(0);
                            }
                            w.println();
                        }
                        blankLine = true;
                    }
                    i++;
                }
            }
        }

        w.close();
    }

    private String word2CONLLString(String word, boolean blankLine) {
        if (ABBR_WORDS.contains(word.trim())) {
            return String.format("%s\t%s\t%s\tO\n", word.trim(), word.trim(), "ABR");
        }
        List<String> wordsBefore = new ArrayList<String>();
        List<String> wordsAfter = new ArrayList<String>();
        String rest = word;
        while ((rest.length() > 1) && (LEFT_PUNCT_CHARS.contains(rest.charAt(0)))) {
            wordsBefore.add(String.format("%c\t%c\tPUNCT\tO\n", rest.charAt(0), rest.charAt(0)));
            rest = rest.substring(1);
        }
        while ((rest.length() > 1) && (RIGHT_PUNCT_CHARS.contains(rest.charAt(rest.length() - 1)))) {
            wordsAfter.add(String.format("%c\t%c\tPUNCT\tO\n", rest.charAt(rest.length() - 1), rest.charAt(rest.length() - 1)));
            rest = rest.substring(0, rest.length() - 1);
        }
        StringBuilder resultBuf = new StringBuilder();
        for (String w : wordsBefore) {
            resultBuf.append(w);
        }

        resultBuf.append(String.format("%s\t%s\t%s\t%s\n",
                rest,
                rest,
                !blankLine && Character.isUpperCase(rest.charAt(0)) ? "NN" : "O",
                !blankLine && Character.isUpperCase(rest.charAt(0)) ? "B-NP" : "O"));

        boolean sentenceEnd = false;
        for (String w : wordsAfter) {
            resultBuf.append(w);
            sentenceEnd = SENTENCE_END_CHARS.contains(w.charAt(0));
        }

        if (sentenceEnd) resultBuf.append("\n");

        return resultBuf.toString();
    }

    private boolean hasNewLineElements(Vector<Annotation> anns) {
        if (anns == null) return false;
        for (Annotation ann : anns) {
            if (NEW_LINE_ELEMENTS.contains(ann.type())) {
                return true;
            }
        }
        return false;
    }

    private int skipAnnotationTo(FuseDocument doc, int i) {
        Vector<Annotation> anns = doc.annotationsAt(i);
        if (anns == null) return i;
        for (Annotation ann : anns) {
            if ((ann.get("lang") != null) && (((String) ann.get("lang")).trim().equals("eng"))) {
                return ann.end();
            }
        }
        return i;
    }

}
