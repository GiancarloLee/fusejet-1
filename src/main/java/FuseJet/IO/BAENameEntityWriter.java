package FuseJet.IO;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import com.google.common.collect.ImmutableMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 9/26/12
 * Time: 10:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class BAENameEntityWriter implements FuseDocumentWriter {

    private static final Map<String, String> typeMap =
        new ImmutableMap.Builder<String, String>().put("ORGDICT", "ORGANIZATION").build();

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        String[] annTypes = {"ENAMEX"};
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }
        Vector<Annotation> annotations = doc.annotationsOfType("ENAMEX");
        if (annotations != null) {
            for (Annotation ann : annotations) {
                String type = "";
                if (ann.get("type") != null) {
                    type = (String)ann.get("type");
                }
                else {
                    if (ann.get("TYPE") != null) {
                        type = (String)ann.get("TYPE");
                    }
                }
                if (typeMap.containsKey(type)) {
                    type = typeMap.get(type);
                }
                w.println(String.format("ENAMEX TYPE=\"%s\" START=\"%d\" END=\"%d\" TEXT=\"%s\"",
                        type, ann.start(), ann.end(), doc.text(ann.span()).replaceAll("\n", " ")));
            }
        }
        w.close();
    }
}
