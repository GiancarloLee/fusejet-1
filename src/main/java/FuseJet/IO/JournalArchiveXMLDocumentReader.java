package FuseJet.IO;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;
import Jet.Tipster.Document;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * User: yhe
 * Date: 5/25/12
 * Time: 2:09 PM
 */
public class JournalArchiveXMLDocumentReader implements XMLDocumentReader {
    private static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    private FuseDocument fuseDocument;
    private static final Map<String, String> TAG_ANNOTATION_MAP = new ImmutableMap.Builder<String, String>()
            .put("p", "P")
            .put("abstract", "ABSTRACT")
            .put("body", "BODY")
            .put("sec", "SECTION")
            .put("title", "SECTION-TITLE")
            .put("name", "NAME")
            .put("string-name", "NAME").build();
    private static final Set<String> TAG_TO_PROCESS = ImmutableSet.of(
            "xref",
            "contrib"
    );

    @Override
    public FuseDocument readXML(String fileName) throws ParserConfigurationException, IOException, SAXException {
        fuseDocument = new FuseDocument();

        builderFactory.setValidating(false);

        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        builder.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, IOException {
                    return new InputSource(new StringReader(""));

            }
        });
        org.w3c.dom.Document doc = builder.parse(new File(fileName));

        List <Annotation> annotations = new ArrayList<Annotation>();

        Node root = doc.getDocumentElement();
        String text = readFromNode(root, annotations, 0);

        fuseDocument.setText(text);
        stretchAndAddAnnotations(fuseDocument, annotations.toArray(new Annotation[0]));
//        fuseDocument.clearAnnotations();
//        for (Annotation ann : annotations) {
//            fuseDocument.addAnnotation(ann);
//        }
        //System.err.println(new Date());
        fuseDocument.splitAndTokenize();
        //System.err.println(new Date());
        return fuseDocument;
    }

    private String readFromNode(Node node, List<Annotation> annotations, int offset) {
        if (node instanceof Element) {
            StringBuilder s = new StringBuilder();
            NodeList nodes = node.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                s.append(readFromNode(nodes.item(i), annotations, offset + s.length()));
            }
            String annotationName = "ELEMENT_"+node.getNodeName();
            if (TAG_ANNOTATION_MAP.containsKey(node.getNodeName())) {
                annotationName = TAG_ANNOTATION_MAP.get(node.getNodeName());
            }
            Annotation annotation = AnnotationFactory.createSingleAttributeAnnotation(
                    annotationName,
                    fuseDocument,
                    offset,
                    offset + s.length(),
                    "id",
                    fuseDocument.getNextAnnotationId());
            annotations.add(annotation);
            return s.toString();
        }
        else {
            return node.getTextContent();
        }
    }

//    private String handleSection(Node node, int sectionId, int offset, Map<Integer, String> edits, List<Annotation> annotations) {
//        NodeList nodes = node.getChildNodes();
//        /* if has title */
//        int i = 0;
//        int currentOffset = offset;
//        StringBuilder text = new StringBuilder();
//        if (nodes.item(i).getNodeName().equals("title")) {
//            text.append(nodes.item(i).getTextContent());
//            text.append("\n");
//            Annotation annotation = AnnotationFactory.createSingleAttributeAnnotation("SECTION-TITLE",
//                    fuseDocument,
//                    currentOffset,
//                    currentOffset + text.length(),
//                    "id",
//                    fuseDocument.getNextAnnotationId());
//            annotations.add(annotation);
//            annotation = AnnotationFactory.createSingleAttributeAnnotation("P",
//                    fuseDocument,
//                    currentOffset,
//                    currentOffset + text.length(),
//                    "id",
//                    fuseDocument.getNextAnnotationId());
//            annotations.add(annotation);
//            currentOffset += text.length();
//            edits.put(currentOffset-1, "");
//            i++;
//        }
//        int pCount = 0;
//        while (i < nodes.getLength()) {
//            String pText = handleParagraph(nodes.item(i), currentOffset, edits, annotations);
//            Annotation annotation = AnnotationFactory.createSingleAttributeAnnotation("P",
//                    fuseDocument,
//                    currentOffset,
//                    // add the following NEWLINE character
//                    currentOffset + pText.length() + 1,
//                    "id",
//                    fuseDocument.getNextAnnotationId());
//            text.append(pText);
//            annotations.add(annotation);
//            currentOffset = currentOffset + pText.length();
//            text.append("\n");
//            edits.put(currentOffset, "");
//            currentOffset++;
//            pCount++;
//            i++;
//        }
//        return text.toString();
//    }
//
//    private String handleParagraph(Node node, int offset, Map<Integer, String> edits, List<Annotation> annotations) {
//        if (node.getNodeName().equals("fig") || node.getNodeName().equals("table-wrap") ||
//                node.getNodeName().equals("supplementary-material")) {
//            String figTableText = handleFigAndTable(node, offset, edits, annotations);
//            return figTableText;
//        }
//
//        NodeList nodes = node.getChildNodes();
//        StringBuilder text = new StringBuilder();
//        int currentOffset = offset;
//        for (int i = 0; i < nodes.getLength(); i++) {
//            Node currentNode = nodes.item(i);
//            if (currentNode.getNodeName().equals("xref") &&
//                    currentNode.getAttributes().getNamedItem("ref-type").getNodeValue().equals("bibr")) {
//                String citationText = currentNode.getTextContent();
//                String citationRid  = currentNode.getAttributes().getNamedItem("rid").getNodeValue();
//                Annotation annotation = AnnotationFactory.createDoubleAttributeAnnotation("CITATION",
//                        fuseDocument,
//                        currentOffset,
//                        currentOffset+citationText.length(),
//                        "id",
//                        fuseDocument.getNextAnnotationId(),
//                        "rid",
//                        citationRid);
//                annotations.add(annotation);
//                text.append(citationText);
//                currentOffset += citationText.length();
//            }
//            else {
//
//                    String rawText = currentNode.getTextContent();
//                    for (char c : rawText.toCharArray()) {
//                        if (c == '\n') {
//                            edits.put(currentOffset, " ");
//                            c = ' ';
//                        }
//                        text.append(c);
//                        currentOffset++;
//                    }
//                }
//
//        }
//        return text.toString();
//    }
//
//    private String handleFigAndTable(Node node, int offset, Map<Integer, String> edits, List<Annotation> annotations) {
//        NodeList nodes = node.getChildNodes();
//        StringBuilder text = new StringBuilder();
//        for (int i = 0; i < nodes.getLength(); i++) {
//            Node n = nodes.item(i);
//            String textContent = n.getTextContent();
//            text.append(textContent);
//            offset += textContent.length();
//            if (n.getNodeName().equals("label") || n.getNodeName().equals("caption")) {
//                text.append("\n");
//                edits.put(offset, "");
//                offset++;
//            }
//        }
//        Annotation annotation = AnnotationFactory.createSingleAttributeAnnotation("P",
//                fuseDocument,
//                offset,
//                // add the following NEWLINE character
//                offset + text.length(),
//                "id",
//                fuseDocument.getNextAnnotationId());
//        return text.toString();
//    }
//
    private void stretchAndAddAnnotations (Document doc, Annotation[] annotations) {
        int length = doc.length();
        boolean[] startingPoint = new boolean[length];
        for (int i = 0; i < annotations.length; i++) {
            Annotation a = annotations[i];
            startingPoint[a.start()] = true;
        }
        for (int i = 0; i < annotations.length; i++) {
            Annotation a = annotations[i];
            // annotations must be removed and re-added so that they are properly
            // indexed on end position
            doc.removeAnnotation(a);
            Span s = a.span();
            int posn = s.end();
            while (posn < length && !startingPoint[posn] && Character.isWhitespace(doc.charAt(posn)))
                posn++;
            s.setEnd(posn);
            doc.addAnnotation(a);
        }
    }
}
