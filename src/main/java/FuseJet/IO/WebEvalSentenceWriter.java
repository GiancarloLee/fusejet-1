package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Annotation;
import Jet.Tipster.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * User: yhe
 * Date: 2/22/13
 * Time: 10:35 PM
 */
public class WebEvalSentenceWriter implements FuseDocumentWriter {

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Error opening output file: " + outputFileName);
            return;
        }
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        Set<FuseAnnotation> allAnns = doc.getFuseEntities();
        if ((sentences == null) || (allAnns == null)) {
            w.close();
            return;
        }
        Set<FuseAnnotation> allCitations = new HashSet<FuseAnnotation>();
        for (FuseAnnotation ann : allAnns) {
            if (ann.getEntityType() == FuseAnnotation.AnnotationType.CITATION) {
                allCitations.add(ann);
            }
        }

        for (int i = 0; i < sentences.size(); i++) {
            Annotation sentence = sentences.get(i);
            String prevString = i == 0 ? "" : doc.text(sentences.get(i - 1).span()).replace('\n', ' ');
            String nextString = i == sentences.size()-1 ? "" : doc.text(sentences.get(i + 1).span()).replace('\n', ' ');
            int sentenceStart = sentence.start();
            int sentenceEnd = sentence.end();
            for (FuseAnnotation ann : allCitations) {
                if ((ann.start() > sentence.start()) &&
                        (ann.end() < sentence.end())) {
                    String fileName = "UNKNOWN";
                    Vector<Annotation> fileNameAnns = doc.annotationsOfType("fileName");
                    if (fileNameAnns != null && fileNameAnns.size() > 0) {
                        String fn = (String)fileNameAnns.get(0).get("fileName");
                        if (fn != null) fileName = fn;
                    }
                    printExample(fileName, doc, sentence, ann, prevString, nextString, w);
                }
            }
        }

        w.close();
    }

    private void printExample(String filename, Document doc, Annotation sentence, Annotation citation, String pre, String next, PrintWriter w) {
        w.println(String.format("file:%s|||offset:%d|||cite:%d-%d|||text:%s|||pre:%s|||next:%s",
                filename,
                sentence.start(),
                citation.start(),
                citation.end(),
                doc.text(sentence.span()).replace('\n', ' '),
                pre,
                next
        ));
    }
}
