package FuseJet.IO;

import FuseJet.Models.*;
import FuseJet.Utils.FuseUtils;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableSet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author yhe & gcl
 */
public class NYUFactsReader implements FuseDocumentReader {

    private Map<String, FuseAnnotation> annotationMap = new HashMap<String, FuseAnnotation>();

    private static final Set<String> fuseAnnTypes = ImmutableSet.of(
            "JARGON", "ENAMEX","SIGNAL", "CITATION", "ORGANIZATION", "PERSON", "URL");

    public FuseDocument read(String textFileName, String factsFileName) {
        FuseDocument doc = new FuseDocument();
        String[] factFileNames = factsFileName.split("\\|\\|\\|");
        List<String> relationBuf = new ArrayList<String>();
        try {
            doc.setText(FuseUtils.readFileAsString(textFileName));
            String text = doc.text();

            int[] textRecord = new int[text.length()+1];
            for (String fileName : factFileNames) {
                BufferedReader r = new BufferedReader(new FileReader(fileName));
                String line;
                while ((line = r.readLine()) != null) {
                    if (line.trim().length() == 0) continue;
                    if (line.startsWith("RELATION")) {
                        relationBuf.add(line);
                        continue;
                    }
                    if (line.startsWith("SIGNAL")){
                        line = line.replaceFirst("_", " ");
                    }
                    String[] parts = line.split(" ");
                    //System.err.println(parts[0]);
                    if (fuseAnnTypes.contains(parts[0].toUpperCase())) {
                        FuseAnnotation fuseAnn = null;
                        Annotation ann =  null;
                        try {
                            fuseAnn = fuseAnnotationFromString(line.trim());
                            if (fuseAnn == null) {
                                System.err.println("Parse failed for line: " + line.trim());
                                continue;
                            }
                            if (parts[0].toUpperCase().equals("JARGON")) {
                                fuseAnn.setCategory(FuseAnnotation.AnnotationCategory.JARGON);
                            }else if (parts[0].toUpperCase().equals("SIGNAL")){
                                fuseAnn.setCategory(FuseAnnotation.AnnotationCategory.SIGNAL);
                            }
                            else {
                                fuseAnn.setCategory(FuseAnnotation.AnnotationCategory.ENAMEX);
                            }
                            if (fuseAnn.getCategory() == FuseAnnotation.AnnotationCategory.ENAMEX) {
                                fuseAnn.setEntityType(FuseAnnotation.AnnotationType.valueOf(parts[0]));

                            }
                            fuseAnn.setType("fuse-entity");
                            //System.err.println("FuseEntity id: " + ((String)fuseAnn.get("id")).toUpperCase());
                            if (line.contains("NYU_ID=")){
                                annotationMap.put(((String) fuseAnn.get("nyu_id")).toUpperCase(), fuseAnn);
                            }else{
                                annotationMap.put(((String) fuseAnn.get("id")).toUpperCase(), fuseAnn);
                            }
                            doc.addAnnotation(fuseAnn);
                        }
                        catch (Exception e) {
                            System.err.print("\nUnable to parse annotation. Line: "+line+"\nStacktrace: ");
                            e.printStackTrace();
                        }
                    }
                }
                annotationMap.put("PAPER", FuseDocument.THIS_ARTICLE);

            }
            for (String relationLine : relationBuf) {
                FuseRelation relation = relationFromString(relationLine);
                doc.addRelation(relation);
                //System.err.println("Relation added...");
            }

        }
        catch (IOException e) {
            System.err.println("Error reading NYU fact file:" + factsFileName);
            e.printStackTrace();
            return doc;
        }
        return doc;
    }

    public FuseRelation relationFromString(String s) {
        String[] parts = s.split(" ");
        int start = -1;
        int end   = -1;
        if (parts.length < 3) return null;
        String type = parts[0].trim().toLowerCase();
        FeatureSet fs = new FeatureSet();
        boolean quoted = false;
        StringBuilder valueBuffer = new StringBuilder(100);
        String key = "";
        for (int i = 1; i < parts.length; i++) {
            if (!quoted) {
                String[] kv = parts[i].split("=");
                if (kv.length < 2) {
                    valueBuffer.setLength(0);
                }
                else {
                    key = kv[0];
                    valueBuffer.setLength(0);
                    if (kv[1].startsWith("\"")) {
                        if ((kv[1].length() < 2)|| kv[1].endsWith("\"")) {
                            fs.put(key.toLowerCase(), kv[1].replaceAll("\"", ""));
                        }
                        else {
                            valueBuffer.append(kv[1].substring(1));
                            quoted = true;
                        }
                    }
                    else {
                        if (key.equals("START") || key.equals("END")) {
                            if (key.equals("START")) {
                                start = Integer.valueOf(kv[1]);
                            }
                            else {
                                end = Integer.valueOf(kv[1]);
                            }
                        }
                        else {
                            fs.put(key.toLowerCase(), kv[1]);
                        }
                    }
                }
            }
            else {
                if (parts[i].endsWith("\"")) {
                    valueBuffer.append(" ").append(parts[i].substring(0, parts[i].length() - 1));
                    fs.put(key.toLowerCase(), valueBuffer.toString());
                    valueBuffer.setLength(0);
                    quoted = false;
                }
                else {
                    valueBuffer.append(" ").append(parts[i]);
                }
            }
        }
        if ((start < 0) || (end < 0)) return null;
        String arg1Str = fs.containsFeature("arg1_nyu_id") ? ((String)fs.get("arg1_nyu_id")).toUpperCase() : ((String)fs.get("arg1")).toUpperCase();
        if (arg1Str.toUpperCase().equals("PAPER")) {
            fs.put("arg1", FuseDocument.THIS_ARTICLE);
        }
        else {
            if (annotationMap.containsKey(arg1Str)) {
                fs.put("arg1", annotationMap.get(arg1Str));
            }
            else {
                System.err.println("arg1: " + arg1Str);
            }
        }
        String arg2Str = fs.containsFeature("arg2_nyu_id") ? ((String)fs.get("arg2_nyu_id")).toUpperCase() : ((String)fs.get("arg2")).toUpperCase();
        if (arg2Str.toUpperCase().equals("PAPER")) {
            fs.put("arg2", FuseDocument.THIS_ARTICLE);
        }
        else {
            if (annotationMap.containsKey(arg2Str)) {
                fs.put("arg2", annotationMap.get(arg2Str));
            }
            else {
                System.err.println("arg2: " + arg2Str);
            }
        }
        if (fs.get("signal_entity") != null){
            String signalStr = ((String)fs.get("signal_entity")).toUpperCase();
            if (annotationMap.containsKey(signalStr)) {
                fs.put("signal_entity", annotationMap.get(signalStr));
            }
            else {
                System.err.println("Signal: " + signalStr);
            }
        }
        if (fs.containsFeature("subtype")) {
            fs.put("arg3", fs.get("subtype"));
        }

        String arg3 = fs.get("arg3") == null ? "" : (String)fs.get("arg3");
        String arg4 = fs.get("arg4") == null ? "" : (String)fs.get("arg4");
        String textSignal = fs.get("text_signal") == null ? "" : (String)fs.get("text_signal");
        String grammaticalSignal = "";
        if(fs.get("grammatical_signal") == null){
            if (fs.get("gram_signal") == null){
                grammaticalSignal = "";
            }else{
                grammaticalSignal = (String)fs.get("gram_signal");
            }
        }else{
            grammaticalSignal = (String)fs.get("grammatical_signal");
        }
        if (fs.get("signal_entity") != null){
            return new FuseRelation(
                    "",
                    (FuseAnnotation)fs.get("arg1"),
                    (FuseAnnotation)fs.get("arg2"),
                    arg3,
                    arg4,
                    FuseRelation.RelationType.valueOf((String)fs.get("type")),
                    (FuseAnnotation)fs.get("signal_entity"),
                    grammaticalSignal);
        }else{
            return new FuseRelation(
                    "",
                    (FuseAnnotation)fs.get("arg1"),
                    (FuseAnnotation)fs.get("arg2"),
                    arg3,
                    arg4,
                    FuseRelation.RelationType.valueOf((String)fs.get("type")),
                    textSignal,
                    grammaticalSignal);
        }
    }

    public FuseAnnotation fuseAnnotationFromString(String s) {
        String[] parts = s.split(" ");
        int start = -1;
        int end   = -1;
        if (parts.length < 3) return null;
        String type = parts[0].trim().toLowerCase();
        FeatureSet fs = new FeatureSet();
        boolean quoted = false;
        StringBuilder valueBuffer = new StringBuilder(100);
        String key = "";
        for (int i = 1; i < parts.length; i++) {
            if (!quoted) {
                String[] kv = parts[i].split("=");
                if (kv.length < 2) {
                    valueBuffer.setLength(0);
                }
                else {
                    key = kv[0];
                    valueBuffer.setLength(0);
                    if (kv[1].startsWith("\"")) {
                        if ((kv[1].length() < 2)|| kv[1].endsWith("\"")) {
                            fs.put(key.toLowerCase(), kv[1].replaceAll("\"", ""));
                        }
                        else {
                            valueBuffer.append(kv[1].substring(1));
                            quoted = true;
                        }
                    }
                    else {
                        if (key.equals("START") || key.equals("END")) {
                            if (key.equals("START")) {
                                start = Integer.valueOf(kv[1]);
                            }
                            else {
                                end = Integer.valueOf(kv[1]);
                            }
                        }
                        else {
                            fs.put(key.toLowerCase(), kv[1]);
                        }
                    }
                }
            }
            else {
                if (parts[i].endsWith("\"")) {
                    valueBuffer.append(" ").append(parts[i].substring(0, parts[i].length() - 1));
                    fs.put(key.toLowerCase(), valueBuffer.toString());
                    valueBuffer.setLength(0);
                    quoted = false;
                }
                else {
                    valueBuffer.append(" ").append(parts[i]);
                }
            }
        }
        if ((start < 0) || (end < 0)) {
            System.err.println("Start and End information missing... Possibly lead to failed parse.");
            return null;
        }
        if (type.equals("structure")) {
            type = ((String)fs.get("type")).trim().toLowerCase();
            fs.remove("type");
        }
        return new FuseAnnotation("fuse-entity", new FuseEntitySpan(start, end), fs,
                FuseAnnotation.AnnotationCategory.NA, FuseAnnotation.AnnotationType.NA);
    }

    @Override
    public FuseDocument read(String fileIdentifier) {
        String[] parts = fileIdentifier.split(";");
        if (parts.length != 2) {
            System.err.println("A BAE file identifier should contain the txt filename and the fact filename," +
                    "separated by ;");
            return null;
        }
        else {
            String txtFileName = parts[0];
            String factFileName = parts[1];
            return read(txtFileName, factFileName);
        }
    }
}