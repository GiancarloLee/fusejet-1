package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseRelation;
import Jet.Tipster.Span;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Does not output FuseEntities but only FuseRelations.
 * FuseRelations will have START and END.
 */
public class CollapsedRelationWriter extends BAEDocumentWriter {
    @Override
    public void write(FuseDocument doc, String outputFileName) {
        Map<FuseAnnotation, String> entities = new HashMap<FuseAnnotation, String>();
        //Map<FuseRelation, Integer> relations = new HashMap<FuseRelation, Integer>();
        // For now, we don't control spans, as spans are already
        // we don't care about validAnnTypes also
        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        entities.put(FuseDocument.THIS_ARTICLE, "PAPER");
        int eid = 1;
        for (FuseAnnotation entity : doc.getFuseEntities()) {
            if (!SHOULD_PRINT_SIGNAL) {
                if (entity.getCategory() == FuseAnnotation.AnnotationCategory.SIGNAL) continue;
            }
            String text = "";
            if (entity != FuseDocument.THIS_ARTICLE) {
                text = doc.text(entity.span()).trim().replace("\n", " ");
            }
            String baeId = null;
            if (entity.get("bae_id") != null) {
                baeId = (String)entity.get("bae_id");
            }
            String id = new StringBuilder("NYU_").append(entity.getCategory()).append("_").append(eid).toString();
//            if ((entity.get("id") != null) && (!((String)entity.get("id")).trim().equals("")))
//                id = (String)entity.get("id");
            if (baeId != null) {
                id = baeId;
            }
            entities.put(entity, id);
            eid++;
        }
        for (FuseAnnotation entity : entities.keySet()) {
            String id = entities.get(entity);
            // Do not print "PAPER" and BAE facts
            if ((entity != FuseDocument.THIS_ARTICLE) && !id.startsWith("http://bae")) {
                w.println(String.format("%s ID=\"%s\" TYPE=\"%s\" START=%d END=%d TEXT=\"%s\"",
                        entity.getCategory(),
                        id,
                        entity.getEntityType(),
                        entity.start(),
                        entity.end(),
                        doc.text(new Span(entity.start(), entity.end()))
                ));
            }
        }
        int rid = 1;
        for (FuseRelation relation : doc.getRelations()) {
            String[] argNames;
            if (ARG_NAME_MAP.containsKey(relation.getType().toString()+"."+relation.getArg3().toUpperCase())) {
                argNames = ARG_NAME_MAP.get(relation.getType().toString()+"."+relation.getArg3().toUpperCase());
            }
            else {
                argNames = ARG_NAME_MAP.get(relation.getType().toString()+".");
            }
            int s = relation.getArg1().start();
            int e = relation.getArg2().end();
            if (relation.getArg1().start() == -1) {
                s = relation.getArg2().start();
                e = relation.getArg2().end();
            }
            else {
                if (s > e) {
                    s = relation.getArg2().start();
                    e = relation.getArg1().end();
                }
            }
            String textSignal = "";
            if (relation.getAnnotatedSignal() != null) {
                FuseAnnotation signal = relation.getAnnotatedSignal();
                if (signal.start() > 0 && signal.end() > 0) {
                     textSignal = doc.text(signal).trim();
                }
            }
            w.println(String.format("RELATION ID=\"%s\" TYPE=\"%s\" ARG1=\"%s\" ARG1_NAME=\"%s\" ARG2=\"%s\" ARG2_NAME=\"%s\" SUBTYPE=\"%s\" TEXT_SIGNAL=\"%s\" START=\"%d\" END=\"%d\"",
                    String.format("NYU_RELATION_%s_%d",
                            relation.getArg3().trim().equals("") ? relation.getType().toString() : relation.getType().toString()+"_"+relation.getArg3(),
                            rid),
                    relation.getType().toString(),
                    entities.get(relation.getArg1()),
                    argNames[0],
                    entities.get(relation.getArg2()),
                    argNames[1],
                    relation.getArg3().toUpperCase(),
                    textSignal,
                    s,
                    e
            ));
            rid++;
        }

        w.close();
    }

}
