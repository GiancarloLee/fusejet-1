package FuseJet.IO;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: llinda
 * Date: 7/8/13
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class SentimentPredictionWriter implements FuseDocumentWriter{

    @Override
    public void write(FuseDocument doc, String outputFileName) {
        Map<FuseAnnotation, String> entities = new HashMap<FuseAnnotation, String>();

        PrintWriter w = null;
        try {
            w = new PrintWriter(new File(outputFileName));
        } catch (FileNotFoundException e) {
            System.err.println("Unable to open file for output: " + outputFileName);
            return;
        }
        entities.put(FuseDocument.THIS_ARTICLE, "PAPER");
        Vector<Annotation> opinions = doc.annotationsOfType("OPINION");
        for (Annotation ann: opinions ) {
            w.println(String.format("TYPE=%s " + "START=%d " +
                    "END=%d " +
                    "TERM=%s "    +
                    "SENTENCE=%s " + "\n",
                    ann.get("PREDICTION"), ann.start(), ann.end(), ann.get("TERM"), ann.get("SENTENCE")));
            //  System.out.println(ann.attributes());
            //String term = doc.annotationsAt(ann.start(), "OPINION");
        }
        w.close();
    }

}

