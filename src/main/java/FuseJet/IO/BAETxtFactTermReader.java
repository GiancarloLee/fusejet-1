package FuseJet.IO;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import FuseJet.Utils.FuseUtils;
import Jet.Lex.Lexicon;
import Jet.Tipster.Annotation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 6/18/13
 * Time: 3:11 PM
 */
public class BAETxtFactTermReader extends BAEDocumentReader {

    @Override
    public FuseDocument read(String fileIdentifier) {
        String[] parts = fileIdentifier.split(";");
        if (parts.length != 3) {
            System.err.println("A BAE file identifier should contain the txt filename and the fact filename," +
                    "separated by ;");
            return null;
        }
        else {
            String txtFileName = parts[0];
            String factFileName = parts[1];
            String termFileName = parts[2];
            return read(txtFileName, factFileName, termFileName);
        }
    }

    public FuseDocument read(String textFileName, String factsFileName, String termFileName) {
        FuseDocument doc = read(textFileName, factsFileName);
        try {
            addTermAnnotation(doc, termFileName);
        }
        catch (IOException e) {
            System.err.println("Error reading term file: " + termFileName);
            e.printStackTrace();
        }
        List<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences != null) {
            for (Annotation sentence : sentences) {
                Lexicon.annotateWithDefinitions(doc, sentence.start(), sentence.end());
            }
        }
        return doc;
    }

    public void addTermAnnotation(FuseDocument doc, String termFileName) throws IOException{
        BufferedReader r = new BufferedReader(new FileReader(termFileName));
        String line;
        //System.err.println();
        while ((line = r.readLine()) != null) {
            Annotation ann = annotationFromString(line.trim());
            //System.err.println(ann + " TEXT:" + doc.text(ann));
            doc.addAnnotation(ann);
        }
        doc.stretchAll();
        List<Annotation> terms = doc.annotationsOfType("term");
        for (Annotation term : terms) {
            //System.err.println("[Stretched] " + term + " TEXT:" + doc.text(term));
        }
        r.close();
    }


}
