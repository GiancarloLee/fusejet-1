package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.Tipster.Annotation;

import java.util.Map;
import java.util.Vector;

/**
 * User: yhe
 * Date: 9/8/12
 * Time: 11:00 AM
 */
public class UncasedDictionaryAnnotator extends DictionaryAnnotator {

    @Override
    protected void addItemToTree(DictionaryItem item) {
        tree.add(item.getWord().toUpperCase().replace("\\-", " ").getBytes(), item);
    }

    @Override
    protected String buildInputString(FuseDocument doc, Vector<Annotation> tokens, Map<Integer, Integer> tokeniStart, Map<Integer, Integer> tokeniEnd) {
        StringBuilder input = new StringBuilder();
        for (int i = 0; i < tokens.size(); i++) {
            Annotation token = tokens.get(i);
            tokeniStart.put(input.length(), i);
            input.append(doc.text(token.span()).trim().toUpperCase());
            tokeniEnd.put(input.length(), i);
            input.append(" ");
        }
        return input.toString().replace("\\-", " ");
    }
}
