package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.CRF.NE.CRFDomainAdaptedNameTagger;
import Jet.Lex.Lexicon;
import Jet.Tipster.Annotation;

import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 6/3/13
 * Time: 5:24 PM
 */
public class CRFNameEntityAnnotator implements FuseJetAnnotator  {
    private CRFDomainAdaptedNameTagger tagger;

    @Override
    public void initialize(String[] resourceInformation) {
        try {
            tagger = new CRFDomainAdaptedNameTagger(resourceInformation[0]);
        }
        catch (Exception e) {
            System.err.println("Error initializing CRF name tagger...");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public void annotate(FuseDocument doc) {
        List<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) return;
        for (Annotation sentence : sentences) {
            Lexicon.annotateWithDefinitions(doc, sentence.start(), sentence.end());
        }
        tagger.annotate(doc);
    }
}
