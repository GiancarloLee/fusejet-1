package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableSet;
import domaintagger.TaggerRunner;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 6/25/13
 * Time: 10:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChineseNPAnnotator implements FuseJetAnnotator {
    @Override
    public void annotate(FuseDocument doc) {
        tokenize(doc);
    }

    private String tagToParse = "TEXT";

    private boolean SEGMENTATION_DOUBLE_CHECK = true;

    private TaggerRunner tagger = null;

    @Override
    public void initialize(String[] resourceInformation) {
        if (resourceInformation.length != 2) {
            System.err.println("Chinese tagger initialization error: unable to obtain correct path.");
            System.exit(-1);
        }
        tagToParse = resourceInformation[0];
        if (tagToParse.trim().equals("")) {
            tagToParse = "TEXT";
        }
        tagger = TaggerRunner.instance(resourceInformation[1]);
    }


    private static final Set<Character> SENTENCE_END_CHARS = ImmutableSet.of(
            '。',
            '；'
    );


    public void tokenize(FuseDocument doc) {
        //TaggerRunner tagger = TaggerRunner.instance("/Users/yhe/Dropbox/work/FUSE/Release/Models/ChineseTagger/model.tag");
        boolean blankLine = true;
        char[] docChars = doc.text().toCharArray();
        doc.addAnnotation(new Annotation("fullspan", doc.fullSpan(), new FeatureSet()));
        StringBuilder builder = null;
        long taggingTime = 0, ansjTaggingTime = 0, ansjStartTime, startTime, endTime;
        builder = new StringBuilder();
        Vector<Annotation> spanAnns = doc.annotationsOfType(tagToParse);
        if (spanAnns == null) {
            return;
        }
        else {
            for (Annotation spanAnn : spanAnns) {
                //System.err.println(spanAnn);
                int i = spanAnn.start();
                i = skipAnnotationTo(doc, i);
                while (i < spanAnn.end()) {
                    if (!Character.isWhitespace(doc.charAt(i))) break;
                    i++;
                }
                if (i >= spanAnn.end()) continue;
                int begin = i;
                for (; i < spanAnn.end(); i++) {
                    if (SENTENCE_END_CHARS.contains(doc.charAt(i))) {
                        buildTaggedString(doc, begin, i+1);
                        begin = i + 1;
                    }
                }
                if (begin < i + 1) {
                    buildTaggedString(doc, begin, i+1);
                }
            }
        }

        System.err.print("(tagging: "+taggingTime+" ms)\t");
        //w.close();
    }

    public void buildTaggedString(FuseDocument doc, int start, int end) {
        String text = doc.text(new Span(start, end));
        String strippedText = text.replaceAll("\\s", "");
        if (strippedText.trim().length() == 0) return;
        //System.err.println("[StringBuilder]" + strippedText);
        //System.err.println("[DOCUMENT.TEXT]" + doc.text(new Span(start, end)));
        // Add sentence boundaries
        Annotation sentence = new Annotation("sentence", new Span(start, end), new FeatureSet());
        doc.addAnnotation(sentence);
        TaggerRunner.WordTag[] wordTags = tagger.tagString(strippedText);
        List<Term> ansjTaggedStr = ToAnalysis.paser(strippedText);
        int[] ansjSegments = getSegmentIndices(ansjTaggedStr);
        int front = 0;
        for (TaggerRunner.WordTag wordTag : wordTags) {
            String word = wordTag.getWord();
            //System.err.println(word);
            char[] chars = word.toCharArray();
            int s = front;
            for (int i = 0; i < chars.length && front < text.length(); i++) {
                if (chars[i] == text.charAt(front)) {
                    if (i == 0) s = front;
                    front++;
                }
            }
            while (front < text.length() && Character.isWhitespace(text.charAt(front))) front++;
            if (s < front) {
                Annotation token = new Annotation("token", new Span(start + s, start + front), new FeatureSet());
                doc.addAnnotation(token);
                Annotation constit = new Annotation("constit", new Span(start + s, start + front), new FeatureSet());
                doc.addAnnotation(constit);
            }
            //System.err.println(token);
        }
    }

    public String buildTaggedString(String s) {
        TaggerRunner.WordTag[] wordTags = tagger.tagString(s);
        List<Term> ansjTaggedStr = ToAnalysis.paser(s);
        int[] ansjSegments = getSegmentIndices(ansjTaggedStr);
        //new NatureRecognition(ansjTaggedStr).recogntion();
        String taggedStr = wordTag2String(wordTags, ansjSegments);
        return taggedStr;
    }

    private int[] getSegmentIndices(List<Term> taggedString) {
        List<Integer> segmentIndices = new ArrayList<Integer>();
        segmentIndices.add(0);
        int len = 0;
        for (Term t : taggedString) {
            //System.out.println(t.getName());
            len += t.getName().length();
            segmentIndices.add(len);
        }
        int[] result = new int[segmentIndices.size()];
        for (int i = 0; i < segmentIndices.size(); i++) {
            result[i] = segmentIndices.get(i);
        }
        return result;
    }

    private String addTokens(TaggerRunner.WordTag[] wordTags, int[] segmentIndices, FuseDocument doc, int start, int end) {
        boolean isFirstWordInNP = true;
        StringBuilder builder = new StringBuilder();
        int furthest = 0;
        int len = 0;
        int bufLen = 0;
        List<TaggerRunner.WordTag> wordTagBuffer = new ArrayList<TaggerRunner.WordTag>();
        for (TaggerRunner.WordTag wordTag : wordTags) {
            if (!isPartOfNP(wordTag)) {
                if (!wordTagBuffer.isEmpty()) {
                    //if ((numInArr(len-bufLen, 0, segmentIndices)) &&
                    //        (numInArr(len, 0, segmentIndices))) {
                    if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) ) {
                        furthest = len + bufLen;
                        if (isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                            appendBuilder(wordTagBuffer, "B-NP", builder);
                        }
                        else {
                            appendBuilder(wordTagBuffer, "O", builder);
                        }
                    }
                    else {
                        appendBuilder(wordTagBuffer, "O", builder);
                    }
                    wordTagBuffer.clear();
                    bufLen = 0;
                }
                appendBuilder(wordTag, "O", builder);
                len += wordTag.getWord().length();
            }
            else {
                wordTagBuffer.add(wordTag);
                len += wordTag.getWord().length();
                bufLen += wordTag.getWord().length();
            }
        }
        if (!wordTagBuffer.isEmpty()) {
            //if ((numInArr(len - bufLen, 0, segmentIndices)) &&
            //        (numInArr(len, 0, segmentIndices)) &&
            if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) &&
                    isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                appendBuilder(wordTagBuffer, "B-NP", builder);
            }
            else {
                appendBuilder(wordTagBuffer, "O", builder);
            }
            wordTagBuffer.clear();
            bufLen = 0;
        }
        builder.append("\n");
        return builder.toString();
    }

    private String wordTag2String(TaggerRunner.WordTag[] wordTags, int[] segmentIndices) {
        boolean isFirstWordInNP = true;
        StringBuilder builder = new StringBuilder();
        int furthest = 0;
        int len = 0;
        int bufLen = 0;
        List<TaggerRunner.WordTag> wordTagBuffer = new ArrayList<TaggerRunner.WordTag>();
        for (TaggerRunner.WordTag wordTag : wordTags) {
            if (!isPartOfNP(wordTag)) {
                if (!wordTagBuffer.isEmpty()) {
                    //if ((numInArr(len-bufLen, 0, segmentIndices)) &&
                    //        (numInArr(len, 0, segmentIndices))) {
                    if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) ) {
                        furthest = len + bufLen;
                        if (isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                            appendBuilder(wordTagBuffer, "B-NP", builder);
                        }
                        else {
                            appendBuilder(wordTagBuffer, "O", builder);
                        }
                    }
                    else {
                        appendBuilder(wordTagBuffer, "O", builder);
                    }
                    wordTagBuffer.clear();
                    bufLen = 0;
                }
                appendBuilder(wordTag, "O", builder);
                len += wordTag.getWord().length();
            }
            else {
                wordTagBuffer.add(wordTag);
                len += wordTag.getWord().length();
                bufLen += wordTag.getWord().length();
            }
        }
        if (!wordTagBuffer.isEmpty()) {
            //if ((numInArr(len - bufLen, 0, segmentIndices)) &&
            //        (numInArr(len, 0, segmentIndices)) &&
            if (segmentationAgreed(wordTagBuffer, len-bufLen, segmentIndices) &&
                    isEndOfNP(wordTagBuffer.get(wordTagBuffer.size()-1))){
                appendBuilder(wordTagBuffer, "B-NP", builder);
            }
            else {
                appendBuilder(wordTagBuffer, "O", builder);
            }
            wordTagBuffer.clear();
            bufLen = 0;
        }
        builder.append("\n");
        return builder.toString();
    }

    private boolean segmentationAgreed(List<TaggerRunner.WordTag> wordTags, int offset, int[] segmentationIndices) {
        if (!SEGMENTATION_DOUBLE_CHECK) return true;
        int segPoint = offset;
        if (!numInArr(segPoint, 0, segmentationIndices)) return false;
        for (TaggerRunner.WordTag wordTag : wordTags) {
            int i = segPoint + 1;
            segPoint += wordTag.getWord().length();
            for (;i < segPoint; i++) {
                if (numInArr(i, 0, segmentationIndices)) return false;
            }
            if (!numInArr(segPoint, 0, segmentationIndices)) return false;
        }
        return true;
    }

    private void appendBuilder(List<TaggerRunner.WordTag> wordTags, String tag, StringBuilder b) {
        for (TaggerRunner.WordTag wordTag : wordTags) {
            b.append(wordTag.getWord())
                    .append("\t")
                    .append(wordTag.getWord())
                    .append("\t")
                    .append(tag.equals("O") ? "O" : wordTag.getTag())
                    .append("\t")
                    .append(tag)
                    .append("\n");
            tag = tag.equals("O") ? "O" : "I-NP";
        }
    }

    private void appendBuilder(TaggerRunner.WordTag wordTag, String tag, StringBuilder b) {
        b.append(wordTag.getWord())
                .append("\t")
                .append(wordTag.getWord())
                .append("\t")
                .append(tag.equals("O") ? "O" : wordTag.getTag())
                .append("\t")
                .append(tag)
                .append("\n");
    }

    private boolean numInArr(int num, int furthest, int[] arr) {
        for (int i = furthest; i < arr.length; i++) {
            if (num == arr[i]) return true;
        }
        return false;
    }

    private boolean isPartOfNP(TaggerRunner.WordTag wordTag) {
        return (wordTag.getTag().equals("NN") ||
                wordTag.getTag().equals("NR") ||
                wordTag.getTag().equals("JJ"));
    }

    private boolean isEndOfNP(TaggerRunner.WordTag wordTag) {
        return wordTag.getTag().equals("NN") ||
                wordTag.getTag().equals("NR");
    }


    private int skipAnnotationTo(FuseDocument doc, int i) {
        Vector<Annotation> anns = doc.annotationsAt(i);
        if (anns == null) return i;
        for (Annotation ann : anns) {
            if ((ann.get("lang") != null) && (((String)ann.get("lang")).trim().equals("eng"))) {
                return ann.end();
            }
        }
        return i;
    }



}
