package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;
import com.google.common.collect.ImmutableSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 12/11/12
 * Time: 5:54 PM
 */
public class ForwardPeekSentenceSplitter implements FuseJetAnnotator {
    private static final ImmutableSet<Character> allowableChars =
            new ImmutableSet.Builder<Character>()
            .add('[')
            .add(']')
            .add('(')
            .add(')')
            .add(',')
            .add(';')
            .build();

    private static final int MIN_SEMI_COLON_SPLIT_LENGTH = 200;

    private static final int MAX_MERGED_SENT_LENGTH = 100;
    private Vector<Annotation> citationsInDoc = new Vector<Annotation>();

    @Override
    public void initialize(String[] resourceInformation) {
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> annotations = doc.annotationsOfType("sentence");
        citationsInDoc = doc.annotationsOfType("citation");
        if (citationsInDoc == null) {
            citationsInDoc = new Vector<Annotation>();
        }
        if (annotations == null) return;
        doc.removeAnnotationsOfType("sentence");
        List<Annotation> annotationBuf = new ArrayList<Annotation>();
        for (Annotation sentence : annotations) {
            List<Annotation> sentences = forwardPeekSplit(sentence, doc);
            annotationBuf.addAll(sentences);
        }
        annotationBuf = forwardPeekMerge(annotationBuf, doc);
        annotationBuf = etAlMerge(annotationBuf, doc);
        annotationBuf = splitSemicolon(annotationBuf, doc);
        annotationBuf = mergeCitation(annotationBuf, doc);
        for (Annotation s : annotationBuf) {
            doc.addAnnotation(s);
        }

        //System.out.println("test");
    }

    public List<Annotation> forwardPeekSplit(Annotation sentence, FuseDocument doc) {
        char[] chars = doc.text(sentence.span()).toCharArray();
        List<Annotation> result = new ArrayList<Annotation>();
        int chunkStart = 0;
        boolean afterDot = false;
        int i = 0;
        while (i < chars.length) {
            //System.err.println(i + "/" + chars.length);
                    //+ "IN:" + doc.text(sentence.span()));
//            if (i == 5068) {
//                System.err.println("stop here");
//            }
            if (chars[i] == '.') {
                afterDot = true;
                i++;
                continue;
            }
            Vector<Annotation> citations = doc.annotationsAt(i, "citation");
            if ((citations != null) && (citations.size() > 0)) {
                int j = citations.get(0).end();
                i = j>i ? j : i+1;
                continue;
            }
            if (afterDot) {
                if (allowableChars.contains(chars[i])) {
                    i++;
                    continue;
                }
                if (Character.isDigit(chars[i])) {
                    i++;
                    continue;
                }
                if (Character.isWhitespace(chars[i])) {
                    i++;
                    continue;
                }
                if (Character.isUpperCase(chars[i])) {
                    Annotation newAnn = new Annotation("sentence",
                            new Span(sentence.start() + chunkStart, sentence.start() + i),
                            new FeatureSet());
                    result.add(newAnn);
                    chunkStart = i;
                }
                afterDot = false;
            }
            i++;
        }
        if (chunkStart != chars.length) {
            Annotation newAnn = new Annotation("sentence", new Span(sentence.start()+chunkStart, sentence.start()+chars.length), new FeatureSet());
            result.add(newAnn);
        }
        return result;
    }

    public List<Annotation> splitSemicolon(List<Annotation> sentences, FuseDocument doc) {
        List<Annotation> result = new ArrayList<Annotation>();
        for (Annotation sentence : sentences) {
            if (doc.text(sentence.span()).length() <= MIN_SEMI_COLON_SPLIT_LENGTH) {
                result.add(sentence);
            }
            else {
                Vector<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
                int begin = sentence.start();
                int end = sentence.end();
                if (tokens == null) {
                    result.add(sentence);
                }
                else {
                    for (Annotation token : tokens) {
                        if (doc.text(token.span()).trim().equals(";") ||
                                doc.text(token.span()).trim().equals(":")) {
                            end = token.end();
                            if (inCitation(end)) continue;
                            if (sentence.end() - begin < MIN_SEMI_COLON_SPLIT_LENGTH) continue;
                            Annotation newSentence = new Annotation("sentence",
                                    new Span(begin, end),
                                    new FeatureSet());
                            result.add(newSentence);
                            begin = end;
                            end = sentence.end();
                        }
                    }
                    if (begin < end) {
                        Annotation newSentence = new Annotation("sentence",
                                new Span(begin, end),
                                new FeatureSet());
                        result.add(newSentence);
                    }
                }
            }
        }
        return  result;
    }

    public List<Annotation> forwardPeekMerge(List<Annotation> sentences, FuseDocument doc) {
        int start = 0, end = -1;
        int i = 0;
        int rbCount = 0, sbCount =0;
        List<Annotation> result = new ArrayList<Annotation>();
        char[] chars = doc.text().toCharArray();
        while (i < sentences.size()) {
            Annotation currSentence = sentences.get(i);
            if (end < 0) {
                start = currSentence.start();
                end = currSentence.end();
            }
            else {
                end = currSentence.end();
            }
            if (end - start > MAX_MERGED_SENT_LENGTH) {
                Annotation newAnn = new Annotation("sentence", new Span(start, end), new FeatureSet());
                result.add(newAnn);
                end = -1;
            }
            else {
                rbCount = 0;
                sbCount = 0;
                for (int j = start; j < end; j++) {
                    if (chars[j] == '(')
                        rbCount++;
                    if (chars[j] == ')')
                        rbCount--;
                    if (chars[j] == '[')
                        sbCount++;
                    if (chars[j] == ']')
                        sbCount--;
                }
                if (((rbCount <= 0) && (sbCount <= 0)) || i == sentences.size() - 1) {
                    Annotation newAnn = new Annotation("sentence", new Span(start, end), new FeatureSet());
                    result.add(newAnn);
                    end = -1;
                }
                else {
                    //System.out.println("debug");
                }
            }
            i++;
        }
        return result;
    }

    private boolean inCitation(int i) {
        for (Annotation citation : citationsInDoc) {
            if ((i > citation.start()) && (i < citation.end())) {
                return true;
            }
        }
        return false;
    }

    private List<Annotation> mergeCitation(List<Annotation> sentences, FuseDocument doc) {
        int start = 0;
        int i = 0;
        List<Annotation> result = new ArrayList<Annotation>();
        if (sentences.size() > 0) {
            start = sentences.get(0).start();
        }
        while (i < sentences.size()) {
            Annotation currSentence = sentences.get(i);

            if (!(inCitation(currSentence.end()))) {
                Annotation newAnn = new Annotation("sentence",
                        new Span(start, currSentence.end()),
                        new FeatureSet());
                result.add(newAnn);
                if (i < sentences.size()-1) {
                    start = sentences.get(i+1).start();
                }
            }
            i++;
        }
        return result;
    }

    public List<Annotation> etAlMerge(List<Annotation> sentences, FuseDocument doc) {
        int start = 0;
        int i = 0;
        List<Annotation> result = new ArrayList<Annotation>();
        if (sentences.size() > 0) {
            start = sentences.get(0).start();
        }
        while (i < sentences.size()) {
            Annotation currSentence = sentences.get(i);
            Vector<Annotation> tokens = doc.annotationsOfType("token", currSentence.span());

            if ((tokens == null) || (!endsWithEtAl(tokens, doc)) || i == sentences.size()-1
                    || sentences.get(i+1).start() != currSentence.end()) {
                Annotation newAnn = new Annotation("sentence", new Span(start, currSentence.end()), new FeatureSet());
                result.add(newAnn);
                if (i < sentences.size()-1) {
                    start = sentences.get(i+1).start();
                }
            }
            i++;
        }
        return result;
    }

    private boolean endsWithEtAl(Vector<Annotation> tokens, FuseDocument doc) {
        if ((tokens == null) || (tokens.size() < 3)) return false;
        return doc.text(tokens.get(tokens.size()-3)).trim().equals("et") &&
                doc.text(tokens.get(tokens.size()-2)).trim().equals("al") &&
                doc.text(tokens.get(tokens.size()-1)).trim().equals(".");

    }



}