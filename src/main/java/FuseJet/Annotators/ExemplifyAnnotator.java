package FuseJet.Annotators;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseEntitySpan;
import FuseJet.Models.FuseRelation;
import FuseJet.Utils.Console;
import Jet.Lisp.FeatureSet;
import Jet.Pat.PatternSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.Vector;

/**
 * User: yhe
 * Date: 7/26/12
 * Time: 12:14 PM
 */
public class ExemplifyAnnotator implements FuseJetAnnotator {

    private PatternSet exemplifyPatternSet;

    @Override
    public void initialize(String[] resourceInformation) {
        exemplifyPatternSet = Console.pc.getPatternSet(resourceInformation[0]);
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences != null) {
            for (Annotation sentence : sentences) {
                exemplifyPatternSet.apply(doc, sentence.span());
            }
        }
        Vector<Annotation> annotations = doc.annotationsOfType("exemplify-candidate");
        if (annotations == null) {
            return;
        }
        for (Annotation ann : annotations) {
            doc.removeAnnotation(ann);
            Span arg1Span = (Span)ann.get("arg1");
            arg1Span.setDocument(doc);
            FuseEntitySpan fuseArg1Span = new FuseEntitySpan(arg1Span);
            //System.err.println("Before trim():" +  fuseArg2Span);
            fuseArg1Span.trim();

            Span arg2Span = ((Annotation)ann.get("arg2")).span();
            arg2Span.setDocument(doc);
            FuseEntitySpan fuseArg2Span = new FuseEntitySpan(arg2Span);
            //System.err.println("Before trim():" +  fuseArg2Span);
            fuseArg2Span.trim();

            Span signalSpan = (Span)ann.get("textSignal");
            signalSpan.setDocument(doc);
            FuseEntitySpan fuseSignalSpan = new FuseEntitySpan(signalSpan);
            fuseSignalSpan.trim();

            if (fuseArg1Span.isValidJargon() && fuseArg2Span.isValidJargon()) {
                FeatureSet featureSet = new FeatureSet();
                FuseAnnotation annotation1 = new FuseAnnotation(
                        "fuse-entity", fuseArg1Span, featureSet,
                        FuseAnnotation.AnnotationCategory.JARGON,
                        FuseAnnotation.AnnotationType.NA);
                annotation1 = (FuseAnnotation)doc.addAnnotation(annotation1);
                FuseAnnotation annotation2 = new FuseAnnotation(
                        "fuse-entity", fuseArg2Span, featureSet,
                        FuseAnnotation.AnnotationCategory.JARGON,
                        FuseAnnotation.AnnotationType.NA);
                annotation2 = (FuseAnnotation)doc.addAnnotation(annotation2);
                FuseAnnotation signalEntity = new FuseAnnotation(
                        "fuse-entity", fuseSignalSpan, featureSet,
                        FuseAnnotation.AnnotationCategory.SIGNAL,
                        FuseAnnotation.AnnotationType.NA);
                signalEntity = (FuseAnnotation)doc.addAnnotation(signalEntity);

                FuseRelation relation =
                        new FuseRelation("", annotation1, annotation2, "", "",
                                FuseRelation.RelationType.EXEMPLIFY, signalEntity, "");
                doc.addRelation(relation);
            }
        }
    }
}