package FuseJet.Annotators;

import FuseJet.Models.*;
import FuseJet.Sentiment.JargonSentiment.JargonSentiment;
import FuseJet.Utils.Console;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: llinda
 * Date: 7/3/13
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class SentimentRelationLearningAnnotator implements FuseJetAnnotator{

    private JargonSentiment jargonSentiment = new JargonSentiment();
    private Map<String,Integer> counts=new HashMap<String,Integer>();

    @Override
    public void initialize(String[] resourceInformation) {
        jargonSentiment.loadModel(resourceInformation[0]);
    }


    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences= doc.annotationsOfType("sentence");
        if (sentences == null) return;

        for (Annotation sentence : sentences) {
            if (sentence == null) continue;
            Vector<Annotation> terms = doc.annotationsOfType("ENAMEX", sentence.span());
            if (terms == null) continue;

            Vector<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
            if (tokens == null) continue;
            String fullSentence = buildSentenceString(tokens);
            for (Annotation term: terms) {
                String sentimentOutcome = jargonSentiment.predict(formatSentenceToPredict(term, fullSentence));

                String id = "";
                if (term.get("id") != null) id = (String)term.get("id");

                /*The SentimentPredictionWriter will output sentences/terms while the other (CollapsedRelation) will output relations) */
                if (Console.getWriter().toString().contains("SentimentPrediction")) {
                    String termIdentifier = term.get("string") == null ? term.attributes().toString() : term.get("string").toString();
                    Annotation a = AnnotationFactory.createTripleAttributeAnnotation("OPINION", doc, term.start(), term.end(), "TERM", termIdentifier, "SENTENCE", fullSentence, "PREDICTION", sentimentOutcome);
                    doc.addAnnotation(a);
                }
                else {
                    FuseAnnotation fuseEntity = new FuseAnnotation("fuse-entity",
                            new FuseEntitySpan(term.span()),
                            new FeatureSet("bae_id", id),
                            FuseAnnotation.AnnotationCategory.ENAMEX,
                            FuseAnnotation.AnnotationType.NA);
                    fuseEntity = (FuseAnnotation)doc.addAnnotation(fuseEntity);

                    FuseRelation relation = new FuseRelation("",
                            FuseDocument.THIS_ARTICLE,
                            fuseEntity,
                            sentimentOutcome, "", FuseRelation.RelationType.OPINION, "", "");
                    doc.addRelation(relation);
                }

                if (counts.containsKey(sentimentOutcome)){
                    counts.put(sentimentOutcome, counts.get(sentimentOutcome)+1);
                }
                else {
                    counts.put(sentimentOutcome, 1);
                }
            }
        }
        System.out.println("\n");
        for(String key:counts.keySet()){
            System.out.println(key+ ": " + counts.get(key));
        }
    }

    private String buildSentenceString(Vector<Annotation> words){
        StringBuilder builder = new StringBuilder();
        for (Annotation word : words) {
            String currentWord = word.get("tokenString").toString();
            //to avoid having extra spaces around a punctuation mark
            if ((currentWord.length() == 1) && (!Character.isLetter(currentWord.charAt(0)))) {
                builder.append(currentWord);
            }
            else {
                builder.append(" ").append(currentWord);
            }
        }
        return builder.toString().trim();
    }

    //slightly modified JargonSentiment.readDatumFromString
    private String[] formatSentenceToPredict(Annotation term, String sentence){
        int start = term.span().start();
        int end = term.span().end();
        char[] chars = sentence.toCharArray();
        int i = 0;
        List<String> contextList = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        int afterEnd = -1;
        while (i < sentence.length()) {
            if (i == start) {
                if (sb.length() > 0) {
                    contextList.add(sb.toString());
                }
                int j = contextList.size();
                j = j > 3 ? 3 : j;
                while (j > 0) {
                    contextList.add("W-"+j+"="+contextList.get(contextList.size()-j));
                    //contextList.add("Stem-"+j+"="+stemmer.stem(contextList.get(contextList.size()-j)));
                    j--;
                }
                sb.setLength(0);
                afterEnd = 1;
                i = end;
                continue;
            }
            if (Character.isWhitespace(chars[i])) {
                if (sb.length() > 0) {
                    contextList.add(sb.toString());
                    if ((afterEnd > 0) && (afterEnd < 4)) {
                        contextList.add("W+"+afterEnd+"="+sb.toString());
                        //contextList.add("Stem+"+afterEnd+"="+stemmer.stem(sb.toString()));
                        afterEnd++;
                    }
                    sb.setLength(0);
                }
            }
            else {
                sb.append(chars[i]);
            }
            i++;
        }
        int j = 0;
        List<String> smallList = new ArrayList<String>();
        while (j < contextList.size()) {
            if (smallList.size() > 3) {
                break;
            }
            if (!contextList.get(j).startsWith("W-")) {
                smallList.add("P-" + smallList.size() + "=" + contextList.get(j).trim());
            }
            j++;
        }
        contextList.addAll(smallList);
        String[] context = contextList.toArray(new String[contextList.size()]);

        return context;
    }
}