package FuseJet.Annotators;

import AceJet.Gazetteer;
import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseEntitySpan;
import FuseJet.Models.FuseRelation;
import FuseJet.Utils.Console;
import Jet.Lisp.FeatureSet;
import Jet.Pat.PatternSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 8/22/12
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class OriginateAnnotator implements FuseJetAnnotator {
    private PatternSet originatePatternSet = null;

    @Override
    public void initialize(String[] resourceInformation) {
        originatePatternSet = Console.pc.getPatternSet(resourceInformation[0]);
    }

    @Override
    public void annotate(FuseDocument doc) {
        Span materialSpan = null;
        Vector<Annotation> sections = doc.annotationsOfType("doc_segment");
        if (sections == null) {
            sections = doc.annotationsOfType("section");
        }
        if (sections == null) {
            return;
        }
        for (Annotation section : sections) {
            if ((section.get("title") != null)
                    && (((String) section.get("title")).toLowerCase().contains("material"))) {
                materialSpan = section.span();
                break;
            }
        }

        if (materialSpan == null) return;

        Vector<Annotation> sentences = doc.annotationsOfType("sentence", materialSpan);

        if (sentences == null) return;

        for (Annotation sentence : sentences) {
            originatePatternSet.apply(doc, sentence.span());
        }

        Vector<Annotation> annotations = doc.annotationsOfType("originate-candidate");
        if (annotations == null) return;

        for (Annotation ann : annotations) {
            doc.removeAnnotation(ann);
            Span arg1Span = (Span) ann.get("arg1");
            arg1Span.setDocument(doc);
            FuseEntitySpan fuseArg1Span = new FuseEntitySpan(arg1Span);

            Span arg2Span = (Span) ann.get("arg2");
            arg2Span.setDocument(doc);
            FuseEntitySpan fuseArg2Span = new FuseEntitySpan(arg2Span);
            //System.err.println("Before trim():" +  fuseArg2Span);
            fuseArg2Span.trim();

            if (isPossibleManufacture(doc, fuseArg1Span)) {
                if (exampleStringValidatedWithGazette(doc.text(fuseArg1Span))) {
                    FeatureSet featureSet = new FeatureSet();
                    FuseAnnotation annotation1 = new FuseAnnotation(
                            "fuse-entity", fuseArg1Span, featureSet,
                            FuseAnnotation.AnnotationCategory.JARGON,
                            FuseAnnotation.AnnotationType.NA);
                    annotation1 = (FuseAnnotation) doc.addAnnotation(annotation1);
                    FuseAnnotation annotation2 = new FuseAnnotation(
                            "fuse-entity", fuseArg2Span, featureSet,
                            FuseAnnotation.AnnotationCategory.JARGON,
                            FuseAnnotation.AnnotationType.NA);
                    annotation2 = (FuseAnnotation) doc.addAnnotation(annotation2);
                    FuseRelation relation =
                            new FuseRelation("", annotation1, annotation2, "MANUFACTURE", "",
                                    FuseRelation.RelationType.ORIGINATE, "", "");
                    doc.addRelation(relation);
                }
            }
        }
    }

    private boolean isPossibleManufacture(FuseDocument doc, Span span) {
        Vector<Annotation> tokens = doc.annotationsOfType("token", span);
        if (tokens == null) return false;
        boolean hasComma = false;
        for (Annotation token : tokens) {
            if (!(Character.isUpperCase(doc.text().charAt(token.start())) ||
                    (doc.text().charAt(token.start()) == ',')))
                return false;
            if (doc.text().charAt(token.start()) == ',')
                hasComma = true;
        }
        return hasComma;
    }

    private boolean exampleStringValidatedWithGazette(String exampleString) {
        String[] parts = exampleString.split(",");
        Gazetteer gazetteer = Console.getGazetteer();
        boolean hasLoc = false;
        boolean allLoc = true;
        for (String part : parts) {
            if (gazetteer.isLocation(new String[]{part.trim()})) {
                hasLoc = true;
            }
            else {
                allLoc = false;
            }
        }
        return hasLoc && !allLoc;
    }
}
