package FuseJet.Annotators;

import FuseJet.Models.*;
import FuseJet.Utils.AnnotationStartComparator;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import com.google.common.collect.ImmutableSet;

import java.util.Collections;
import java.util.Set;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 8/21/12
 * Time: 10:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class CocitationAnnotator implements FuseJetAnnotator {

    private static Set<Character> charsBetweenCitations =  new ImmutableSet.Builder<Character>()
            .add(';')
            .add(',')
            .add('[')
            .add(']')
            .add('(')
            .add(')')
            .build();

    @Override
    public void initialize(String[] resourceInformation) {

    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> anns = doc.annotationsOfType("citation");
        if (anns == null) return;
        Collections.sort(anns, new AnnotationStartComparator());
        Annotation prevAnn = AnnotationFactory.createAnnotation("citation", null, 0, 0);
        for (Annotation ann : anns) {
            int i = prevAnn.end();
            while (i < ann.start()) {
                if (!isAllowableChar(doc.charAt(i))) {
                    break;
                }
                i++;
            }
            if (i == ann.start()) {
                String prevId = "";
                if (prevAnn.get("id") != null) prevId = (String)prevAnn.get("id");
                FuseAnnotation prevCitation = new FuseAnnotation("fuse-entity",
                        new FuseEntitySpan(prevAnn.span()),
                        new FeatureSet("bae_id", prevId),
                        FuseAnnotation.AnnotationCategory.ENAMEX,
                        FuseAnnotation.AnnotationType.CITATION);
                doc.addAnnotation(prevCitation);
                String thisId = "";
                if (prevAnn.get("id") != null) thisId = (String)ann.get("id");
                FuseAnnotation thisCitation = new FuseAnnotation("fuse-entity",
                        new FuseEntitySpan(ann.span()),
                        new FeatureSet("bae_id", thisId),
                        FuseAnnotation.AnnotationCategory.ENAMEX,
                        FuseAnnotation.AnnotationType.CITATION);
                doc.addAnnotation(thisCitation);
                FuseRelation relation = new FuseRelation("", prevCitation, thisCitation, "CO-CITATION", "", FuseRelation.RelationType.RELATED_WORK, "", "");
                doc.addRelation(relation);
            }
            prevAnn = ann;

        }

    }

    private boolean isAllowableChar(char c) {
        return Character.isWhitespace(c) || charsBetweenCitations.contains(c);
    }
}
