package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;

import java.util.List;

/**
 * User: yhe
 * Date: 6/24/13
 * Time: 5:17 PM
 */
public class OnomaAnnotator implements FuseJetAnnotator {
    @Override
    public void initialize(String[] resourceInformation) {
    }

    @Override
    public void annotate(FuseDocument doc) {
        List<Annotation> onomas = doc.annotationsOfType("onoma");
        if (onomas != null) {
            for (Annotation onoma : onomas) {
                boolean crossed = false;
                for (int i = onoma.start(); i < onoma.end(); i++) {
                    if (doc.annotationsAt(i, "ENAMEX") != null ||
                            doc.annotationsEndingAt(i, "ENAMEX") != null) {
                        crossed = true;
                        break;
                    }
                }
                if (!crossed) {
                    Annotation ann = new Annotation("ENAMEX", onoma.span(), new FeatureSet("TYPE", "GPE"));
                    doc.addAnnotation(ann);
                }
            }
        }
        //System.err.println("Done...");
    }
}
