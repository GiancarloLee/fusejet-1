package FuseJet.Annotators;

import FuseJet.Models.*;
import FuseJet.Utils.AnnotationStartComparator;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import SentimentAnalysis.SentimentAnalysisPredictor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 9/12/12
 * Time: 12:53 PM
 */
public class SentimentAnnotator implements FuseJetAnnotator {

    private SentimentAnalysisPredictor p;

    @Override
    public void initialize(String[] resourceInformation) {
        if (resourceInformation.length != 2) {
            System.err.println("Error initializing SentimentAnalysisPredictor: need 2 pieces of ResourceInformation");
        }
        String modelFile = resourceInformation[0];
        String dataDir = resourceInformation[1];
        p = SentimentAnalysisPredictor.getInstance(modelFile, dataDir);
    }

    private String[] tokensToStrings(Vector<Annotation> tokens, FuseDocument doc) {
        if (tokens == null) return null;
        List<String> words = new ArrayList<String>();
        for (Annotation token : tokens) {
            words.add(doc.text(token.span()).trim());
        }
        return words.toArray(new String[words.size()]);
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) return;
        for (Annotation sentence : sentences) {
            Vector<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
            if (tokens == null) continue;
            Collections.sort(tokens, new AnnotationStartComparator());
            Vector<Annotation> citations = doc.annotationsOfType("citation", sentence.span());
            if (citations != null) {
                String outcome = p.predict(tokensToStrings(tokens, doc)).substring(10);
                if (outcome.equals("POSITIVE") || outcome.equals("SIGNIFICANT")) {
                    for (Annotation citation : citations) {
                        String id = "";
                        if (citation.get("id") != null) id = (String)citation.get("id");
                        FuseAnnotation fuseCitation = new FuseAnnotation("fuse-entity",
                                new FuseEntitySpan(citation.span()),
                                new FeatureSet("bae_id", id),
                                FuseAnnotation.AnnotationCategory.ENAMEX,
                                FuseAnnotation.AnnotationType.CITATION);
                        fuseCitation = (FuseAnnotation)doc.addAnnotation(fuseCitation);
                        FuseRelation relation = new FuseRelation("",
                                FuseDocument.THIS_ARTICLE,
                                fuseCitation,
                                outcome, "", FuseRelation.RelationType.OPINION, "", "");
                        doc.addRelation(relation);
                    }
                }
            }

        }


    }
}
