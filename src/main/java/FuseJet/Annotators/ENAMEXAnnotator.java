package FuseJet.Annotators;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

/**
 * User: yhe
 * Date: 7/10/12
 * Time: 12:30 PM
 */
public class ENAMEXAnnotator implements FuseJetAnnotator {

    private static Set<String> annotationSet = new HashSet<String>();

    @Override
    public void initialize(String[] resourceInformation) {
        annotationSet.clear();
        for (String annotation : resourceInformation) {
            annotationSet.add(annotation);
        }
    }

    @Override
    public void annotate(FuseDocument doc) {
        for (String annotation : annotationSet) {
            Vector<Annotation> anns = doc.annotationsOfType(annotation);
            if (anns == null) continue;
            for (Annotation ann : anns) {
                //for (Span span : spans) {
                //    if ((span.start() <= ann.start()) && (span.end() >= ann.end())) {
                Annotation enamex = AnnotationFactory.createAnnotation("ENAMEX", doc, ann.start(), ann.end());
                enamex.put("type", annotation.toUpperCase());
                Enumeration keys = ann.attributes().keys();
                while (keys.hasMoreElements()) {
                    String key = (String)keys.nextElement();
                    Object val = ann.get(key);
                    if ((val != null ) && (val instanceof String)) {
                        if (val.equals("")) {
                            continue;
                        }
                        enamex.put(key, val);
                    }
                }
//
//                String val = (String)ann.get("id");
//                if (val != null) {
//                    enamex.put("id", val);
//                }
//                val = (String)ann.get("title");
//                if (val != null)  {
//                    enamex.put("title", val);
//                }

                Vector<Annotation> sameStartAnns = doc.annotationsAt(ann.start());
                boolean isDupulicate = false;
                if (sameStartAnns != null) {
                    for (Annotation a : sameStartAnns) {
                        if (a.type().equals("ENAMEX") && (a.end() == ann.end())) {
                            isDupulicate = true;
                            break;
                        }
                    }
                }
                if (!isDupulicate) {
                    doc.addAnnotation(enamex);
                }
                //break;
                // }
                //}
            }
        }
    }
}
