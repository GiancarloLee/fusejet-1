package FuseJet.Annotators;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseEntitySpan;
import FuseJet.Models.FuseRelation;
import FuseJet.Utils.Console;
import Jet.Lisp.FeatureSet;
import Jet.Pat.PatternSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.Vector;

/**
 * User: yhe
 * Date: 6/18/12
 * Time: 2:15 PM
 */
public class OpinionAnnotator implements FuseJetAnnotator {

    private PatternSet practicalPatternSet;

    public OpinionAnnotator() {

    }

    public OpinionAnnotator(PatternSet practicalPatternSet) {
        this.practicalPatternSet = practicalPatternSet;
    }

    public void setPracticalPatternSet(PatternSet practicalPatternSet) {
        this.practicalPatternSet = practicalPatternSet;
    }

    @Override
    public void initialize(String[] resourceInformation) {
        practicalPatternSet = Console.pc.getPatternSet(resourceInformation[0]);
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences != null) {
            for (Annotation sentence : sentences) {
                practicalPatternSet.apply(doc, sentence.span());
            }
        }
        Vector<Annotation> annotations = doc.annotationsOfType("opinion-candidate");
        if (annotations == null) {
            return;
        }
        System.err.println("Annotations found... ");
        for (Annotation ann : annotations) {
            doc.removeAnnotation(ann);
            Span arg2Span = (Span) ann.get("arg2");
            arg2Span.setDocument(doc);
            FuseEntitySpan fuseArg2Span = new FuseEntitySpan(arg2Span);
            //System.err.println("Before trim():" +  fuseArg2Span);
            fuseArg2Span.trim();
            //System.err.println("After trim():" + fuseArg2Span);
            Span signalSpan = (Span)ann.get("textSignal");
            signalSpan.setDocument(doc);
            FuseEntitySpan fuseSignalSpan = new FuseEntitySpan(signalSpan);
            fuseSignalSpan.trim();
            if (fuseArg2Span.isValidJargon()) {
                FeatureSet featureSet = new FeatureSet();
                FuseAnnotation annotation = new FuseAnnotation(
                        "fuse-entity", fuseArg2Span, featureSet,
                        FuseAnnotation.AnnotationCategory.JARGON,
                        FuseAnnotation.AnnotationType.NA);
                annotation = (FuseAnnotation)doc.addAnnotation(annotation);
                FuseAnnotation signalEntity = new FuseAnnotation(
                        "fuse-entity", fuseSignalSpan, featureSet,
                        FuseAnnotation.AnnotationCategory.SIGNAL,
                        FuseAnnotation.AnnotationType.NA);
                signalEntity = (FuseAnnotation)doc.addAnnotation(signalEntity);
                FuseRelation relation =
                        new FuseRelation("", FuseDocument.THIS_ARTICLE, annotation, "PRACTICAL", "",
                                FuseRelation.RelationType.OPINION, signalEntity, "");
                doc.addRelation(relation);
            }
        }
    }

}