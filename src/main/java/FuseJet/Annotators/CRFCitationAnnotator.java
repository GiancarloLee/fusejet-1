package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.CRF.Citation.CRFCitationTagger;
import Jet.CRF.Citation.ParagraphAnnotator;

/**
 * User: yhe
 * Date: 6/2/13
 * Time: 3:37 PM
 */
public class CRFCitationAnnotator implements FuseJetAnnotator {
    private CRFCitationTagger tagger;
    private ParagraphAnnotator paragraphAnnotator = new ParagraphAnnotator();
    @Override
    public void initialize(String[] resourceInformation) {
        try {
            tagger = new CRFCitationTagger(resourceInformation[0]);
        }
        catch (Exception e) {
            System.err.println("Error initializing ");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @Override
    public void annotate(FuseDocument doc) {
        paragraphAnnotator.annotate(doc);
        tagger.annotate(doc);
    }
}
