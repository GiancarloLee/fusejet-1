package FuseJet.Annotators;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import FuseJet.Utils.AnnotationStartComparator;
import FuseJet.Utils.FuseUtils;
import Jet.Tipster.Annotation;
import org.arabidopsis.ahocorasick.AhoCorasick;
import org.arabidopsis.ahocorasick.SearchResult;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: yhe
 * Date: 8/19/12
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class DictionaryAnnotator implements FuseJetAnnotator {

    protected AhoCorasick tree = new AhoCorasick();

    protected boolean[] coveredIndices;

    @Override
    public void initialize(String[] resourceInformation) {
        tree = new AhoCorasick();
        try {
            String dictFile = resourceInformation[0];
            System.err.print ("Loading dictionary data... ");
            BufferedReader r = new BufferedReader(new FileReader(dictFile));
            String line = null;
            while ((line = r.readLine()) != null) {
                String[] parts = line.split("\\|\\|\\|");
                DictionaryItem item = new DictionaryItem();
                item.setWord(parts[0].trim());
                if (parts.length > 1) {
                    item.setType(parts[1].trim());
                }
                if (parts.length > 2) {
                    item.setSubtype(parts[2].trim());
                }
//                if (item.word.trim().toLowerCase().equals("rnai")) {
//                    System.err.println("adding rnai to dictionary?");
//                }
                addItemToTree(item);
            }
            r.close();
            System.err.println("done.");
            System.err.print("Preparing dictionary... ");
            tree.prepare();
            System.err.println("done.");
        }
        catch (IOException e) {
            System.err.println("Error: unable to open resource file for DictionaryAnnotator");
            System.exit(-1);
        }
    }

    protected void addItemToTree(DictionaryItem item) {
        tree.add(item.getWord().getBytes(), item);
    }

    @Override
    public void annotate(FuseDocument doc) {
        coveredIndices = new boolean[doc.text().length()];
        Vector<Annotation> anns = doc.annotationsOfType("orgdict");
        if (anns != null) {
            for (Annotation ann : anns) {
                for (int i = ann.start(); i < ann.end(); i++) {
                    coveredIndices[i] = true;
                }
            }
        }
        anns = doc.annotationsOfType("term");
        if (anns != null) {
            for (Annotation ann : anns) {
                for (int i = ann.start(); i < ann.end(); i++) {
                    coveredIndices[i] = true;
                }
            }
        }
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) return;
        Collections.sort(sentences, new AnnotationStartComparator());
        for (Annotation sentence : sentences) {
           annotateSpan(doc, sentence);
        }
    }

    public void annotateSpan(FuseDocument doc, Annotation sentence) {
        Map<Integer, Integer> tokeniStart = new HashMap<Integer, Integer>();
        Map<Integer, Integer> tokeniEnd = new HashMap<Integer, Integer>();

        Vector<Annotation> tokens = doc.annotationsOfType("token", sentence.span());
        if (tokens == null) return;
        Collections.sort(tokens, new AnnotationStartComparator());

        // Construct input string for Aho-Corasick tree
        String inputString = buildInputString(doc, tokens, tokeniStart, tokeniEnd);
//        if (inputString.contains("RNAi")) {
//            System.err.println("RNAi in input");
//        }
        Iterator iter = tree.search(inputString.getBytes());
        List termsThatHit = new ArrayList();
        while (iter.hasNext()) {
            SearchResult searchResult = (SearchResult)iter.next();
            termsThatHit.addAll(searchResult.getOutputs());
        }
        for (Object termThatHit : termsThatHit) {
            DictionaryItem term = (DictionaryItem)termThatHit;
//            if (term.word.trim().toLowerCase().equals("rnai")) {
//                System.err.println("rnai in dictionary?");
//            }
            int iStart = inputString.indexOf(term.getWord());
            int iEnd = iStart + term.getWord().length();
            if (iStart > -1) {
                Integer tokenStart = tokeniStart.get(iStart);
                Integer tokenEnd = tokeniEnd.get(iStart + term.getWord().length());
                if ((tokenStart != null) && (tokenEnd != null)) {
                    Annotation ann = AnnotationFactory.createSingleAttributeAnnotation(term.getType(),
                            doc,
                            tokens.get(tokenStart).start(),
                            tokens.get(tokenEnd).end(),
                            "subtype",
                            term.getSubtype());
//                    if (doc.text(ann.span()).trim().equals("their")) {
//                        System.err.println("their to be annotated");
//                    }
                    addNonConflictingAnnotation(doc, ann);
                    //System.err.println("Added annotation:" + ann);
                    //System.err.println("Sentence:" + doc.text(sentence.span()).trim());
                    //System.err.println("Term:" + doc.text(ann.span()).trim());
                }
            }
        }
    }

    private void addNonConflictingAnnotation(FuseDocument doc, Annotation ann) {
        for (int i = ann.start(); i < ann.end(); i++) {
            if (coveredIndices[i]) {
                return;
            }
        }
        doc.addAnnotation(ann);
        for (int i = ann.start(); i < ann.end(); i++) {
            coveredIndices[i] = true;
        }
    }

    protected String buildInputString(FuseDocument doc, Vector<Annotation> tokens, Map<Integer, Integer> tokeniStart, Map<Integer, Integer> tokeniEnd) {
        StringBuilder input = new StringBuilder();
        for (int i = 0; i < tokens.size(); i++) {
            Annotation token = tokens.get(i);
            tokeniStart.put(input.length(), i);
            input.append(doc.text(token.span()).replaceAll("\\s+", " "));
            tokeniEnd.put(input.length(), i);
            input.append(" ");
        }
        return input.toString();
    }

    public class DictionaryItem {
        private String word;
        private String type = "orgdict";
        private String subtype = "";

        public String getSubtype() {
            return subtype;
        }

        public void setSubtype(String subtype) {
            this.subtype = subtype;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }
    }
}
