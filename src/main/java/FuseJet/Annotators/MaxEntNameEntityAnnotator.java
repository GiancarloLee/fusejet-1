package FuseJet.Annotators;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import Jet.Chunk.MENameTagger;
import Jet.Chunk.MaxEntNE;
import Jet.Lex.Lexicon;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.io.IOException;
import java.util.Vector;

/**
 * User: yhe
 * Date: 6/26/12
 * Time: 5:22 PM
 */
public class MaxEntNameEntityAnnotator implements FuseJetAnnotator {

    private static MENameTagger nameTagger = null;

    @Override
    public void initialize(String[] resourceInformation) {
        // ResourceLocations=modelfile;wordclusterfile
        nameTagger = new MENameTagger();

//        try {
//            MaxEntNE.loadWordClusters(resourceInformation[1]);
//        } catch (IOException e) {
//            System.err.println("Error reading MENameTagger word cluster file: " + resourceInformation[0]);
//        }

        try {
            nameTagger.load(resourceInformation[0]);
        } catch (IOException e) {
            System.err.println("Error reading MENameTagger model file: " + resourceInformation[1]);
        }
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> anns = doc.annotationsOfType("ENAMEX");
        if (anns != null) {
            for (Annotation ann : anns) {
                if ((ann.get("type") != null) && (ann.get("type").equals("CITATION") ||
                        ann.get("type").equals("ORGDICT") ||
                        ann.get("type").equals("TERM"))) {
                    Annotation isExactName = AnnotationFactory.createSingleAttributeAnnotation("isExactName",
                            doc,
                            ann.start(),
                            ann.end(),
                            "type",
                            "other");
                    doc.addAnnotation(isExactName);
                }
            }
        }

        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences != null) {
            for (Annotation sentence : sentences)  {
                Lexicon.annotateWithDefinitions(doc, sentence.start(), sentence.end());

                // for (Span span : spans) {
                //    if ((span.start() <= sentence.start()) && (span.end() >= sentence.end())) {
                        nameTagger.tag(doc, sentence.span());
                //    }
                //    break;
                //}
            }
        }

    }
}
