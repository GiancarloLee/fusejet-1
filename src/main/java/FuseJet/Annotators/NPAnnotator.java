package FuseJet.Annotators;

import FuseJet.Models.AnnotationFactory;
import FuseJet.Models.FuseDocument;
import FuseJet.Chunker.Chunker;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.Vector;

/**
 * User: yhe
 * Date: 7/17/12
 * Time: 2:55 PM
 */
public class NPAnnotator implements FuseJetAnnotator {
    private static Chunker chunker = new Chunker();

    @Override
    public void initialize(String[] resourceInformation) {
        chunker.loadModel(resourceInformation[0]);
    }

    @Override
    public void annotate(FuseDocument doc) {
        Vector<Annotation> sentences = doc.annotationsOfType("sentence");
        if (sentences == null) return;
        for (Annotation sentence : sentences) {
            chunker.chunk(doc, sentence.span());
        }
        Vector<Annotation> ngs = doc.annotationsOfType("ng");
        if (ngs != null) {
            for (Annotation ng : ngs) {
                Annotation np = AnnotationFactory
                        .createSingleAttributeAnnotation("constit", doc, ng.start(), ng.end(), "cat", "np");
                doc.addAnnotation(np);
            }
        }
    }
}
