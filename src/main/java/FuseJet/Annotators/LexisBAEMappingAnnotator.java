package FuseJet.Annotators;

import FuseJet.Models.FuseDocument;
import Jet.Lisp.FeatureSet;
import Jet.Tipster.Annotation;
import Jet.Tipster.Span;

import java.util.*;

/**
 * User: yhe
 * Date: 3/2/13
 * Time: 4:31 PM
 */
public class LexisBAEMappingAnnotator implements FuseJetAnnotator {

    private Map<String, String> nameMap = new HashMap<String, String>();
    private Map<String, String> tagsToRename = new HashMap<String, String>();
    private Set<String> nonTextTags = new HashSet<String>();
    private Set<String> textCandidateTags = new HashSet<String>();
    private Set<String> textTags = new HashSet<String>();

    public LexisBAEMappingAnnotator() {
        nameMap.put("govt-interest", "Government Interest");
        //nameMap.put("abstract", "Abstract");
        nameMap.put("description", "Description");
        nameMap.put("claims", "Claims");
        nameMap.put("claim", "Claim");
        //nameMap.put("patent-citation", "Patent Citation");
        nameMap.put("description", "Description");
        //nameMap.put("description-of-drawings", "Description of Drawings");

        tagsToRename.put("section", "classification-section");
        tagsToRename.put("text", "classification-text");
        tagsToRename.put("citation", "patent-citation");
        tagsToRename.put("invention-title", "title");

        nonTextTags.add("table");
        nonTextTags.add("maths");
        nonTextTags.add("non-text");

        textCandidateTags.add("section");
        textCandidateTags.add("footnote");

        textTags.add("abstract");
        textTags.add("title");
    }

    @Override
    public void initialize(String[] resourceInformation) {

    }

    @Override
    public void annotate(FuseDocument doc) {
        //System.err.println("Converting LexisNexus to BAE...");
        renameTags(doc);
        for (String tag : nameMap.keySet()) {
            mapTag(tag, doc);
        }
        Vector<Annotation> sections = doc.annotationsOfType("footnote");
        if (sections != null) {
            for (Annotation section : sections) {
                Annotation ann = new Annotation("structure", section.span(),
                        new FeatureSet("TYPE", "FOOTNOTE"));
                doc.addAnnotation(ann);
            }
        }
        addSectionFromHeadings(doc, "description");
        //addSectionFromHeadings(doc, "description-of-drawings");
        findNonTextRegion("[in-line-formulae]", "[/in-line-formulae]", doc);
        addStructures(doc);
        doc.splitAndTokenize();
        //System.out.println("test");
    }

    private void addSectionFromHeadings(FuseDocument doc, String tag) {
        Vector<Annotation> descriptions = doc.annotationsOfType(tag);
        if (descriptions != null) {
            for (Annotation description : descriptions) {
                int descriptionEnd = description.end();
                Vector<Annotation> headings = doc.annotationsOfType("heading", description.span());
                if (headings != null) {
                    for (int i = 0; i < headings.size(); i++) {
                        int begin = headings.get(i).start();
                        String title = doc.text(headings.get(i).span()).trim();
                        int end;
                        if (i < headings.size() - 1) {
                            end = headings.get(i + 1).start();
                        } else {
                            end = descriptionEnd;
                        }
                        Annotation ann = new Annotation("section",
                                new Span(begin, end),
                                new FeatureSet("title", title));
                        doc.addAnnotation(ann);
                    }
                }
            }
        }
    }

    private void mapTag(String tag, FuseDocument doc) {
        Vector<Annotation> sections = doc.annotationsOfType(tag);
        if (sections != null) {
            for (Annotation section : sections) {
                Annotation ann = new Annotation("section", section.span(),
                        new FeatureSet("title", nameMap.get(tag)));
                doc.addAnnotation(ann);
            }
        }
    }

    private void renameTags(FuseDocument doc) {
        for (String tag : tagsToRename.keySet()) {
            Vector<Annotation> sections = doc.annotationsOfType(tag);
            if (sections != null) {
                for (Annotation section : sections) {
                    String newType = tagsToRename.get(tag);
                    Annotation newAnn = new Annotation(newType, section.span(), section.attributes());
                    doc.removeAnnotation(section);
                    doc.addAnnotation(newAnn);
                }
            }
        }
    }

    private void findNonTextRegion(String beginText, String endText, FuseDocument doc) {
        String text = doc.text();
        int begin = text.indexOf(beginText);
        while (begin > 0) {
            int end = text.indexOf(endText, begin + 1) + endText.length();
            if (end < 0) break;
            Annotation ann = new Annotation("non-text", new Span(begin, end), new FeatureSet());
            doc.addAnnotation(ann);
            begin = text.indexOf(beginText, end + 1);
        }
    }

    private void addStructures(FuseDocument doc) {
        for (String tag : textTags) {
            Vector<Annotation> anns = doc.annotationsOfType(tag);
            if (anns == null) continue;
            for (Annotation ann : anns) {
                Annotation newAnn = new Annotation("structure",
                        ann.span(),
                        new FeatureSet("TYPE", tag.toUpperCase()));
                doc.addAnnotation(newAnn);
                newAnn = new Annotation("structure",
                        ann.span(),
                        new FeatureSet("TYPE", "TEXT"));
                doc.addAnnotation(newAnn);
                newAnn = new Annotation("text",
                        ann.span(),
                        new FeatureSet("TYPE", "TEXT"));
                doc.addAnnotation(newAnn);
            }
        }
        boolean[] boolArr = new boolean[doc.text().length() + 1];
        for (String tag : textCandidateTags) {
            Vector<Annotation> anns = doc.annotationsOfType(tag);
            if (anns == null) continue;
            for (Annotation ann : anns) {
                for (int i = ann.start(); i < ann.end(); i++) {
                    boolArr[i] = true;
                }
            }
        }
        for (String tag : nonTextTags) {
            Vector<Annotation> anns = doc.annotationsOfType(tag);
            if (anns == null) continue;
            for (Annotation ann : anns) {
                for (int i = ann.start(); i < ann.end(); i++) {
                    boolArr[i] = false;
                }
            }
        }
        int begin = -1;
        int allBegin = -1;
        int allEnd = -1;
        for (int i = 0; i < boolArr.length; i++) {
            if ((begin >= 0) && !boolArr[i]) {
                Annotation newAnn = new Annotation("structure",
                        new Span(begin, i),
                        new FeatureSet("TYPE", "TEXT"));
                if (allBegin < 0) allBegin = begin;
                allEnd = i;
                doc.addAnnotation(newAnn);
                newAnn = new Annotation("text",
                        newAnn.span(),
                        new FeatureSet("TYPE", "TEXT"));
                doc.addAnnotation(newAnn);
                begin = -1;
            } else {
                if (begin >= 0) {
                    Vector<Annotation> headings = doc.annotationsEndingAt(i, "heading");
                    Vector<Annotation> lis = doc.annotationsEndingAt(i, "li");
                    Vector<Annotation> liStarts = doc.annotationsAt(i, "li");
                    Vector<Annotation> cs = doc.annotationsEndingAt(i, "claim-text");
                    Vector<Annotation> cStarts = doc.annotationsAt(i, "claim-text");
                    Vector<Annotation> ps = doc.annotationsEndingAt(i, "p");
                    if ((headings != null) || (lis != null) || (liStarts != null)
                            || (cs != null) || (cStarts != null) || ps != null) {
                        Annotation newAnn = new Annotation("structure",
                                new Span(begin, i),
                                new FeatureSet("TYPE", "TEXT"));
                        if (allBegin < 0) allBegin = begin;
                        allEnd = i;
                        doc.addAnnotation(newAnn);
                        newAnn = new Annotation("text",
                                newAnn.span(),
                                new FeatureSet("TYPE", "TEXT"));
                        doc.addAnnotation(newAnn);
                        begin = i;
                    }
                }
                if ((begin < 0) && boolArr[i]) {
                    begin = i;
                }
            }
        }
        if ((allBegin >= 0) && (allEnd >= 0)) {
            Annotation newAnn = new Annotation("structure",
                    new Span(allBegin, allEnd),
                    new FeatureSet("TYPE", "TEXT_CHUNK"));
            doc.addAnnotation(newAnn);
        }
    }

}
