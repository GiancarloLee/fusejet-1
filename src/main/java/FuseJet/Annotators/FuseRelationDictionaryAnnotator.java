package FuseJet.Annotators;

import FuseJet.Models.FuseAnnotation;
import FuseJet.Models.FuseDocument;
import FuseJet.Models.FuseEntitySpan;
import FuseJet.Models.FuseRelation;
import FuseJet.Utils.FuseUtils;
import Jet.Lisp.FeatureSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: yhe
 * Date: 11/13/12
 * Time: 11:14 AM
 */
public class FuseRelationDictionaryAnnotator implements FuseJetAnnotator {

    private List<RelationDictionaryItem> relations = new ArrayList<RelationDictionaryItem>();
    private ArrayList<String> jargon = new ArrayList<String>();
    private String jargonRegEx="";
    private ArrayList<String> people = new ArrayList<String>();
    private String peopleRegEx="";
    private ArrayList<String> organizations = new ArrayList<String>();
    private String organizationsRegEx="";
    private ArrayList<String> generalJargon = new ArrayList<String>();
    private String generalJargonRegEx="";
    private ArrayList<String> stopList = new ArrayList<String>(Arrays.asList("such","several","one","most","more","many","less","few","enough","both","all","your","those","this","these","their","the","that","some","our","no","neither","my","its","his","her","every","either","each","any","another","an","abaft","abeam","aboard","about","above","abreast of","according to","across","across from","afore","after","against","ahead of","along","along with","amid","amidst","among amongst","apart from","around","as a result of","as far as","as for","as from","aside from","as of","as per","as soon as","as to","at","atop","at the expense of","at the hands of","at variance with","away from","bar","barring","because of","before","behind","below","beneath","beside","besides","between","beyond","but","but for","by dint of","by means of","by virtue of","by way of","close to","contrary to","despite","devoid of","down","due to","during except","except for","exclusive of","following","for","for sake of","for the sake of","for want of","from want of","in accordance with","in addition to","in aid of","in back of","in case of","in charge of","in common with","in comparison with","in compliance with","in conformity with","in connection with","in consequence of","in contact with","in exchange for","in face of","in favor of","in front of","in lieu of","in light of","in line with","in need of","in place of","in process of","in quest of","in reference to","in regard to","in relation to","in respect of","in respect to","in return for","in search of","inside","inside of","in spite of","instead of","in terms of","in the face of","in the light of","in the process of","into","in view of","irrespective of","like","minus","near","nearer","nearer to","near to","next to","notwithstanding","off","off of","on","on account of","on behalf of","on grounds of","on pain of","on the grounds of","on the matter of","on the part of","on the strength of","onto","on top of","out","out of","outside","outside of","over","'ver","owing to","past","pending","per","plus","preliminary to","preparatory to","previous to","prior to","pursuant to","regardless of","short of","since","subsequent to","such as","thanks to","through","throughout","till","'til","to","together with","toward","towards","under","unlike","until","unto","up","up against","upon","up to","upwards of","versus","void of","vs","within","without","with reference to","with regard to","with respect to","with the exception of","worth",":",","," "));
    private String stopListRegEx = "";
    private ArrayList<String> exemplifySignals = new ArrayList<String>(Arrays.asList("is", "other", "include",  "was", "namely", "designated", "named","type","member","especially","known as","referred to","termed","class"));
    private String exemplifyListSignalsRegEx ="";
    private ArrayList<String> exemplifyListSignals = new ArrayList<String>(Arrays.asList("called","such as", "including", "are", "e.g.", "eg", "like", "include", "i.e.", "ie", "were","for example", "namely","including", "included"));
    private String exemplifySignalsRegEx = "";
    private ArrayList<String> practicalSignals = new ArrayList<String>(Arrays.asList("via","using","used","performed","use","based on","applied","carried out","useful","employed","subjected to","effective","efficient","utilized"));
    private String practicalSignalsRegEx = "";
    private ArrayList<String> standardSignals = new ArrayList<String>(Arrays.asList("standard","widely used","commonly used","popular"));
    private String standardSignalsRegEx = "";
    private ArrayList<String> significantSignals = new ArrayList<String>(Arrays.asList("important","essential","importance","major","significant","critical","promising","crucial","customary"));
    private String significantSignalsRegEx = "";
    private ArrayList<String> supplySignals = new ArrayList<String>(Arrays.asList("(","from","obtained","obtained from","provided","provided by", "purchased","purchased from"));
    private String supplySignalsRegEx = "";
    private ArrayList<String>endCharacters = new ArrayList<String>(Arrays.asList(" ",",",".","\n","-","(",")","[","]","\'","’","?","\"",":",";","!","_","=","—","`"));
    private String endCharactersRegEx ="";
    private ArrayList<String> morph = new ArrayList<String>();
    private HashMap<String, String[]> prepDict = new HashMap<String, String[]>();
    private FuseDocument doc = null;


    @Override
    public void initialize(String[] resourceInformation) {
        try {
            String relationDictFile = resourceInformation[0];
            String jargonDictFile = resourceInformation[1];
            String peopleDictFile = resourceInformation[2];
            String orgDictFile = resourceInformation[3];
            String generalJargonDictFile = resourceInformation[4];
            String prepDictFile = resourceInformation[5];
            String fullMorphFile = resourceInformation[6];
            System.err.print ("Loading dictionary data... ");
            String[] lines = FuseUtils.readLines(relationDictFile);
            relations.clear();
            for (String line : lines) {
                relations.add(new RelationDictionaryItem(line));
            }
            populateDict(jargonDictFile, jargon);
            populateDict(peopleDictFile, people);
            populateDict(orgDictFile, organizations);
            populateDict(generalJargonDictFile, generalJargon);
            populateDict(fullMorphFile, morph);
            populatePrepDict(prepDictFile);
            jargonRegEx=buildRegEx(jargon,"{1}");
            peopleRegEx=buildRegEx(people,"{1}");
            organizationsRegEx= buildRegEx(organizations,"{1}");
            generalJargonRegEx=buildRegEx(generalJargon,"{1}");
            stopListRegEx=buildRegEx(stopList,"*");
            endCharactersRegEx=buildRegEx(endCharacters,"{1}");
            exemplifySignalsRegEx=buildRegEx(exemplifySignals,"{1}");
            exemplifyListSignalsRegEx=buildRegEx(exemplifyListSignals,"{1}");
            practicalSignalsRegEx =buildRegEx(practicalSignals,"{1}");
            standardSignalsRegEx=buildRegEx(standardSignals,"{1}");
            significantSignalsRegEx=buildRegEx(significantSignals,"{1}");
            supplySignalsRegEx=buildRegEx(supplySignals,"{1}");

        }
        catch (IOException e) {
            System.err.println("Error: unable to open resource file for DictionaryAnnotator");
            System.exit(-1);
        }
    }

    private void populatePrepDict(String prepDictFile) throws IOException {
        String[] lines = FuseUtils.readLines(prepDictFile);
        for (String line : lines) {
            String prepPart = line.split("\\|")[2];
            String[] preps = prepPart.split("\" \"");
            String entry = "";
            boolean isSig =false;
            for (int i = 0; i<preps.length; i++){
                if (practicalSignals.contains(preps[i])){
                    entry = line.substring(1, line.indexOf("\"",1));
                    String[] oldEntry = (prepDict.get(preps[i])!=null) ?  prepDict.get(preps[i]) : new String[]{};
                    String[] currentEntry = new String[1 + oldEntry.length];
                    for (int j =0;j<oldEntry.length;j++){
                        currentEntry[j]=oldEntry[j];
                    }
                    currentEntry[currentEntry.length-1]=entry;
                    prepDict.put(preps[i],currentEntry);

                }
            }
        }
    }

    private String buildRegEx(ArrayList<String> list, String tail){
        String regEx = "(";
        for (String item : list){
            item = replaceReserved(item);
            regEx += item + "|";
        }
        return regEx.substring(0, regEx.length()-1)+")"+tail;
    }

    private String replaceReserved(String string) {
        string=string.replace("\\", "\\\\");
        string=string.replace("(", "\\(");
        string=string.replace(")", "\\)");
        string=string.replace("[", "\\[");
        string=string.replace("]", "\\]");
        string=string.replace("^", "\\^");
        string=string.replace("$", "\\$");
        string=string.replace(".", "\\.");
        string=string.replace("|", "\\|");
        string=string.replace("?", "\\?");
        string=string.replace("*", "\\*");
        return string;
    }

    private void populateDict(String dictFile, ArrayList<String> dict) throws IOException {
        String[] lines = FuseUtils.readLines(dictFile);
        dict.clear();
        for (String line : lines) {
            if(line.length()>1 && !stopList.contains(line))
                dict.add(line);
        }
    }

   @Override
   public void annotate(FuseDocument doc) {
       this.doc = doc;
       String text = doc.text();
       matchRelations(text);
       findExemplifies(text);
       findListExemplifies(text);
       findSupplys(text);
       findDiscovers(text);
       findOpinions(text,"PRACTICAL",practicalSignalsRegEx);
       findOpinions(text,"STANDARD",standardSignalsRegEx);
       findOpinions(text,"SIGNIFICANT",significantSignalsRegEx);
    }


   private void matchRelations(String text){
        for (RelationDictionaryItem item : relations) {
            try{
                int offset = 0-item.text.length();
                do{
                    offset = text.indexOf(item.text,offset+item.text.length());
                    if (offset != -1){
                        addRelation(item,offset+item.arg1Start,offset+item.arg2Start,offset+item.signalStart,"kb");
                    }
                }while (offset != -1);
            }catch(Exception e ){
                System.out.println(item.text);
            }
        }
   }

    private void findExemplifies(String text){
        Pattern exemplifyPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+stopListRegEx+exemplifySignalsRegEx+stopListRegEx+generalJargonRegEx+endCharactersRegEx);
        Pattern jargonPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+endCharactersRegEx);
        Pattern signalPattern = Pattern.compile(exemplifySignalsRegEx+endCharactersRegEx);
        Pattern generalJargonPattern =Pattern.compile(endCharactersRegEx+generalJargonRegEx+endCharactersRegEx);
        Matcher exemplifyMatcher = exemplifyPattern.matcher(text);
        Matcher arg1Matcher = jargonPattern.matcher(text);
        Matcher arg2Matcher = generalJargonPattern.matcher(text);
        Matcher signalMatcher = signalPattern.matcher(text);
        try{
            while(exemplifyMatcher.find()){
                arg1Matcher.find(exemplifyMatcher.start());
                int arg1Start = arg1Matcher.start()+1;
                int arg1End = arg1Matcher.end()-1;
                signalMatcher.find(arg1End);
                int sigStart = signalMatcher.start();
                int sigEnd = signalMatcher.end()-1;
                arg2Matcher.find(sigEnd);
                int arg2Start = arg2Matcher.start()+1;
                int arg2End = arg2Matcher.end()-1;
                String arg2 = text.substring(arg2Start,arg2End);
                String signalText = text.substring(sigStart,sigEnd);
                String grammaticalSignal = getGrammaticalSignal(text.substring(sigStart,sigEnd));
                if (!jargon.contains(text.substring(arg1Start,arg2End)) && arg2End <= exemplifyMatcher.end()+1 && !arg2.endsWith("en") && !arg2.endsWith("ed") && !arg2.endsWith("ing"))
                    addRelation("EXEMPLIFY","",arg1Start,arg1End,"JARGON","NA",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","exemplify");
            }
        }catch (Exception e){
            System.out.println(e.getStackTrace());
        }
    }

    private void findListExemplifies(String text){
        Pattern exemplifyPattern = Pattern.compile(endCharactersRegEx+generalJargonRegEx+stopListRegEx+exemplifyListSignalsRegEx+stopListRegEx+jargonRegEx+endCharactersRegEx);
        Pattern jargonPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+endCharactersRegEx);
        Pattern signalPattern = Pattern.compile(exemplifyListSignalsRegEx+endCharactersRegEx);
        Pattern generalJargonPattern =Pattern.compile(endCharactersRegEx+generalJargonRegEx+endCharactersRegEx);
        Pattern nextJargonPattern = Pattern.compile(stopListRegEx +"(\\(.*\\))?"+stopListRegEx + "(,){1}" +stopListRegEx + endCharactersRegEx + jargonRegEx +endCharactersRegEx);
        Pattern endJargonPattern = Pattern.compile(stopListRegEx +"(\\(.*\\))?"+stopListRegEx + "(and|or){1}" +stopListRegEx + endCharactersRegEx + jargonRegEx +endCharactersRegEx);
        Matcher endJargonMatcher = endJargonPattern.matcher(text);
        Matcher nextJargonMatcher = nextJargonPattern.matcher(text);
        Matcher exemplifyMatcher = exemplifyPattern.matcher(text);
        Matcher arg1Matcher = jargonPattern.matcher(text);
        Matcher arg2Matcher = generalJargonPattern.matcher(text);
        Matcher signalMatcher = signalPattern.matcher(text);
        try{
            while(exemplifyMatcher.find()){
                arg2Matcher.find(exemplifyMatcher.start());
                int arg2Start = arg2Matcher.start()+1;
                int arg2End = arg2Matcher.end()-1;
                signalMatcher.find(arg2End);
                int sigStart = signalMatcher.start();
                int sigEnd = signalMatcher.end()-1;
                arg1Matcher.find(sigEnd);
                int arg1Start = arg1Matcher.start()+1;
                int arg1End = arg1Matcher.end()-1;
                String grammaticalSignal = getGrammaticalSignal(text.substring(sigStart,sigEnd));
                addRelation("EXEMPLIFY","",arg1Start,arg1End,"JARGON","NA",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","exemplifyList");
                while(nextJargonMatcher.find(arg1End)){
                    if (nextJargonMatcher.start() == arg1End){
                        arg1Matcher.find(nextJargonMatcher.start());
                        arg1Start = arg1Matcher.start()+1;
                        arg1End = arg1Matcher.end()-1;
                        if (text.substring(arg1End,arg1End+1).equalsIgnoreCase(")")){
                            arg1Matcher.find(arg1End);
                            arg1Start = arg1Matcher.start()+1;
                            arg1End = arg1Matcher.end()-1;
                        }
                        addRelation("EXEMPLIFY","",arg1Start,arg1End,"JARGON","NA",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","exemplifyList");
                    }else{break;}
                }
                endJargonMatcher.find(arg1End);
                if (endJargonMatcher.start() == arg1End){
                    arg1Matcher.find(endJargonMatcher.start());
                    arg1Start = arg1Matcher.start()+1;
                    arg1End = arg1Matcher.end()-1;
                    if (text.substring(arg1End,arg1End+1).equalsIgnoreCase(")")){
                        arg1Matcher.find(arg1End);
                        arg1Start = arg1Matcher.start()+1;
                        arg1End = arg1Matcher.end()-1;
                    }
                    addRelation("EXEMPLIFY","",arg1Start,arg1End,"JARGON","NA",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","exemplify");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findSupplys(String text){
        Pattern originatePattern = Pattern.compile(endCharactersRegEx+jargonRegEx+stopListRegEx+supplySignalsRegEx+stopListRegEx+organizationsRegEx+endCharactersRegEx);
        Pattern jargonPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+endCharactersRegEx);
        Pattern signalPattern = Pattern.compile(supplySignalsRegEx);
        Pattern organizationPattern = Pattern.compile(endCharactersRegEx+organizationsRegEx+endCharactersRegEx);
        Matcher originateMatcher = originatePattern.matcher(text);
        Matcher arg2Matcher = jargonPattern.matcher(text);
        Matcher arg1Matcher = organizationPattern.matcher(text);
        Matcher signalMatcher = signalPattern.matcher(text);
        while(originateMatcher.find()){
            arg2Matcher.find(originateMatcher.start());
            int arg2Start = arg2Matcher.start()+1;
            int arg2End = arg2Matcher.end()-1;
            signalMatcher.find(arg2End);
            int sigStart = signalMatcher.start();
            int sigEnd = signalMatcher.end();
            arg1Matcher.find(sigEnd-1);
            int arg1Start = arg1Matcher.start()+1;
            int arg1End = arg1Matcher.end()-1;
            String grammaticalSignal = getGrammaticalSignal(text.substring(sigStart,sigEnd));
            if (!organizations.contains(text.substring(arg2Start,arg2End)))
                addRelation("ORIGINATE","SUPPLY",arg1Start,arg1End,"ENAMEX","ORGANIZATION",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","supply");
        }
    }

    private void findDiscovers(String text){
        Pattern discoverPattern = Pattern.compile(endCharactersRegEx+"("+peopleRegEx+"|"+organizationsRegEx+")"+stopListRegEx+"( |'s|’s|'|’){1}"+stopListRegEx+jargonRegEx+endCharactersRegEx);
        Pattern jargonPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+endCharactersRegEx);
        Pattern signalPattern = Pattern.compile("('s|’s|'|’|â€™s| ){1}");
        Pattern pplOrgPattern = Pattern.compile(endCharactersRegEx+"("+peopleRegEx+"|"+organizationsRegEx+")"+endCharactersRegEx);
        Matcher discoverMatcher = discoverPattern.matcher(text);
        Matcher arg2Matcher = jargonPattern.matcher(text);
        Matcher arg1Matcher = pplOrgPattern.matcher(text);
        Matcher signalMatcher = signalPattern.matcher(text);
        while(discoverMatcher.find()){
            arg1Matcher.find(discoverMatcher.start());
            int arg1Start = arg1Matcher.start()+1;
            int arg1End = arg1Matcher.end()-1;
            signalMatcher.find(discoverMatcher.start());
            int sigStart = signalMatcher.start();
            int sigEnd = signalMatcher.end();
            arg2Matcher.find(discoverMatcher.start());
            int arg2Start = arg2Matcher.start()+1;
            int arg2End = arg2Matcher.end()-1;
            String grammaticalSignal = getGrammaticalSignal(text.substring(sigStart,sigEnd));
            if (arg1End!=arg2End){
                addRelation("OPINION","STANDARD",-1,-1,"ENAMEX","CITATION",sigStart,sigEnd,grammaticalSignal,arg1Start,arg2End,"JARGON","NA","discover");
                if (organizations.contains(text.substring(arg1Start,arg1End))){
                    addRelation("ORIGINATE","DISCOVER",arg1Start,arg1End,"ENAMEX","ORGANIZATION",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","discover");
                }else{
                    addRelation("ORIGINATE","DISCOVER",arg1Start,arg1End,"ENAMEX","PERSON",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","discover");
                }
            }
        }
    }

    private void findOpinions(String text, String subtype, String signalsRegEx){
        Pattern opinionPattern = Pattern.compile(endCharactersRegEx+signalsRegEx+stopListRegEx+jargonRegEx+endCharactersRegEx);
        Pattern jargonPattern = Pattern.compile(endCharactersRegEx+jargonRegEx+endCharactersRegEx);
        Pattern signalPattern = Pattern.compile(endCharactersRegEx+signalsRegEx+endCharactersRegEx);
        Matcher opinionMatcher = opinionPattern.matcher(text);
        Matcher arg2Matcher = jargonPattern.matcher(text);
        Matcher signalMatcher = signalPattern.matcher(text);
        while(opinionMatcher.find()){
            boolean goodMatch = true;
            signalMatcher.find(opinionMatcher.start());
            int sigStart = signalMatcher.start()+1;
            int sigEnd = signalMatcher.end()-1;
            arg2Matcher.find(sigEnd);
            int arg2Start = arg2Matcher.start()+1;
            int arg2End = arg2Matcher.end()-1;
            String grammaticalSignal = getGrammaticalSignal(text.substring(sigStart,sigEnd));
            String sigText =text.substring(sigStart,sigEnd);
            if (prepDict.get(sigText)!=null){
                for (String item :prepDict.get(sigText)){
                    if(text.lastIndexOf(item + " " +sigText) + (item + " " +sigText).length() == sigEnd){
                        goodMatch = false; break;
                    }
                }
            }
            if (goodMatch)
                addRelation("OPINION",subtype,-1,-1,"ENAMEX","CITATION",sigStart,sigEnd,grammaticalSignal,arg2Start,arg2End,"JARGON","NA","opinion"+subtype);
        }
    }

    private String getGrammaticalSignal(String sig) {
        if (sig.equals("(")){
            return "PARENTHESES";
        }if (sig.equals(" ")){
            return "NOUN_MOD";
        }if(sig.equals(":")||sig.equals(",")){
            return "APPOSITION";
        }if (sig.contains("'")||sig.contains("’")){
            return "POSSESSIVE";
        }return "";
    }


    private void matchRelationArguments(RelationDictionaryItem item, String text){
        int firstOffset = 0, secondOffset = -1, thirdOffset = -1;
        boolean firstNull=false,secondNull=false;
        boolean noOverlap = (!item.getGrammaticalSignal().equals("NOUN_MOD") && !item.getGrammaticalSignal().equals("MORPHOLOGY") && !item.getGrammaticalSignal().equals("POSSESSIVE")) ? true:false;
        while(firstOffset!=-1){
            firstOffset = text.indexOf(item.orderedArgs[0][1],firstOffset+item.orderedArgs[0][1].length());
            if (firstOffset != -1){
                secondOffset = (noOverlap) ?  text.indexOf(item.orderedArgs[1][1],item.orderedArgs[0][1].length()+firstOffset):text.indexOf(item.orderedArgs[1][1], firstOffset);
                thirdOffset = 0;
                if (secondOffset >= firstOffset && ((!text.substring(firstOffset,secondOffset).contains(". ") && !text.substring(firstOffset,secondOffset).contains("\n"))||(firstOffset==0||firstNull))){
                    thirdOffset = (noOverlap) ?  text.indexOf(item.orderedArgs[2][1],item.orderedArgs[1][1].length()+secondOffset):text.indexOf(item.orderedArgs[2][1], secondOffset);
                    if (thirdOffset >= secondOffset && ((!text.substring(secondOffset,thirdOffset).contains(". ") && !text.substring(secondOffset,thirdOffset).contains("\n"))||(secondOffset==0||secondNull))){
                        int arg1Offset = (item.orderedArgs[0][0].equals("arg1")) ? firstOffset : (item.orderedArgs[1][0].equals("arg1")) ? secondOffset : thirdOffset;
                        int arg2Offset = (item.orderedArgs[0][0].equals("arg2")) ? firstOffset : (item.orderedArgs[1][0].equals("arg2")) ? secondOffset : thirdOffset;
                        int signalOffset = (item.orderedArgs[0][0].equals("signal")) ? firstOffset : (item.orderedArgs[1][0].equals("signal")) ? secondOffset : thirdOffset;
                        addRelation(item, arg1Offset, arg2Offset, signalOffset,"kb");
                    }
                }
            }
            if (firstOffset == 0||firstNull){
                firstNull=true;
                firstOffset = (secondOffset > 0 && thirdOffset > 0 && !secondNull) ? secondOffset+item.orderedArgs[1][1].length() : (thirdOffset > 0 && secondOffset != -1) ? thirdOffset+item.orderedArgs[2][1].length(): -1;
                if (secondOffset == 0){
                    secondNull = true;
                }
            }
        }
    }

    private void addRelation(String type, String arg3, int arg1Start, int arg1End, String arg1Type, String arg1Subtype, int signalStart, int signalEnd, String grammaticalSignal, int arg2Start, int arg2End, String arg2Type, String arg2Subtype,String source) {

        try{
            FuseAnnotation ann1, ann2;
            if (arg1Start != -1 && arg1End != -1){
                ann1 = new FuseAnnotation("fuse-entity",
                    new FuseEntitySpan(arg1Start,arg1End),
                    new FeatureSet(),
                    FuseAnnotation.AnnotationCategory.valueOf(arg1Type),
                    FuseAnnotation.AnnotationType.valueOf(arg1Subtype));
                    ann1.put("auto",source);
                    doc.addAnnotation(ann1);
            }else{
                ann1 = FuseDocument.THIS_ARTICLE;
            }
            if (arg2Start != -1 && arg2End != -1){
                ann2 = new FuseAnnotation("fuse-entity",
                        new FuseEntitySpan(arg2Start,arg2End),
                        new FeatureSet(),
                        FuseAnnotation.AnnotationCategory.valueOf(arg2Type),
                        FuseAnnotation.AnnotationType.valueOf(arg2Subtype));
                ann2.put("auto",source);
                doc.addAnnotation(ann2);
            }else{
                ann2 = FuseDocument.THIS_ARTICLE;
            }
            FuseAnnotation signalEntity=null;
            if (grammaticalSignal.equals("")){
                FeatureSet featureSet = new FeatureSet();
                FuseEntitySpan fuseSignalSpan = new FuseEntitySpan(signalStart,signalEnd);
                signalEntity = new FuseAnnotation(
                        "fuse-entity", fuseSignalSpan, featureSet,
                        FuseAnnotation.AnnotationCategory.SIGNAL,
                        FuseAnnotation.AnnotationType.NA);
                signalEntity.put("auto",source);
                signalEntity = (FuseAnnotation)doc.addAnnotation(signalEntity);
                }
            FuseRelation relation = new FuseRelation("",
                    ann1,
                    ann2,
                    arg3,
                    "",
                    FuseRelation.RelationType.valueOf(type),
                    signalEntity,
                    grammaticalSignal);
            relation.setAuto(source);
            doc.addRelation(relation);
        }catch(Exception e) {
            //System.out.println(item.text);
        }
    }

    private void addRelation(RelationDictionaryItem item, int arg1Offset, int arg2Offset, int signalOffset, String source) {
        try{
            FuseAnnotation ann1, ann2;
            if (item.arg1Start != -1){
                int arg1Start = correctOffset(arg1Offset, "backwards");
                int arg1End = correctOffset(item.arg1[1].length()+arg1Offset, "forwards");
                if (arg1Start != -1 && arg1End != -1){
                    ann1 = new FuseAnnotation("fuse-entity",
                            new FuseEntitySpan(arg1Start,arg1End),
                            new FeatureSet(),
                            item.arg1Type,
                            item.arg1Subtype);
                    ann1.put("auto",source);
                    doc.addAnnotation(ann1);
                }else return;
            }else {
                ann1 = FuseDocument.THIS_ARTICLE;
            }
            if (item.arg2Start != -1){
                int arg2Start = correctOffset(arg2Offset, "backwards");
                int arg2End = correctOffset(item.arg2[1].length()+arg2Offset, "forwards");
                if (arg2End != -1 && arg2Start !=-1){
                    ann2 = new FuseAnnotation("fuse-entity",
                            new FuseEntitySpan(arg2Start,arg2End),
                            new FeatureSet(),
                            item.arg2Type,
                            item.arg2Subtype);
                    ann2.put("auto",source );
                    doc.addAnnotation(ann2);
                }else return;
            }else {
                ann2 = FuseDocument.THIS_ARTICLE;
            }
            FuseAnnotation signalEntity = null;
            if ((item.signalStart > -1) && (item.signalEnd > -1) && (item.grammaticalSignal.equals(" "))) {
                FeatureSet featureSet = new FeatureSet();
                int signalStart = correctOffset(signalOffset,"backwards");
                int signalEnd = correctOffset(item.signal[1].length()+signalOffset, "forwards");
                if (signalEnd != -1 && signalStart != -1){
                    FuseEntitySpan fuseSignalSpan = new FuseEntitySpan(signalStart,signalEnd);
                    signalEntity = new FuseAnnotation(
                            "fuse-entity", fuseSignalSpan, featureSet,
                            FuseAnnotation.AnnotationCategory.SIGNAL,
                            FuseAnnotation.AnnotationType.NA);
                    signalEntity.put("auto",source);
                    signalEntity = (FuseAnnotation)doc.addAnnotation(signalEntity);
                }else return;
            }
            FuseRelation relation = new FuseRelation("",
                    ann1,
                    ann2,
                    item.arg3,
                    "",
                    item.relationType,
                    signalEntity,
                    item.grammaticalSignal);
            relation.setAuto(source+": "+item.getSource());

            String sourceFile = item.getSource().toLowerCase();
            String[] sourceFiles;
            if (sourceFile.contains(",")){
                sourceFiles = sourceFile.split(",");
                for(String file: sourceFiles){
                    if (file.contains("angus")||file.contains("harriet")){
                        file = file.substring(file.indexOf("-",8)+1)+".xml";
                    }
                    if (file.contains("adjudicated-")){
                        file = file.substring(12)+".xml";
                    }
                    if (file.toLowerCase().contains("lee-jargon")){
                        file = file.substring(11)+".xml";
                    }
                    if (file.toLowerCase().contains("nurit-")){
                        file = file.substring(7)+".xml";
                    }
                    if (file.endsWith("-mae-pre.xml")||file.endsWith("-mae.xml")){
                        file = file.substring(0,file.lastIndexOf("-mae"))+"_mae.xml";
                    }
                    if (!doc.getName().toLowerCase().endsWith(file)){
                        doc.addRelation(relation);
                        break;
                    }
                }
            }else{
                if (sourceFile.contains("angus")||sourceFile.contains("harriet")){
                    sourceFile = sourceFile.substring(sourceFile.indexOf("-",8)+1)+".xml";
                }
                if (sourceFile.contains("adjudicated-")){
                    sourceFile = sourceFile.substring(12)+".xml";
                }
                if (sourceFile.toLowerCase().contains("lee-jargon")){
                    sourceFile = sourceFile.substring(11)+".xml";
                }
                if (sourceFile.toLowerCase().contains("nurit-")){
                    sourceFile = sourceFile.substring(7)+".xml";
                }
                if (sourceFile.endsWith("-mae-pre.xml")||sourceFile.endsWith("-mae.xml")){
                    sourceFile = sourceFile.substring(0,sourceFile.lastIndexOf("-mae"))+"_mae.xml";
                }
                if (!doc.getName().toLowerCase().endsWith(sourceFile))
                    doc.addRelation(relation);
            }

        }catch(Exception e) {
            //System.out.println(item.text);
        }
    }


    private int correctOffset(int offset, String direction) {
        if (offset == 0) return offset;
        int correctedOffset = offset;
        int correctionCounter = 0;
        String text = doc.text();
        char[] stopChars = new char[]{' ',',','.','\n','-','(',')','[',']','\\','^','$','|','\'','’','?','*','+','/','"',':',';','!','_','&','{','}','=','—','`','~'};
        do{
            for (int i = 0; i < stopChars.length;i++){
                if (direction.equalsIgnoreCase("forwards")){
                    if(text.charAt(correctedOffset) == stopChars[i]){
                        return correctedOffset;
                    }
                }if (direction.equalsIgnoreCase("backwards")){
                    if(text.charAt(correctedOffset-1) == stopChars[i]){
                        return correctedOffset;
                    }
                }
            }
            if (direction.equalsIgnoreCase("forwards")){
                correctedOffset++;
            }if (direction.equalsIgnoreCase("backwards")){
                correctedOffset--;
            }
            correctionCounter++;
            if (correctionCounter == 2){return -1;}
        }while(text.substring(correctedOffset,correctedOffset+1) != null);

        return correctedOffset;
    }

    public RelationDictionaryItem dictionaryItemFromString(String s) {
        return null;
    }

    public class RelationDictionaryItem {
        private String text;
        private int arg1Start;
        private int arg1End;
        private String[] arg1 = new String[]{"arg1",""};
        private FuseAnnotation.AnnotationCategory arg1Type;
        private FuseAnnotation.AnnotationType arg1Subtype;
        private int arg2Start;
        private int arg2End;
        private String[] arg2 = new String[]{"arg2",""};
        private FuseAnnotation.AnnotationCategory arg2Type;
        private FuseAnnotation.AnnotationType arg2Subtype;
        private int signalStart;
        private int signalEnd;
        private String[] signal = new String[]{"signal",""};
        private FuseRelation.RelationType relationType;
        private String arg3;
        private String grammaticalSignal;
        private String[][] orderedArgs = new String[3][2];
        private String source ="";
        private RelationDictionaryItem() {
        }

        public RelationDictionaryItem(String s) {
            try{
                String[] parts = s.split("\\|\\|\\|");
                text = parts[0];
                String[] sec1parts = parts[1].split(",");
                arg1Start = Integer.parseInt(sec1parts[0]);
                arg1End = Integer.parseInt(sec1parts[1]);
                arg1[1]= (arg1Start != -1) ? text.substring(arg1Start,arg1End): "";
                arg1Type = FuseAnnotation.AnnotationCategory.valueOf(sec1parts[2]);
                if (arg1Type.name().equals("ENAMEX") && !sec1parts[3].equals(" "))
                    arg1Subtype = FuseAnnotation.AnnotationType.valueOf(sec1parts[3].toUpperCase());
                else
                    arg1Subtype = FuseAnnotation.AnnotationType.NA;
                String[] sec2parts = parts[2].split(",");
                arg2Start = Integer.parseInt(sec2parts[0]);
                arg2End = Integer.parseInt(sec2parts[1]);
                arg2[1]= (arg2Start != -1) ? text.substring(arg2Start,arg2End): "";
                arg2Type = FuseAnnotation.AnnotationCategory.valueOf(sec2parts[2]);
                if (arg2Type.name().equals("ENAMEX") && !sec2parts[3].equals(" "))
                    arg2Subtype = FuseAnnotation.AnnotationType.valueOf(sec2parts[3].toUpperCase());
                else
                    arg2Subtype = FuseAnnotation.AnnotationType.NA;
                String[] sec3parts = parts[3].split(",");
                signalStart = Integer.parseInt(sec3parts[0]);
                signalEnd = Integer.parseInt(sec3parts[1]);
                signal[1] = (signalStart != -1) ? text.substring(signalStart,signalEnd): "";
                String[] sec4parts = parts[4].split(",");
                relationType = FuseRelation.RelationType.valueOf(sec4parts[0]);
                arg3 = sec4parts[1];
                grammaticalSignal = sec4parts[2];
                source = parts[5];
                setArgOrder();
            }catch(Exception e){
                //System.out.println(s);
            }
        }

        private void setArgOrder(){
            int[] argStarts = new int[]{arg1Start,arg2Start,signalStart};
            int leastIndex = findMinIndex(argStarts);
            if (leastIndex == 0){
                orderedArgs[0] = arg1;
                if (arg2Start <= signalStart){
                    orderedArgs[1] = arg2;
                    orderedArgs[2] = signal;
                }else{
                    orderedArgs[1] = signal;
                    orderedArgs[2] = arg2;
                }
            }else if (leastIndex == 1){
                orderedArgs[0] = arg2;
                if (arg1Start <= signalStart){
                    orderedArgs[1] = arg1;
                    orderedArgs[2] = signal;
                }else{
                    orderedArgs[1] = signal;
                    orderedArgs[2] = arg1;
                }
            }else{
                orderedArgs[0] = signal;
                if (arg1Start <= arg2Start){
                    orderedArgs[1] = arg1;
                    orderedArgs[2] = arg2;
                }else{
                    orderedArgs[1] = arg2;
                    orderedArgs[2] = arg1;
                }
            }
        }

        private int findMinIndex(int[] ints){
            int minIndex = 0;
            for (int i= 1; i<ints.length;i++){
                if (ints[minIndex] > ints[i])
                    minIndex = i;
            }   return minIndex;
        }

        public String getText() {
            return text;
        }

        public int getArg1Start() {
            return arg1Start;
        }

        public int getArg1End() {
            return arg1End;
        }

        public FuseAnnotation.AnnotationCategory getArg1Type() {
            return arg1Type;
        }

        public FuseAnnotation.AnnotationType getArg1Subtype() {
            return arg1Subtype;
        }

        public int getArg2Start() {
            return arg2Start;
        }

        public int getArg2End() {
            return arg2End;
        }

        public FuseAnnotation.AnnotationCategory getArg2Type() {
            return arg2Type;
        }

        public FuseAnnotation.AnnotationType getArg2Subtype() {
            return arg2Subtype;
        }

        public int getSignalStart() {
            return signalStart;
        }

        public int getSignalEnd() {
            return signalEnd;
        }

        public FuseRelation.RelationType getRelationType() {
            return relationType;
        }

        public String getArg3() {
            return arg3;
        }

        public String getGrammaticalSignal() {
            return grammaticalSignal;
        }

        public String getSource(){
            return source;
        }

    }
}