package FuseJet.Chunker;

import opennlp.maxent.BasicEventStream;
import opennlp.maxent.GIS;
import opennlp.maxent.GISModel;
import opennlp.maxent.PlainTextByLineDataStream;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.EventStream;

import java.io.*;
import java.util.StringTokenizer;

/**
 * User: yhe
 * Date: 7/25/12
 * Time: 12:08 PM
 */
public class Trainer {
    private static boolean USE_SMOOTHING = false;
    private static double SMOOTHING_OBSERVATION = 0.1;
    private static boolean PRINT_MESSAGES = true;

    public static void generateFeatures (String featureFile, String inFile) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inFile));
            PrintStream writer = new PrintStream (new FileOutputStream(featureFile));
            String line;
            String prevToken = "";
            String prevPOS = "";
            String prevTag = "";
            String currentToken = "";
            String currentPOS = "";
            String currentTag = "";
            String nextToken = "";
            String nextPOS = "";
            String nextTag = "";
            String followingToken = "";
            String followingPOS = "";
            String followingTag = "";
            StringBuffer features = new StringBuffer(200);

            boolean inGroup = false;
            boolean firstToken = true;
            while((line = reader.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line);
                int count = st.countTokens();
                if (count == 0) {
                    // blank line -- end of sentence
                    followingToken = "";
                    followingPOS = "";
                    followingTag = "";
                } else if (count >= 3) {
                    // token
                    followingToken = st.nextToken();
                    followingPOS = st.nextToken();
                    followingTag = st.nextToken();
                } else {
                    System.out.println ("Error:  invalid input line: " + line);
                }
                if (currentToken != "") {
                    features.setLength(0);
                    features.append("prevPOS=" + prevPOS + " ");
                    features.append("currPOS=" + currentPOS + " ");
                    features.append("nextPOS=" + nextPOS + " ");
                    if (nextToken == "")
                        features.append("POS012=" + currentPOS + ":: ");
                    else
                        features.append("POS012=" + currentPOS + ":" + nextPOS
                                + ":" + followingPOS + " ");
                    features.append("prevTag=" + prevTag + " ");
                    features.append("currWord=" + currentToken + " ");
                    features.append("W-1W0=" + prevToken + ":" + currentToken + " ");
                    features.append("W0W1=" + currentToken + ":" + nextToken + " ");
                    features.append(currentTag);
                    writer.println(features);
                }
                prevToken = currentToken;
                prevPOS = currentPOS;
                prevTag = currentTag;
                currentToken = nextToken;
                currentPOS = nextPOS;
                currentTag = nextTag;
                nextToken = followingToken;
                nextPOS = followingPOS;
                nextTag = followingTag;
            }
            writer.close();
            reader.close();
        } catch (IOException e) {
            System.err.println ("IO Error. Stacktrace:");
            e.printStackTrace();
        }
    }

    public static void createModel(String featureFileName, String modelFileName) {
        // String dataFileName = ChunkBuildTrain.chunkDir + "chunk features.txt";
        // String modelFileName = ChunkBuildTrain.chunkDir + "chunk model.txt";
        //String home =
        //        "C:/Documents and Settings/Ralph Grishman/My Documents/";
        //String dataFileName = home + "jet temp/coref features.txt";
        //String modelFileName = home + "jet temp/coref model.txt";
        try {
            FileReader datafr = new FileReader(new File(featureFileName));
            EventStream es =
                    new BasicEventStream(new PlainTextByLineDataStream(datafr));
            GIS.SMOOTHING_OBSERVATION = SMOOTHING_OBSERVATION;
            GISModel model = GIS.trainModel(es, 100, 4, USE_SMOOTHING, PRINT_MESSAGES);
            File outputFile = new File(modelFileName);
            GISModelWriter writer = new SuffixSensitiveGISModelWriter(model, outputFile);
            writer.persist();
            //writer.close();
        } catch (Exception e) {
            System.out.print("Unable to create model due to exception: ");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("FuseJet.Chunker.Trainer featureFile modelFile dataFile");
            System.exit(-1);
        }
        String featureFile = args[0];
        String modelFile = args[1];
        String dataFile = args[2];

        generateFeatures(featureFile, dataFile);
        createModel(featureFile, modelFile);
    }
}
