package FuseJet.Chunker;

import FuseJet.Models.FuseDocument;
import Jet.Console;
import Jet.JetTest;
import Jet.Lex.Tokenizer;
import Jet.Tipster.Annotation;
import Jet.Tipster.Document;
import Jet.Tipster.Span;
import opennlp.maxent.GISModel;
import opennlp.maxent.io.SuffixSensitiveGISModelReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * User: yhe
 * Date: 7/25/12
 * Time: 5:29 PM
 */
public class Chunker {

    public  GISModel model = null;
    private  Annotation[] tokens;
    private  int tokenCount, jetTokenCount;
    private  String[] pos, word, tag;
    private  int[] position;
    private  boolean trace = false;

    /**
     *  adds chunks (annotations of type <b>ng</b>) to Span 'span' of
     *  Document 'doc'.
     */

    public void chunk (Document doc, Span span) {
        Vector<Annotation> tokensInCurrentDocument = doc.annotationsOfType("token");
        if (tokensInCurrentDocument == null) return;
        // 1. get tokens
        List<Annotation> tokensList = new ArrayList<Annotation>();
        for (Annotation token : tokensInCurrentDocument) {
            if (token.span().within(span))  {
                tokensList.add(token);
            }
        }
        tokens = tokensList.toArray(new Annotation[tokensList.size()]);
        // TODO: we should preferably use this, but need to double check if tokenization is always correct.
        // tokens = Tokenizer.gatherTokens(doc, span);
        tokenCount = tokens.length;
        if (tokenCount == 0) {
            return;
        }
        // 2. gather Penn POS and token texts
        pos = new String[tokenCount];
        word = new String[tokenCount];
        position = new int[tokenCount];
        int itoken = 0;
        for (int i=0; i<tokenCount; i++) {
            int posn = tokens[i].span().start();
            position[itoken] = posn;
            pos[itoken] = getPosAt (doc, posn);
            word[itoken] = (String)tokens[i].get("tokenString");
                    //doc.text(tokens[i]).trim();
//            if (itoken >= 2 && doc.text(tokens[i-1]).equals("-")) {
//                pos[itoken-2] = "JJ";
//                word[itoken-2] = word[itoken-2] + word[itoken-1] + word[itoken];
//                itoken = itoken-2;
//            }
            itoken++;
        }
        //jetTokenCount = itoken;
        // 3. compute features and predict chunk tag
        tag = new String[tokenCount];
        for (int i=0; i<tokenCount; i++) {
            String[] features = chunkFeatures(i);
            tag[i] = model.getBestOutcome(model.eval(features)).intern();
            if ((i == 0 || tag[i-1].equals("O")) && tag[i].equals("I"))
                tag[i] = "B";
            // System.err.print ("Word = " + word[i] + " pos = " + pos[i]);
            // System.err.println ("  tag = " + tag[i]);
        }
        // 4. generate chunk annotation (later, add HEAD feature)
        int chunkStart = -1;
        int nameEnd = -1;
        for (int i=0; i<tokenCount; i++) {
            int posn = position[i];
            if (nameEnd == posn)
                nameEnd = -1;
            // if in a chunk, next tag is O or B, and current and next tokens
            // are not part of same name, this is end of chunk
            if (chunkStart >= 0 && (tag[i].equals("O") || tag[i].equals("B"))
                    && nameEnd < 0) {
                // generate chunk from chunkStart to posn
                Annotation ann = new Annotation ("ng", new Span(chunkStart, posn), null);
                doc.addAnnotation(ann);
                if (trace)
                    Console.println("Annotate " + doc.text(ann) + " as " + ann);
                chunkStart = -1;
            }
            if (nameEnd < 0) {
                Vector nameAnns = doc.annotationsAt(posn, "ENAMEX");
                if (nameAnns != null && nameAnns.size() > 0) {
                    Annotation nameAnn = (Annotation) nameAnns.get(0);
                    nameEnd = nameAnn.span().end();
                }
            }
            // if not in a chunk and next token has tag B or I or is part of
            // a name, start a new chunk
            if (chunkStart < 0 && (tag[i].equals("B") || tag[i].equals("I") || nameEnd > 0)) {
                chunkStart = posn;
            }
        }
        // 4a. generate final chunk
        if (chunkStart >= 0) {
            int lastTokenEnd = tokens[tokenCount-1].span().end();
            // generate chunk from chunkStart to lastTokenEnd
            Annotation ann = new Annotation ("ng", new Span(chunkStart, lastTokenEnd), null);
            doc.addAnnotation(ann);
            if (trace)
                Console.println ("Annotate " + doc.text(ann) + " as " + ann);
        }
    }

    private String getPosAt (Document doc, int posn) {
        Vector v = doc.annotationsAt (posn, "tagger");
        if (v == null || v.size() == 0) return "";
        Annotation taggerAnn = (Annotation) v.get(0);
        return (String) taggerAnn.get("cat");
    }

    private String[] chunkFeatures (int i) {
        String[] features = new String[8];
        features[0] = "prevPOS=" + (i>0 ? pos[i-1] : "");
        features[1] = "currPOS=" + pos[i];
        features[2] = "nextPOS=" + (i < (jetTokenCount-1) ? pos[i+1] : "");
        if (i < (jetTokenCount-2))
            features[3] = "POS012=" + pos[i] + ":" + pos[i+1] + ":" + pos[i+2];
        else if (i < (jetTokenCount-2))
            features[3] = "POS012=" + pos[i] + ":" + pos[i+1] + ":";
        else
            features[3] = "POS012=" + pos[i] + "::";
        features[4] = "prevTag=" + (i>0 ? tag[i-1] : "");
        features[5] = "currWord=" + word[i];
        features[6] = "W-1W0=" + (i>0 ? word[i-1] : "") + ":" + word[i];
        features[7] = "W0W1=" + word[i] + ":" + (i < (jetTokenCount-1) ? word[i+1] : "");
        return features;
    }

    /**
     *  load a maximum entropy chunker model from file 'modelFileName'.
     */

    public void loadModel (String modelFileName) {
        try {
            model = (GISModel) new SuffixSensitiveGISModelReader(new File(modelFileName)).getModel();
            System.err.println ("Chunker model loaded from " + modelFileName);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

}
