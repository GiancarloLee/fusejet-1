#The NYU Chinese Terminology Extractor for FUSE

##Introduction

We will list the files in the provided package of the package, describe how to run the sample and real term extraction script, explains how to configure the chunker and the term extractor, and walk through what the term extraction script does step by step in this document.

##Files in the package
You can find the following in the zip file:

###Term extractor

File | Description
--------------------- | --------------------------
*.py and *.sh files | script files supporting term extraction
FuseJet.jar | the term extraction software
data/ | data used by the term extractor
Models/ | data used by the chunker etc.
FuseJet-Lexis_CN2TEXT.properties | configuration file for the noun group chunker
FuseJet-TermExtraction_NewCN.properties | configuration file for the term extractor
terms.sample.expected.txt | the expected output of run_sample_cn.sh

###Sample data

File | Description
-------------------------- | --------------------------
background.xml.filelist | file list for the background set
sampleBackground/ | sample background files
rdg.xml.filelist | file list for the foreground set
sampleRDG/ | sample foreground files

##Running the system

###Running the sample script
To run the term extraction system on sample data, unzip MITRE.zip, and enter the directory. Then run:

    ./run_sample_cn.sh work rdg.xml.filelist background.xml.filelist sample.terms.txt
    
The term list will be written to `sample.terms.txt`

Note that `run_sample_cn.sh` uses `term.sample.properties` internally. The parameters in `term.sample.properties` do not generate best results. They are tuned so that we can have some results on very small sample rdgs. We recommend using `run_cn.sh` in real tasks. The usages of these two scripts are exactly the same, as described below.

We expect the content of `sample.terms.txt` to match the existing `terms.sample.expected.txt` file.

###Running the real script

Before running the system, please make sure that the two property files are configured correctly according to the **Configurations** section.

In general, run the system using:

    ./run_cn.sh WORKING_DIRECTORY INPUT_XML_FILELIST BACKGROUND_XML_FILELIST TERMS_FILE

where WORKING_DIRECTORY is the directory to store intermediate files, INPUT_XML_FILELIST is the list of the foreground set, BACKGROUND_XML_FILELIST the list of the background set, and TERMS_FILE name of the file to store extracted terms.

Typically, use abound 3,000 random patents as the background set is enough. The same background set can be used for all extraction tasks. the foreground set is the actual "RDG" to extract terms from.

###Memory Consumption
The chunker consumes less than 4GB of memory. On RDGs smaller than 10,000 files, the term extractor uses less than 8GB of memory (we recommend changes line 14 in `run_cn.sh` to allow 16GB memory, i.e. change -Xmx8g to -Xmx16g, when there are more than 5,000 files for better performance). RDGs significantly larger than 10,000 files would require more memory. 

##Configurations
### Chinese preprocessing

`FuseJet-Lexis_CN2TEXT.properties` controls the behavior of the Chinese processing tool. It is unnecessary to change the settings for the chunker, but  make sure that `FuseJet.path1` at the beginning of the file points to the Models/ directory. You might need to provide the absolute path, depending on where you are running the program.

### Term Extraction

`FuseJet-TermExtraction_NewCN.properties` controls the Term Extractor. Make sure that the first three lines point to the respective files in data/ and provide absolute path if necessary.

You could also change the `terminologyThreshold` parameter on line 9. Our program extracts all possible candidates for terms and rank them. Therefore, terms at the bottom of the list might not well represent the RDG/domain. This parameter controls how much of the list we should output. Currently, it is set to 0.6, so that term candidates ranked in top 60% will be considered as terms and write to the output file. You can change it according to your need.

##Notes on the script

The `run_cn.sh` script does the following things:

1. Create the working directory and generate the intermediate file lists (lines 1-12)

2. Preprocess (word segmentation, part of speech tagging and noun group chunking) the foreground set (line 13)

3. Preprocess (word segmentation, part of speech tagging and noun group chunking) the background set (line 14) - *this can be commented out if the background set is already preprocessed*

4. Extract terminologies (line 15)