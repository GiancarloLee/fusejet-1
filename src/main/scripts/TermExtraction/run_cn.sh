TERMSYS_ROOT=/Users/yhe/Desktop/TermExtraction
WORKING_DIR=$1
OLD_XML_FILE_LIST=$2
XML_FILE_LIST=${WORKING_DIR}/$2.limit.xml
NEG_POS_FILE_LIST=$3
OUTPUT_FILE=$4
mkdir ${WORKING_DIR}
python3 ${TERMSYS_ROOT}/shuffle_xml.py ${OLD_XML_FILE_LIST} ${XML_FILE_LIST} 1000
python3 ${TERMSYS_ROOT}/gen_pos_filelist.py ${WORKING_DIR} ${XML_FILE_LIST} > ${WORKING_DIR}/positive.pos.filelist
java -cp ${TERMSYS_ROOT}/Jet.jar -Xmx4g FuseJet.Utils.Console ${TERMSYS_ROOT}/FuseJet-Lexis_CN2TEXT.properties ${XML_FILE_LIST} ${WORKING_DIR}/positive.pos.filelist
java -cp ${TERMSYS_ROOT}/Jet.jar -Xmx8g FuseJet.Terminology.TerminologyExtractor ${TERMSYS_ROOT}/FuseJet-TermExtraction_CN.properties ${WORKING_DIR}/positive.pos.filelist ${NEG_POS_FILE_LIST} ${OUTPUT_FILE}
