#!/usr/bin/env python3
'''
gen_pos_filelist.py working_dir xml_file_list
'''

import os, sys
working_dir, xml_file_list = sys.argv[1:]
xml_files = open(xml_file_list).readlines()
#cmd = "rm -rf %s" % working_dir
#sys.stderr.write(cmd + "\n")
#os.system(cmd)
cmd = "mkdir %s" % working_dir
sys.stderr.write(cmd + "\n")
os.system(cmd)
for xml_file in xml_files:
    xml_file = xml_file.strip().strip("/")
    xml_file = xml_file.replace("/", "_")
    print(working_dir + '/' + xml_file + ".pos")
    


