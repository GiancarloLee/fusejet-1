#!/usr/bin/env python
'''
find_filenames.py xml_file
'''
import sys
try:
    input_file = sys.argv[1]
except:
    print(__doc__)
    sys.exit(-1)

for line in open(input_file):
    line = line.strip()
    if line.startswith('<rdgx:Uri>'):
        line = line[len('<rdgx:Uri>file://'):-len('</rdgx:Uri>')]
        print(line)
