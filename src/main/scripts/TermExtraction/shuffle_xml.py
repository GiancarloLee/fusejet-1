#!/usr/bin/env python3
import os, sys, random

try:
    input_file, output_file, size_limit = sys.argv[1:]
    size_limit = int(size_limit)
except:
    print("[Usage] shuffle_xml.py input_file output_file size_limit", file=sys.stderr)
    sys.exit(-1)

input_files = open(input_file).readlines()
output_list = []
o = open(output_file, 'w')

for line in open(input_file):
    filename = line.strip()
    filesize = os.path.getsize(filename)
    if (filesize < 1500000):
        output_list.append(filename)
    else:
        sys.stderr.write('File chosen but size to big:\n%s: %d bytes\n' %
                (filename, filesize))

random.seed(471375478141257564597)
random.shuffle(output_list)
if len(output_list) < size_limit:
    size_limit = len(output_list)
for i in range(size_limit):
    o.write(output_list[i] + '\n')
